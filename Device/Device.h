//
//  Device.h
//  restor
//
//  Created by Umang on 12/8/14.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Message;

@interface Device : NSObject
{
    NSArray *calender;
    NSArray *callHistory;
    NSArray *contacts;
    NSArray *messages;
    NSArray *notes;
    NSArray *whatsApp;
    NSString *contentFilePath;
}
@property (nonatomic, strong) NSArray *calender;
@property (nonatomic, strong) NSArray *callHistory;
@property (nonatomic, strong) NSArray *contacts;
@property (nonatomic, strong) NSArray *messages;
@property (nonatomic, strong) NSArray *notes;
@property (nonatomic, strong) NSArray *whatsApp;
@property (nonatomic, strong) NSString *contentFilePath;

-(id)initWithFilePath:(NSString *)filePath;
-(id)initWithOldDataFilePath:(NSString *)filePath;
-(void)refreshContent;

//functions for deletion.
-(void)deleteContactAtIndex:(NSInteger)index;
-(void)deleteContactsAtIndexs:(NSArray *)arr;

//functions to delete message.
-(void)deleteMessagesAtIndexs:(NSArray *)arr;
-(void)deleteMessageAtIndex:(NSInteger)index;

//functions to delete calender entry.
-(void)deleteCalendersAtIndexs:(NSArray *)arr;
-(void)deleteCalenderAtIndex:(NSInteger)index;

//functions to delete notes.
-(void)deleteNotesAtIndexs:(NSArray *)arr;
-(void)deleteNoteAtIndex:(NSInteger)index;

//function to delete call history.
-(void)deleteCallHistorysAtIndexs:(NSArray *)arr;
-(void)deleteCallHistoryAtIndex:(NSInteger)index;

//functions to delete Whats app.
-(void)deleteWhatsappsAtIndexs:(NSArray *)arr;
-(void)deleteWhatsappAtIndex:(NSInteger)index;
@end
