//
//  Device.m
//  restor
//
//  Created by Umang on 12/8/14.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import "utf8Util.h"
#import "Device.h"

@implementation Device
@synthesize calender,callHistory,contacts,messages,notes,whatsApp,contentFilePath;

-(id)initWithFilePath:(NSString *)filePath
{
    self = [super init];
    if (self)
    {
        self.messages = [[JSONReader sharedReader] getMessagesForDevicePath:filePath];
        self.contacts = [[JSONReader sharedReader] getContactsForDevicePath:filePath];
        self.calender = [[JSONReader sharedReader] getCalenderForDevicePath:filePath];
        self.notes = [[JSONReader sharedReader] getNotesForDevicePath:filePath];
        self.callHistory = [[JSONReader sharedReader] getCallHistoryForDevicePath:filePath];
        self.whatsApp = [[JSONReader sharedReader] getWhatsAppForDevicePath:filePath];
        
        self.contentFilePath = filePath;
    }
    return self;
}

-(id)initWithOldDataFilePath:(NSString *)filePath
{
    self = [super init];
    if (self)
    {
        self.messages = [[JSONReader sharedReader] getMessagesForOldDevicePath:filePath];
        self.contacts = [[JSONReader sharedReader] getContactsForOldDevicePath:filePath];
        self.calender = [[JSONReader sharedReader] getCalenderForOldDevicePath:filePath];
        self.whatsApp = [[JSONReader sharedReader] getWhatsAppForOldDevicePath:filePath];
        self.notes = [[JSONReader sharedReader] getNotesForOldDevicePath:filePath];
        self.callHistory = [[JSONReader sharedReader] getCallHistoryForOldDevicePath:filePath];
        
        self.contentFilePath = [[JSONReader sharedReader] createNewFolderForOldData:filePath];
        [[JSONReader sharedReader] removeOldData:filePath];
    }
    return self;
}

-(void)refreshContent
{
    self.messages = [[JSONReader sharedReader] getMessagesForDevicePath:self.contentFilePath];
    self.contacts = [[JSONReader sharedReader] getContactsForDevicePath:self.contentFilePath];
    self.calender = [[JSONReader sharedReader] getCalenderForDevicePath:self.contentFilePath];
    self.whatsApp = [[JSONReader sharedReader] getWhatsAppForDevicePath:self.contentFilePath];
    self.notes = [[JSONReader sharedReader] getNotesForDevicePath:self.contentFilePath];
    self.callHistory = [[JSONReader sharedReader] getCallHistoryForDevicePath:self.contentFilePath];
}

#pragma mark functions to delete Contacts

-(void)deleteContactsAtIndexs:(NSArray *)arr
{
    for (NSIndexPath *indexPath in arr)
    {
        [self deleteContactAtIndex:indexPath.row];
    }
}

-(void)deleteContactAtIndex:(NSInteger)index
{
    Contact *contactToDelete = [self.contacts objectAtIndex:index];
    
    NSArray *arrFilesToRead = [[JSONReader sharedReader] getFilesForName:@"contacts.json" inDirectory:self.contentFilePath];
    for (NSString *filePath in arrFilesToRead)
    {
        [self readContactsContentAndDeleteAndSaveFile:filePath contact:contactToDelete];
    }
}

-(void)readContactsContentAndDeleteAndSaveFile:(NSString *)filePath contact:(Contact *)object
{
    NSMutableArray *arrOfContacts = [[NSMutableArray alloc] init];
    
    //read the content and delete the record.
    NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    
    //first check it is valid
    utf8Util * utfValidation = [[utf8Util alloc] init];
    
    [utfValidation Validateutf8File:filePath];
    
    
    
    NSData *data = [content dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if (json && [json objectForKey:@"contacts"])
    {
        for (NSDictionary *dic in [json objectForKey:@"contacts"])
        {
            Contact *obj = [Contact modelObjectWithDictionary:dic];
            if (![obj.dictionaryRepresentation isEqualToDictionary:object.dictionaryRepresentation])
            {
                [arrOfContacts addObject:obj.dictionaryRepresentation];
            }
        }
    }
    
    NSDictionary *dic = @{@"contacts": arrOfContacts};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:nil];
    NSString *strData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [strData writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    self.contacts = [[JSONReader sharedReader] getContactsForDevicePath:self.contentFilePath];
}

#pragma mark functions to delete Message

-(void)deleteMessagesAtIndexs:(NSArray *)arr
{
    for (NSIndexPath *indexPath in arr)
    {
        [self deleteMessageAtIndex:indexPath.row];
    }
}

-(void)deleteMessageAtIndex:(NSInteger)index
{
    Message *messageToDelete = [self.messages objectAtIndex:index];
    
    NSArray *arrFilesToRead = [[JSONReader sharedReader] getFilesForName:@"messages.json" inDirectory:self.contentFilePath];
    for (NSString *filePath in arrFilesToRead)
    {
        [self readMessageContentAndDeleteAndSaveFile:filePath message:messageToDelete];
    }
}

-(void)readMessageContentAndDeleteAndSaveFile:(NSString *)filePath message:(Message *)object
{
    NSMutableArray *arrOfMessage = [[NSMutableArray alloc] init];
    //read the content and delete the record.
    NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSData *data = [content dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    if (json && [json objectForKey:@"conversation"])
    {
        for (NSDictionary *dic in [json objectForKey:@"conversation"])
        {
            Message *obj = [Message modelObjectWithDictionary:dic];
            if (![obj.dictionaryRepresentation isEqualToDictionary:object.dictionaryRepresentation])
            {
                [arrOfMessage addObject:obj.dictionaryRepresentation];
            }
        }
    }
    
    NSDictionary *dic = @{@"conversation": arrOfMessage};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:nil];
    NSString *strData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [strData writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    self.messages = [[JSONReader sharedReader] getMessagesForDevicePath:self.contentFilePath];
    
}

#pragma mark functions to delete Calender

-(void)deleteCalendersAtIndexs:(NSArray *)arr
{
    for (NSIndexPath *indexPath in arr)
    {
        [self deleteCalenderAtIndex:indexPath.row];
    }
}

-(void)deleteCalenderAtIndex:(NSInteger)index
{
    Calender *calenderToDelete = [self.calender objectAtIndex:index];
    
    NSArray *arrFilesToRead = [[JSONReader sharedReader] getFilesForName:@"calendar.json" inDirectory:self.contentFilePath];
    for (NSString *filePath in arrFilesToRead)
    {
        [self readCalenderContentAndDeleteAndSaveFile:filePath calender:calenderToDelete];
    }
}

-(void)readCalenderContentAndDeleteAndSaveFile:(NSString *)filePath calender:(Calender *)object
{
    NSMutableArray *arrOfCalender = [[NSMutableArray alloc] init];
    //read the content and delete the record.
    NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSData *data = [content dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    if (json && [json objectForKey:@"calendar"])
    {
        for (NSDictionary *dic in [json objectForKey:@"calendar"])
        {
            Calender *obj = [Calender modelObjectWithDictionary:dic];
            if (![obj.dictionaryRepresentation isEqualToDictionary:object.dictionaryRepresentation])
            {
                [arrOfCalender addObject:obj.dictionaryRepresentation];
            }
        }
    }
    
    NSDictionary *dic = @{@"calendar": arrOfCalender};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:nil];
    NSString *strData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [strData writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    self.calender = [[JSONReader sharedReader] getCalenderForDevicePath:self.contentFilePath];
    
}

#pragma mark functions to delete Notes

-(void)deleteNotesAtIndexs:(NSArray *)arr
{
    for (NSIndexPath *indexPath in arr)
    {
        [self deleteNoteAtIndex:indexPath.row];
    }
}

-(void)deleteNoteAtIndex:(NSInteger)index
{
    Notes *noteToDelete = [self.notes objectAtIndex:index];
    
    NSArray *arrFilesToRead = [[JSONReader sharedReader] getFilesForName:@"notes.json" inDirectory:self.contentFilePath];
    for (NSString *filePath in arrFilesToRead)
    {
        [self readNoteContentAndDeleteAndSaveFile:filePath note:noteToDelete];
    }
}

-(void)readNoteContentAndDeleteAndSaveFile:(NSString *)filePath note:(Notes *)object
{
    NSMutableArray *arrOfnotes = [[NSMutableArray alloc] init];
    //read the content and delete the record.
    NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSData *data = [content dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    if (json && [json objectForKey:@"notes"])
    {
        for (NSDictionary *dic in [json objectForKey:@"notes"])
        {
            Notes *obj = [Notes modelObjectWithDictionary:dic];
            if (![obj.dictionaryRepresentation isEqualToDictionary:object.dictionaryRepresentation])
            {
                [arrOfnotes addObject:obj.dictionaryRepresentation];
            }
        }
    }
    
    NSDictionary *dic = @{@"notes": arrOfnotes};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:nil];
    NSString *strData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [strData writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    self.notes = [[JSONReader sharedReader] getNotesForDevicePath:self.contentFilePath];
}


#pragma mark functions to delete CallHistory

-(void)deleteCallHistorysAtIndexs:(NSArray *)arr
{
    for (NSIndexPath *indexPath in arr)
    {
        [self deleteCallHistoryAtIndex:indexPath.row];
    }
}

-(void)deleteCallHistoryAtIndex:(NSInteger)index
{
    CallHistory *callhistoryToDelete = [self.callHistory objectAtIndex:index];
    
    NSArray *arrFilesToRead = [[JSONReader sharedReader] getFilesForName:@"callhistory.json" inDirectory:self.contentFilePath];
    for (NSString *filePath in arrFilesToRead)
    {
        [self readCallHistoryContentAndDeleteAndSaveFile:filePath callHistory:callhistoryToDelete];
    }
}

-(void)readCallHistoryContentAndDeleteAndSaveFile:(NSString *)filePath callHistory:(CallHistory *)object
{
    NSMutableArray *arrOfCallHistory = [[NSMutableArray alloc] init];
    //read the content and delete the record.
    NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSData *data = [content dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if (json && [json objectForKey:@"callhistory"])
    {
        for (NSDictionary *dic in [json objectForKey:@"callhistory"])
        {
            CallHistory *obj = [CallHistory modelObjectWithDictionary:dic];
            if (![obj.dictionaryRepresentation isEqualToDictionary:object.dictionaryRepresentation])
            {
                [arrOfCallHistory addObject:obj.dictionaryRepresentation];
            }
        }
    }
    
    NSDictionary *dic = @{@"callhistory": arrOfCallHistory};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:nil];
    NSString *strData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [strData writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    self.callHistory = [[JSONReader sharedReader] getCallHistoryForDevicePath:self.contentFilePath];
}

#pragma mark functions to delete Whatsapp

-(void)deleteWhatsappsAtIndexs:(NSArray *)arr
{
    for (NSIndexPath *indexPath in arr)
    {
        [self deleteWhatsappAtIndex:indexPath.row];
    }
}

-(void)deleteWhatsappAtIndex:(NSInteger)index
{
    Whatsapp *whatsappToDelete = [self.whatsApp objectAtIndex:index];
    
    NSArray *arrFilesToRead = [[JSONReader sharedReader] getFilesForName:@"whatsapp.json" inDirectory:self.contentFilePath];
    for (NSString *filePath in arrFilesToRead)
    {
        [self readWhatsappContentAndDeleteAndSaveFile:filePath whatsApp:whatsappToDelete];
    }
}

-(void)readWhatsappContentAndDeleteAndSaveFile:(NSString *)filePath whatsApp:(Whatsapp *)object
{
    NSMutableArray *arrOfWhatsApp = [[NSMutableArray alloc] init];
    //read the content and delete the record.
    NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSData *data = [content dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    if (json && [json objectForKey:@"conversation"])
    {
        for (NSDictionary *dic in [json objectForKey:@"conversation"])
        {
            Whatsapp *obj = [Whatsapp modelObjectWithDictionary:dic];
            if (![obj.dictionaryRepresentation isEqualToDictionary:object.dictionaryRepresentation])
            {
                [arrOfWhatsApp addObject:obj.dictionaryRepresentation];
            }
        }
    }
    
    NSDictionary *dic = @{@"conversation": arrOfWhatsApp};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:nil];
    NSString *strData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [strData writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    self.whatsApp = [[JSONReader sharedReader] getWhatsAppForDevicePath:self.contentFilePath];
    
}

@end
