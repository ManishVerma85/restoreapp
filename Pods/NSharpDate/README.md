NSharpDate
=========

NSharpDate is a category for the NSDate class that adds a few properties and methods. 

for more information and setup instructions go to <a href="http://www.nsdatetostring.com">www.nsdatetostring.com</a>

<a rel="license" href="http://creativecommons.org/licenses/by/3.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by/3.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">NSharpDate</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://www.christianengvall.se" property="cc:attributionName" rel="cc:attributionURL">Christian Engvall</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0 Unported License</a>.