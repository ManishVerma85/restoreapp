//
//  Message.m
//
//  Created by UMANGSHU  on 05/12/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "Message.h"
#import "Messages.h"


NSString *const kMessageName = @"name";
NSString *const kMessagePhone = @"phone";
NSString *const kMessageMessages = @"messages";


@interface Message ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Message

@synthesize name = _name;
@synthesize phone = _phone;
@synthesize messages = _messages;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.name = [self objectOrNilForKey:kMessageName fromDictionary:dict];
            self.phone = [self objectOrNilForKey:kMessagePhone fromDictionary:dict];
    NSObject *receivedMessages = [dict objectForKey:kMessageMessages];
    NSMutableArray *parsedMessages = [NSMutableArray array];
    if ([receivedMessages isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedMessages) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedMessages addObject:[Messages modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedMessages isKindOfClass:[NSDictionary class]]) {
       [parsedMessages addObject:[Messages modelObjectWithDictionary:(NSDictionary *)receivedMessages]];
    }

    self.messages = [NSArray arrayWithArray:parsedMessages];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.name forKey:kMessageName];
    [mutableDict setValue:self.phone forKey:kMessagePhone];
    NSMutableArray *tempArrayForMessages = [NSMutableArray array];
    for (NSObject *subArrayObject in self.messages) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForMessages addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForMessages addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForMessages] forKey:kMessageMessages];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.name = [aDecoder decodeObjectForKey:kMessageName];
    self.phone = [aDecoder decodeObjectForKey:kMessagePhone];
    self.messages = [aDecoder decodeObjectForKey:kMessageMessages];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_name forKey:kMessageName];
    [aCoder encodeObject:_phone forKey:kMessagePhone];
    [aCoder encodeObject:_messages forKey:kMessageMessages];
}

- (id)copyWithZone:(NSZone *)zone
{
    Message *copy = [[Message alloc] init];
    
    if (copy) {

        copy.name = [self.name copyWithZone:zone];
        copy.phone = [self.phone copyWithZone:zone];
        copy.messages = [self.messages copyWithZone:zone];
    }
    
    return copy;
}


@end
