//
//  JSONReader.h
//  restor
//
//  Created by Umang on 05/12/14.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONReader : NSObject
{
    
}
+ (instancetype)sharedReader;

-(NSArray *)getDevicesArray;
-(NSMutableArray *)getDevicesNames;
-(NSString *)getPathForDeviceAtIndex:(NSInteger)index;
-(NSArray *)getFilesForName:(NSString *)fileName inDirectory:(NSString *)strDirectoryPath;


-(NSArray *)getMessagesForDevicePath:(NSString *)path;
-(NSArray *)getCalenderForDevicePath:(NSString *)path;
-(NSArray *)getCallHistoryForDevicePath:(NSString *)path;
-(NSArray *)getContactsForDevicePath:(NSString *)path;
-(NSArray *)getNotesForDevicePath:(NSString *)path;
-(NSArray *)getWhatsAppForDevicePath:(NSString *)path;


#pragma mark different functions to get the old contents.
-(NSArray *)getMessagesForOldDevicePath:(NSString *)path;
-(NSArray *)getCalenderForOldDevicePath:(NSString *)path;
-(NSArray *)getCallHistoryForOldDevicePath:(NSString *)path;
-(NSArray *)getContactsForOldDevicePath:(NSString *)path;
-(NSArray *)getNotesForOldDevicePath:(NSString *)path;
-(NSArray *)getWhatsAppForOldDevicePath:(NSString *)path;

-(NSString *)createNewFolderForOldData:(NSString *)path;
-(void)removeOldData:(NSString *)path;
@end
