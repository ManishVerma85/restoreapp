//
//  JSONReader.m
//  restor
//
//  Created by Umang on 05/12/14.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import "utf8Util.h"
#import "fileUtlities.h"

#import "JSONReader.h"

@implementation JSONReader

static JSONReader *shared;

////////////////////////////////////////////////////////////////////////
+ (instancetype)sharedReader
{
    static JSONReader *_sharedReader = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedReader = [[JSONReader alloc] init];
    });
    
    return _sharedReader;
}

////////////////////////////////////////////////////////////////////////
- (id)init
{
    self = [super init];
    
    if (self)
    {
        //[self populateDataForSelectedDeviceIndex:0];
    }
    
    return self;
}


-(NSString *)getPathForDeviceAtIndex:(NSInteger)index
{
    NSString *path = [self getDocumentDirectoryPath];
    if ([self getDevicesArray].count>0)
    {
        path = [path stringByAppendingPathComponent:[[self getDevicesArray] objectAtIndex:index]];
        return path;
    }
    return nil;
}

-(NSString *)populateDataForSelectedDeviceIndex:(NSInteger)index
{
    NSString *firstDevice = [self getDocumentDirectoryPath];
    firstDevice = [firstDevice stringByAppendingPathComponent:[[self getDevicesArray] firstObject]];
    return firstDevice;
    //NSLog(@"%@",[self getFilesForName:@"messages.json" inDirectory:firstDevice]);
}

-(NSArray *)getFilesForName:(NSString *)fileName inDirectory:(NSString *)strDirectoryPath
{
    NSArray *filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:strDirectoryPath  error:nil];
    NSPredicate *predictae = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"self CONTAINS[cd] '%@'",fileName]];
    filePathsArray = [filePathsArray filteredArrayUsingPredicate:predictae];
    
    NSMutableArray *arrOfPaths = [[NSMutableArray alloc] init];
    
    for (NSString *strFile in filePathsArray)
    {
        NSString *path = [strDirectoryPath stringByAppendingPathComponent:strFile];
        [arrOfPaths addObject:path];
    }
    
    return arrOfPaths;
}

-(NSArray *)getDevicesArray
{
    return [self getDocumentDirectoryContents];
}

-(NSMutableArray *)getDevicesNames
{
    NSArray *arr = [self getDocumentDirectoryContents];
    NSMutableArray *arrOfNames = [[NSMutableArray alloc] init];
    for (NSString *name in arr)
    {
        NSArray *splits = [name componentsSeparatedByString:@"#"];
        
        if (splits.count>1)
        {
            NSInteger firstIndexLength = [[splits firstObject] length];
            NSString *strSubString = [name substringFromIndex:firstIndexLength];
            
            
            //removes the # not needed shouldnt be shown
            if([strSubString hasPrefix:@"#"] && strSubString.length>1)
            {
                strSubString = [strSubString substringFromIndex:1];
            }
            
            
            [arrOfNames addObject:strSubString];
        }
        else
        {
            [arrOfNames addObject:name];
        }
    }
    return arrOfNames;
}

-(NSArray *)getDocumentDirectoryContents
{
    NSArray *filePathsArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[self getDocumentDirectoryPath] error:nil];
    
    NSPredicate *predictae = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"NOT self BEGINSWITH '.' AND NOT self ENDSWITH '-wal' AND NOT self ENDSWITH '-shm' AND NOT self ENDSWITH '.sqlite'"]];
    
    return [filePathsArray filteredArrayUsingPredicate:predictae];
}

-(NSString *)getDocumentDirectoryPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSLog(@"%@",paths);
    return documentsDirectory;
}

#pragma mark different functions to get the contents.
-(NSArray *)getMessagesForDevicePath:(NSString *)path
{
    NSMutableArray *arrOfMessages = [[NSMutableArray alloc] init];
    
    NSArray *arrFilesToRead = [self getFilesForName:@"messages.json" inDirectory:path];
    
    utf8Util *utfValidation = [[utf8Util alloc] init];
    
    for (NSString *filePath in arrFilesToRead)
    {
        [utfValidation Validateutf8File:filePath];
        
        NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        NSData *data = [content dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        if (json && [json objectForKey:@"conversation"])
        {
            for (NSDictionary *dic in [json objectForKey:@"conversation"])
            {
                Message *obj = [Message modelObjectWithDictionary:dic];
               // BOOL isFound = NO;
                /*
                for (Message *objPrevious in arrOfMessages)
                {
                    if ([obj.dictionaryRepresentation isEqualToDictionary:objPrevious.dictionaryRepresentation])
                    {
                        isFound = YES;
                        break;
                    }
                }
                */
                //if (!isFound)
              //  {
                    [arrOfMessages addObject:obj];
              //  }
            }
        }
    }
    return arrOfMessages;
}

-(NSArray *)getCalenderForDevicePath:(NSString *)path
{
    NSMutableArray *arrOfCalnder = [[NSMutableArray alloc] init];
    
    NSArray *arrFilesToRead = [self getFilesForName:@"calendar.json" inDirectory:path];
    
    utf8Util *utfValidation = [[utf8Util alloc] init];
    
    for (NSString *filePath in arrFilesToRead)
    {
        
        [utfValidation Validateutf8File:filePath];
        
        NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        NSData *data = [content dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        if (json && [json objectForKey:@"calendar"])
        {
            for (NSDictionary *dic in [json objectForKey:@"calendar"])
            {
                Calender *obj = [Calender modelObjectWithDictionary:dic];
                BOOL isFound = NO;
                /*
                for (Calender *objPrevious in arrOfCalnder)
                {
                    if ([obj isEqualToCalender:objPrevious])
                    {
                        isFound = YES;
                        break;
                    }
                }
                */
                if (!isFound)
                {
                    [arrOfCalnder addObject:obj];
                }
            }
        }
    }
    
    
    return arrOfCalnder;
}

-(NSArray *)getCallHistoryForDevicePath:(NSString *)path
{
    NSMutableArray *arrOfCalls = [[NSMutableArray alloc] init];
    
    NSArray *arrFilesToRead = [self getFilesForName:@"callhistory.json" inDirectory:path];
    
    utf8Util * utfValidation = [[utf8Util alloc] init];
    
    for (NSString *filePath in arrFilesToRead)
    {
        [utfValidation Validateutf8File:filePath];
        
        NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        NSData *data = [content dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
      
        if (json && [json objectForKey:@"callhistory"])
        {
            for (NSDictionary *dic in [json objectForKey:@"callhistory"])
            {
                CallHistory *obj = [CallHistory modelObjectWithDictionary:dic];
                BOOL isFound = NO;
                /*
                for (CallHistory *objPrevious in arrOfCalls)
                {
                    if ([obj isEqualToCallHistory:objPrevious])
                    {
                        isFound = YES;
                        break;
                    }
                }
                */
                if (!isFound)
                {
                    [arrOfCalls addObject:obj];
                }
            }
        }
    }
    return arrOfCalls;
}

-(NSArray *)getContactsForDevicePath:(NSString *)path
{
    NSMutableArray *arrOfContacts = [[NSMutableArray alloc] init];
    
    NSArray *arrFilesToRead = [self getFilesForName:@"contacts.json" inDirectory:path];
    
    utf8Util * utfValidation = [[utf8Util alloc] init];
    
    for (NSString *filePath in arrFilesToRead)
    {
        [utfValidation Validateutf8File:filePath];
        
        NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        NSData *data = [content dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        if (json && [json objectForKey:@"contacts"])
        {
            for (NSDictionary *dic in [json objectForKey:@"contacts"])
            {
                Contact *obj = [Contact modelObjectWithDictionary:dic];
                BOOL isFound = NO;
                /*
                for (Contact *objPrevious in arrOfContacts)
                {
                    if ([obj isEqualToContact:objPrevious])
                    {
                        isFound = YES;
                        break;
                    }
                }
                */
                if (!isFound)
                {
                    [arrOfContacts addObject:obj];
                }
            }
        }
    }
    return arrOfContacts;
}

-(NSArray *)getNotesForDevicePath:(NSString *)path
{
    NSMutableArray *arrOfNotes = [[NSMutableArray alloc] init];
    
    utf8Util * utfValidation = [[utf8Util alloc] init];
    
    //   NSString * fullPath = [[NSString alloc] initWithFormat:@"%@notes.json",path];
    
    // NSString *fullPath = [path stringByAppendingPathComponent:@"notes.json"];
    
    
    //there can be more than one file to be loaded for utf8
    NSArray *arrFilesToRead = [self getFilesForName:@"notes.json" inDirectory:path];
    
    
    for (NSString *filePath in arrFilesToRead)
    {
        
        
     //   [utf8Util insertUTF8TestDataIntoFile:filePath atPosition:10];
        
        [utfValidation Validateutf8File:filePath];
        
        NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        
        NSData *data = [content dataUsingEncoding:NSUTF8StringEncoding];
        NSError * error;
        
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        
        if (json && [json objectForKey:@"notes"])
        {
            for (NSDictionary *dic in [json objectForKey:@"notes"])
            {
                Notes *obj = [Notes modelObjectWithDictionary:dic];
                BOOL isFound = NO;
                /*
                for (Notes *objPrevious in arrOfNotes)
                {
                    if ([obj isEqualToNotes:objPrevious])
                    {
                        isFound = YES;
                        break;
                    }
                }
                */
                if (!isFound)
                {
                    [arrOfNotes addObject:obj];
                }
            }
        }
    }
    
    return arrOfNotes;
}





-(NSArray *)getWhatsAppForDevicePath:(NSString *)path
{
    NSMutableArray *arrOfWhatsApp = [[NSMutableArray alloc] init];
    

    utf8Util *utfValidation = [[utf8Util alloc] init];
    
    
    
    NSArray *arrFilesToRead = [self getFilesForName:@"whatsapp.json" inDirectory:path];
    for (NSString *filePath in arrFilesToRead)
    {
        
      //  [utf8Util insertUTF8TestDataIntoFile:filePath atPosition:10];
    
        [utfValidation Validateutf8File:filePath];
        
        
        NSError *error;
        NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
        
        if (error==nil)
        {
            NSData *data = [content dataUsingEncoding:NSUTF8StringEncoding];
            id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            if (json && [json objectForKey:@"conversation"] && error==nil)
            {
                for (NSDictionary *dic in [json objectForKey:@"conversation"])
                {
                   // BOOL isFound = NO;
                    /*
                     for (Whatsapp *objPrevious in arrOfWhatsApp)
                     {
                     if ([obj.dictionaryRepresentation isEqualToDictionary:objPrevious.dictionaryRepresentation])
                     {
                     isFound = YES;
                     break;
                     }
                     }
                     */
                 //   if (!isFound)
                 //   {
                        [arrOfWhatsApp addObject:[Whatsapp modelObjectWithDictionary:dic]];
                  //  }
                }
            }
            else
            {
                NSLog(@"error while json");
            }
            
            data = nil;
        }
        else
        {
            NSLog(@"error reading file");
        }
        
        content = nil;
        
    }
    
   return arrOfWhatsApp;
}

#pragma mark different functions to get the old contents.

-(NSString *)createNewFolderForOldData:(NSString *)path
{
    NSString *newPath = [[self getDocumentDirectoryPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"XXXX#%@",[path lastPathComponent]]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:newPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:newPath withIntermediateDirectories:NO attributes:nil error:nil];
    }
    
    return newPath;
}

-(void)removeOldData:(NSString *)path
{
    for (NSString *file in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil])
    {
        [[NSFileManager defaultManager] removeItemAtPath:[path stringByAppendingPathComponent:file] error:nil];
    }
    [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
}

-(NSArray *)getMessagesForOldDevicePath:(NSString *)path
{
    NSMutableArray *arrOfMessages = [[NSMutableArray alloc] init];
    
    NSString *filePath = [path stringByAppendingPathComponent:@"messages.plist"];
    id data = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    
    if (data && [data objectForKey:@"conversation"])
    {
        for (NSDictionary *dic in [data objectForKey:@"conversation"])
        {
            Message *obj = [Message modelObjectWithDictionary:dic];
            [arrOfMessages addObject:obj];
        }
    }
    
    
    if (arrOfMessages.count>0)
    {
        NSMutableArray *arrOfDataToAdd = [[NSMutableArray alloc] init];
        for (Message *object in arrOfMessages)
        {
            [arrOfDataToAdd addObject:object.dictionaryRepresentation];
        }
        NSDictionary *dic = @{@"conversation": arrOfDataToAdd};
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:nil];
        NSString *strData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [strData writeToFile:[[self createNewFolderForOldData:path] stringByAppendingPathComponent:@"messages.json"] atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
    
    return arrOfMessages;
}

-(NSArray *)getCalenderForOldDevicePath:(NSString *)path
{
    NSMutableArray *arrOfCalnder = [[NSMutableArray alloc] init];
    
    NSString *filePath = [path stringByAppendingPathComponent:@"calendar.plist"];
    id data = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    
    
    if (data && [data objectForKey:@"calendar"])
    {
        for (NSDictionary *dic in [data objectForKey:@"calendar"])
        {
            [arrOfCalnder addObject:[Calender modelObjectWithDictionary:dic]];
        }
    }
    
    if (arrOfCalnder.count>0)
    {
        NSMutableArray *arrOfDataToAdd = [[NSMutableArray alloc] init];
        for (Calender *object in arrOfCalnder)
        {
            [arrOfDataToAdd addObject:object.dictionaryRepresentation];
        }
        NSDictionary *dic = @{@"calendar": arrOfDataToAdd};
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:nil];
        NSString *strData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [strData writeToFile:[[self createNewFolderForOldData:path] stringByAppendingPathComponent:@"calendar.json"] atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
    
    return arrOfCalnder;
}

-(NSArray *)getCallHistoryForOldDevicePath:(NSString *)path
{
    NSMutableArray *arrOfCalls = [[NSMutableArray alloc] init];
    
    NSString *filePath = [path stringByAppendingPathComponent:@"callhistory.plist"];
    id data = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    
    
    if (data && [data objectForKey:@"callhistory"])
    {
        for (NSDictionary *dic in [data objectForKey:@"callhistory"])
        {
            [arrOfCalls addObject:[CallHistory modelObjectWithDictionary:dic]];
        }
    }
    
    if (arrOfCalls.count>0)
    {
        NSMutableArray *arrOfDataToAdd = [[NSMutableArray alloc] init];
        for (CallHistory *object in arrOfCalls)
        {
            [arrOfDataToAdd addObject:object.dictionaryRepresentation];
        }
        NSDictionary *dic = @{@"callhistory": arrOfDataToAdd};
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:nil];
        NSString *strData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [strData writeToFile:[[self createNewFolderForOldData:path] stringByAppendingPathComponent:@"callhistory.json"] atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
    
    
    return arrOfCalls;
}

-(NSArray *)getContactsForOldDevicePath:(NSString *)path
{
    NSMutableArray *arrOfContacts = [[NSMutableArray alloc] init];
    
    NSString *filePath = [path stringByAppendingPathComponent:@"contacts.plist"];
    id data = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    
    
    if (data && [data objectForKey:@"contacts"])
    {
        for (NSDictionary *dic in [data objectForKey:@"contacts"])
        {
            [arrOfContacts addObject:[Contact modelObjectWithDictionary:dic]];
        }
    }
    
    if (arrOfContacts.count>0)
    {
        NSMutableArray *arrOfDataToAdd = [[NSMutableArray alloc] init];
        for (Contact *object in arrOfContacts)
        {
            [arrOfDataToAdd addObject:object.dictionaryRepresentation];
        }
        NSDictionary *dic = @{@"contacts": arrOfDataToAdd};
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:nil];
        NSString *strData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [strData writeToFile:[[self createNewFolderForOldData:path] stringByAppendingPathComponent:@"contacts.json"] atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
    
    return arrOfContacts;
}

-(NSArray *)getNotesForOldDevicePath:(NSString *)path
{
    NSMutableArray *arrOfNotes = [[NSMutableArray alloc] init];
    
    NSString *filePath = [path stringByAppendingPathComponent:@"notes.plist"];
    id data = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    
    
    if (data && [data objectForKey:@"notes"])
    {
        for (NSDictionary *dic in [data objectForKey:@"notes"])
        {
            [arrOfNotes addObject:[Notes modelObjectWithDictionary:dic]];
        }
    }
    
    if (arrOfNotes.count>0)
    {
        NSMutableArray *arrOfDataToAdd = [[NSMutableArray alloc] init];
        for (Notes *object in arrOfNotes) {
            [arrOfDataToAdd addObject:object.dictionaryRepresentation];
        }
        NSDictionary *dic = @{@"notes": arrOfDataToAdd};
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:nil];
        NSString *strData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [strData writeToFile:[[self createNewFolderForOldData:path] stringByAppendingPathComponent:@"notes.json"] atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
    
    return arrOfNotes;
}

-(NSArray *)getWhatsAppForOldDevicePath:(NSString *)path
{
    NSMutableArray *arrOfWhatsApp = [[NSMutableArray alloc] init];
    
    NSString *filePath = [path stringByAppendingPathComponent:@"whatsapp.plist"];
    id data = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    
    
    if (data && [data objectForKey:@"conversation"])
    {
        for (NSDictionary *dic in [data objectForKey:@"conversation"])
        {
            [arrOfWhatsApp addObject:[Whatsapp modelObjectWithDictionary:dic]];
        }
    }
    
    if (arrOfWhatsApp.count>0)
    {
        NSMutableArray *arrOfDataToAdd = [[NSMutableArray alloc] init];
        for (Whatsapp *object in arrOfWhatsApp)
        {
            [arrOfDataToAdd addObject:object.dictionaryRepresentation];
        }
        NSDictionary *dic = @{@"conversation": arrOfDataToAdd};
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:nil];
        NSString *strData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [strData writeToFile:[[self createNewFolderForOldData:path] stringByAppendingPathComponent:@"whatsapp.json"] atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
    
    return arrOfWhatsApp;
}

@end
