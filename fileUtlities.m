//
//  fileUtlities.m
//  restor
//
//  Created by Andy on 22/07/2015.
//  Copyright (c) 2015 AgamPuram. All rights reserved.
//

#import "fileUtlities.h"

@implementation fileUtlities





- (NSData*)readDataFromFileAtURL:(NSURL*)anURL
{
    NSString* filePath = [anURL path];
    int fd = open([filePath UTF8String], O_RDONLY);
    
    if (fd == -1)
    {
        return nil;
    }
    
    NSMutableData* theData = [[NSMutableData alloc] initWithLength:1024];
    
    if (theData)
    {
        void* buffer = [theData mutableBytes];
        NSUInteger bufferSize = [theData length];
        
        NSUInteger actualBytes = read(fd, buffer, bufferSize);
        if (actualBytes < 1024)
        {
            [theData setLength:actualBytes];
        }
    }
    
    close(fd);
    return theData;
}




- (BOOL)setFileToChunkFile:(NSString *)filePath withChunkSize:(int)chunkSize
{
    BOOL fileOpened= NO;
    
    //open the file
    _fileDescriptor = open([filePath UTF8String], O_RDONLY);
    
    if (_fileDescriptor == -1)
    {
        return fileOpened;
    }
    else
    {
        fileOpened =YES;
    }
    
    
    //check if open is good
    
    //set start position 0
    _filePosition=0;
    _chunkSize=chunkSize;
    
    return fileOpened;
}



- (NSData*)readFileChunk
{
    
    if (_fileDescriptor == -1)
    {
        return nil;
    }
    
    NSMutableData* theData = [[NSMutableData alloc] initWithLength:_chunkSize];
    
    if (theData)
    {
        void* buffer = [theData mutableBytes];
        NSUInteger bufferSize = [theData length];
        
        
//        lseek(, <#off_t#>, <#int#>)
        
        NSUInteger actualBytes = read(_fileDescriptor, buffer, bufferSize);
        
        if (actualBytes < _chunkSize)
        {
            [theData setLength:actualBytes];
        }
    }
    

    return theData;
}


-(BOOL)closeFile
{
    close(_fileDescriptor);

    return YES;
}


- (NSData*)readChunkofFile:(NSURL*)anURL
{
    NSString* filePath = [anURL path];
    int fd = open([filePath UTF8String], O_RDONLY);
    
    if (fd == -1)
    {
        return nil;
    }
    
    NSMutableData* theData = [[NSMutableData alloc] initWithLength:1024];
    
    if (theData)
    {
        void* buffer = [theData mutableBytes];
        NSUInteger bufferSize = [theData length];
        
        NSUInteger actualBytes = read(fd, buffer, bufferSize);
        if (actualBytes < 1024)
        {
            [theData setLength:actualBytes];
        }
    }
    
    close(fd);
    return theData;
}


+(unsigned long long) fileSizeAtPath:(NSString *)fileName
{
    NSFileManager *man = [NSFileManager defaultManager];
    NSDictionary *attrs = [man attributesOfItemAtPath: fileName error: NULL];
    
    
    
    return [attrs fileSize];
}




+(BOOL)replaceFile:(NSString *)initialFile withThisFile:(NSString *) newFileToReplaceWith
{
    BOOL didReplace=NO;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    // NSArray *appsDirectory = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // NSString *documentPath = [appsDirectory objectAtIndex:0];

    NSError * error;
    
    if ([fileManager fileExistsAtPath:initialFile] == YES)
    {
        [fileManager removeItemAtPath:initialFile error:&error];
        
            //  NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"txtFile" ofType:@"txt"];
        [fileManager copyItemAtPath:newFileToReplaceWith toPath:initialFile error:&error];
        
        didReplace=YES;
    }



    return didReplace;
}


+(BOOL)deleteFile:(NSString *)initialFile
{
    BOOL didDelete=NO;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    // NSArray *appsDirectory = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // NSString *documentPath = [appsDirectory objectAtIndex:0];
    
    NSError * error;
    
    if ([fileManager fileExistsAtPath:initialFile] == YES)
    {
        didDelete = [fileManager removeItemAtPath:initialFile error:&error];
        
    }
    
    return didDelete;
}



+(BOOL)insertIntoFile:(NSString *)filePath  Data:(NSData *)dat atPosition:(unsigned long long)pos
{
    BOOL successful=NO;
    
    

    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError * error;
    
    //check file exists
    if ([fileManager fileExistsAtPath:filePath] == YES)
    {
        //successful = [fileManager removeItemAtPath:initialFile error:&error];
        
        unsigned long long fileSize = [self fileSizeAtPath:filePath];
        
        
        if (pos<fileSize )
        {
            //can insert
            NSData * fileData = [[NSData alloc] initWithContentsOfFile:filePath];
            
            
            NSData * firstPartOfData = [fileData subdataWithRange:NSMakeRange(0,(unsigned int)pos)];
            
            NSData * lastPartOfData = [fileData subdataWithRange:NSMakeRange((unsigned int)pos, fileData.length-(unsigned int)pos)];
            
            
            
            //build up the new data
            NSMutableData * fullData =[[NSMutableData alloc] initWithData:firstPartOfData];
            
            [fullData appendData:dat];
            
            [fullData appendData:lastPartOfData];
            
            
            //write the file out
            [fullData writeToFile:filePath options:NSDataWritingAtomic error:&error];
            
            if (error==nil)
            {
                successful=YES;
            }
            
            
        }
        else
        {
            //not possible to insert
            successful =NO;
        }
        
    }
    
    
    return successful;
}


+(BOOL)appendtoFile:(NSString *) filePath thisData:(NSData *)dataToAppend
{
    BOOL appendedOK=NO;
    
    NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
    
    if (fileHandle!=nil)
    {
        [fileHandle seekToEndOfFile];
        [fileHandle writeData:dataToAppend];
        [fileHandle closeFile];
        appendedOK=YES;
    }

    return appendedOK;
}


+(int)getNumberDirectories:(NSString *)folderPath
{
    NSFileManager *fm = [NSFileManager defaultManager];

    NSArray *filelist= [fm contentsOfDirectoryAtPath:folderPath error:nil];

    int numDirectories=0;
    
    
    BOOL isDirectory;
    for (NSString *item in filelist)
    {
        NSString * fullPath = [folderPath stringByAppendingPathComponent:item];
        
        BOOL fileExistsAtPath = [[NSFileManager defaultManager] fileExistsAtPath:fullPath isDirectory:&isDirectory];
        if (fileExistsAtPath) {
            if (isDirectory)
            {
                //It's a Directory.
                numDirectories++;
            }
        }
        if ([[item pathExtension] isEqualToString:@"png"]) {
            //This is Image File with .png Extension
        }
    }
    
    
    return numDirectories;
}

@end
