//
//  AppDelegate.h
//  Restor
//
//  Created by Deva Palanisamy on 18/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "SocialViewController.h"
#import "SearchViewController.h"
#import "SettingsViewController.h"
//#import "LoginViaCodeViewController.h"
#import "WebViewController.h"



FOUNDATION_EXPORT NSString *const dataFound;

#define APPDELEGATE ((AppDelegate*)[UIApplication sharedApplication].delegate)




@class HomeViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate,iRateDelegate>
{
    HomeViewController* homeViewController;
    SocialViewController* socialViewController;
    SearchViewController* searchViewController;
    SettingsViewController* settingsViewController;
    
    NSArray* viewControllersList;
    UITabBarController* tabBarController;
    UINavigationController *homeNavController;
    UINavigationController *socialNavController;
    UINavigationController *searchNavController;
    UINavigationController *settingsNavController;
    UINavigationController *firstNavController;
    
    NSMutableArray *messageArray;
    NSMutableArray *notesArray;
    NSMutableArray *calenderArray;
    NSMutableArray *callsHistoryArray;
    NSMutableArray *whatsappMessagesArray;
    NSMutableArray *contactsArray;
    
    NSMutableDictionary *contentDictionary;
    NSUserDefaults *defaults;
    
    UINavigationController *loginNavController;
  //  LoginViaCodeViewController *loginViaCodeViewController;

}

@property (strong,nonatomic) UITabBarController* tabBarController;
//@property (strong,nonatomic) LoginViaCodeViewController *loginViaCodeViewController;
@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic,strong) NSMutableArray *messageArray;
@property (nonatomic,strong) NSMutableArray *notesArray;
@property (nonatomic,strong) NSMutableArray *calenderArray;
@property (nonatomic,strong) NSMutableArray *callsHistoryArray;
@property (nonatomic,strong) NSMutableArray *whatsappMessagesArray;
@property (nonatomic,strong) NSMutableArray *contactsArray;
@property (nonatomic,strong) NSMutableDictionary *contentDictionary;


@property (nonatomic, assign) BOOL didUserRequestForRemindLaterOption;

@property (atomic, strong) NSString * currentlySelectedDevice;

@property(nonatomic, strong) id<GAITracker> tracker;


- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

-(void)setRootViewControllerToHomeViewController;
//-(void)setRootViewControllerToLoginViewController;
//- (void)setRootViewControllerToFirstTimeLoginViewController;
@property (assign,nonatomic) BOOL enablePortrait;

-(void)showTabbarController;

- (void)showiRatePopUp;
@end
