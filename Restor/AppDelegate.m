//
//  AppDelegate.m
//  Restor
//
//  Created by Deva Palanisamy on 18/05/2014.
//
//

#import <UIKit/UIKit.h>
#import "ACTReporter.h"
#import "AppDelegate.h"
#import "AFHTTPRequestOperation.h"
#import "Flurry.h"
#import "LaunchViewController.h"
#import "HTProgressHUD.h"



@interface UIImage(Overlay)
@end

@implementation UIImage(Overlay)

- (UIImage *)imageWithColor:(UIColor *)color1
{
    UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, self.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextClipToMask(context, rect, self.CGImage);
    [color1 setFill];
    CGContextFillRect(context, rect);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
@end




/******* Set your tracking ID here *******/
static NSString *const kTrackingId      = @"UA-33682002-2";//@"UA-58141292-1";//@"UA-TRACKING-ID";
static NSString *const kAllowTracking   = @"allowTracking";
NSString *const dataFound = @"dataFound";

//https://github.com/nicklockwood/iRate



#define MIXPANEL_TOKEN @"6014c693895b654c4817566311543c4b"
@implementation AppDelegate
@synthesize messageArray, notesArray, whatsappMessagesArray, callsHistoryArray,
contactsArray, calenderArray, contentDictionary,
enablePortrait,tabBarController;//,loginViaCodeViewController;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

@synthesize didUserRequestForRemindLaterOption;

-(BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
   /* [UIView animateWithDuration:5.0
                          delay:0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                        // self.window.viewForBaselineLayout.alpha = 0; // and at this alpha
                     }
                     completion:^(BOOL finished){
                     }];
    */
    return YES;
    
}

- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{    
    
       [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    //mv
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    
    //iRateUserDidDeclineToRateApp
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userRequestedRemindLaterOption) name:iRateUserDidRequestReminderToRateApp object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDeclinedToRateApp) name:iRateUserDidDeclineToRateApp object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(DataFound)
                                                 name:dataFound object:nil];
    
    
    
    //mv
    [APPDELEGATE dispatchEvent:[[GAIDictionaryBuilder createEventWithCategory:@"UI" action:@"App launched" label:nil value:nil] build]];
    
    //add google tracking
    NSDictionary *appDefaults = @{kAllowTracking: @(YES)};
    [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
    // User must be able to opt out of tracking
    [GAI sharedInstance].optOut =
    ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
    // Initialize Google Analytics with a 120-second dispatch interval. There is a
    // tradeoff between battery usage and timely dispatch.
    [GAI sharedInstance].dispatchInterval = 120;
    //    [GAI sharedInstance].trackUncaughtExceptions = YES;
    self.tracker = [[GAI sharedInstance] trackerWithName:@"RestoreApp"
                                              trackingId:kTrackingId];
    
    
    // Enable IDFA collection.
    [self.tracker setAllowIDFACollection:YES];
    
    [iRate sharedInstance].delegate = self;
//    [self showiRatePopUp];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
 /*   [self.window makeKeyAndVisible];
    
    self.window.viewForBaselineLayout.alpha = 0.1;
    [UIView animateWithDuration:10
                          delay:0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                         float f=0.0f;
                         for (int iloopcounter=0; iloopcounter<100; iloopcounter++)
                         {
                             f=f+0.01f;
                             self.window.viewForBaselineLayout.alpha = f; // and at this alpha
                             sleep(0.15f);
                         }
                         
                     }
                     completion:^(BOOL finished){
                     }];
   */
    [Mixpanel sharedInstanceWithToken:MIXPANEL_TOKEN];
    contentDictionary = [[NSMutableDictionary alloc] init];
    
    [self initiateViewControllers];
    //[self setArraysForHomeView];
    defaults = [NSUserDefaults standardUserDefaults];
    
    
  /*  if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isFirstLaunch"])
    {
        LaunchViewController *controller = [[LaunchViewController alloc] initWithNibName:@"LaunchViewController" bundle:nil];
        UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:controller];
        [navigation setNavigationBarHidden:YES];
        self.window.rootViewController=navigation;
    }
    else
    {
        if ([[JSONReader sharedReader] getDevicesArray].count>0)
        {
            [self showTabbarController];
        }
        else
        {
            NODataViewController *controller = [[NODataViewController alloc] initWithNibName:@"NODataViewController" bundle:nil];
            UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:controller];
            [navigation setNavigationBarHidden:YES];
            self.window.rootViewController=navigation;
        }
    }*/
    
    if ([[JSONReader sharedReader] getDevicesArray].count>0)
    {
        [self showTabbarController];
    }
    else
    {
        LaunchViewController *controller = [[LaunchViewController alloc] initWithNibName:@"LaunchViewController" bundle:nil];
        UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:controller];
        [navigation setNavigationBarHidden:YES];
        self.window.rootViewController=navigation;
        
        /*
        NODataViewController *controller = [[NODataViewController alloc] initWithNibName:@"NODataViewController" bundle:nil];
        UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:controller];
        [navigation setNavigationBarHidden:YES];
        self.window.rootViewController=navigation;*/
    }

    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceSelected:) name:@"deviceSelected" object:nil];
    
    [Flurry setCrashReportingEnabled:YES];
    
    [Flurry startSession:@"FX3MN44G899Y8BRJ3S5W"];
    
    [Flurry logEvent:@"App opened"];
    
    
    [ACTConversionReporter reportWithConversionID:@"996341473" label:@"3FyJCJTckF0Q4e2L2wM" value:@"40.00" isRepeatable:NO];
    
//ACTAutomatedUsageTracker
    
    // self.window.rootViewController=tabBarController;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    
    return YES;
}


-(void)deviceSelected:(NSNotification *)notification
{

    self.currentlySelectedDevice = [notification object];
    
    [self setRootViewControllerToHomeViewController];
    
}



-(void)DataFound
{
    sleep(1);
    
    [self showTabbarController];
    
}


- (void)showiRatePopUp
{
    //mv
    //set the bundle ID. normally you wouldn't need to do this
    //as it is picked up automatically from your Info.plist file
    //but we want to test with an app that's actually on the store
    [iRate sharedInstance].appStoreID = 910568421;
    [iRate sharedInstance].applicationBundleID = @"74Y2QYH7DF";
    [iRate sharedInstance].onlyPromptIfLatestVersion = NO;
    
    [[iRate sharedInstance] setPromptAtLaunch:NO];
    [[iRate sharedInstance] setPromptForNewVersionIfUserRated:YES];
    
    //enable preview mode
    [iRate sharedInstance].previewMode = YES;
    
    
    //just placed here to be picked for localisation
    //uncomment when doing localisation
 /*   NSString * iRateMessageTitle = NSLocalizedString(@"Rate %@",@"");
    NSString * iRateAppMessage = NSLocalizedString(@"If you enjoy using %@, would you mind taking a moment to rate it? It won’t take more than a minute. Thanks for your support!",@"");
    NSString * iRateGameMessage = NSLocalizedString(@"If you enjoy playing %@, would you mind taking a moment to rate it? It won’t take more than a minute. Thanks for your support!",@"");
    NSString * iRateCancelButton = NSLocalizedString(@"No, Thanks",@"");
    NSString * iRateRateButton = NSLocalizedString(@"Rate It Now",@"");
    NSString * iRateRemindButton = NSLocalizedString(@"Remind Me Later",@"");
   */
}

- (void)userRequestedRemindLaterOption
{
    NSLog(@"iRateUserDidRequestReminderToRateApp");
    
    didUserRequestForRemindLaterOption = YES;
}

- (void)userDeclinedToRateApp
{
    NSLog(@"iRateUserDidDeclineToRateApp");
    
    didUserRequestForRemindLaterOption = NO;
}

-(void)showTabbarController
{
    
       UIColor *backGroundColor = [UIColor colorWithRed:54.0f/255.0f
                    green:62.0f/255.0f
                     blue:73.0f/255.0f
                    alpha:1.0f];

    
    UIColor *selectedColour = [UIColor colorWithRed:38.0f/255.0f
                                               green:150.0f/255.0f
                                                blue:219.0f/255.0f
                                               alpha:1.0f];
    
    
  //  homeNavController.navigationBar.tintColor = [UIColor whiteColor];//[UIColor colorWithRed:(26.0f/255.0f) green:(103.0f/255.0f) blue:(159.0f/255.0f) alpha:1.0f]
    
  //  homeNavController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    
    
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,nil];
    
    homeNavController.navigationBar.titleTextAttributes = textAttributes;
  //  self.title = @"Title of the Page";
    
    
    
    homeNavController.navigationBar.barTintColor = backGroundColor;//[UIColor colorWithRed:(26.0f/255.0f) green:(103.0f/255.0f) blue:(159.0f/255.0f) alpha:1.0f]
    searchNavController.navigationBar.barTintColor = backGroundColor;//[UIColor colorWithRed:(26.0f/255.0f) green:(103.0f/255.0f) blue:(159.0f/255.0f) alpha:1.0f]
    socialNavController.navigationBar.barTintColor = backGroundColor;//[UIColor colorWithRed:(26.0f/255.0f) green:(103.0f/255.0f) blue:(159.0f/255.0f) alpha:1.0f]
    settingsNavController.navigationBar.barTintColor = backGroundColor;//[UIColor colorWithRed:(26.0f/255.0f) green:(103.0f/255.0f) blue:(159.0f/255.0f) alpha:1.0f]
    

    viewControllersList = [NSArray
                           arrayWithObjects:homeNavController, searchNavController,
                           // shareNavController,
                           socialNavController, settingsNavController, nil];
    tabBarController = [[UITabBarController alloc] init];
    tabBarController.viewControllers = viewControllersList;
    tabBarController.selectedIndex = 0;
    
    [homeNavController.navigationBar setTintColor:[UIColor whiteColor]];
    
    
    //tabBarController.tintColor = [UIColor whiteColor];
    //tabBarController.backgroundTintColor = [UIColor blackColor];
    
    //[tabBarController setb]
    
 //   [tabBarController.tabBar setTintColor:[UIColor whiteColor]];
 
    
    [tabBarController.tabBar setBarTintColor:backGroundColor];
    
  //  [[UIView appearanceWhenContainedIn:[UITabBar class], nil] setTintColor:[UIColor whiteColor]];
  //  [[UITabBar appearance] setSelectedImageTintColor:[UIColor blueColor]];
    
    //   [tabBarController.tabBar setBackgroundColor:[UIColor whiteColor]];
    
    
    
    
    
    // set the selected colors
    [tabBarController.tabBar  setTintColor:selectedColour];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    
    
    UIColor * unselectedColor = [UIColor whiteColor];//[UIColor colorWithRed:184/255.0f green:224/255.0f blue:242/255.0f alpha:1.0f];
    
    
    // set color of unselected text
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:unselectedColor, NSForegroundColorAttributeName, nil]
                                             forState:UIControlStateNormal];
    
    // generate a tinted unselected image based on image passed via the storyboard
    for(UITabBarItem *item in tabBarController.tabBar.items)
    {
        // use the UIImage category code for the imageWithColor: method
        item.image = [[item.selectedImage imageWithColor:unselectedColor] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        
        [item   setTitleTextAttributes:@{ NSForegroundColorAttributeName : selectedColour }
        forState:UIControlStateSelected];
    
    }
    
    
    //need to set the current device for the first time
    NSString *strFirstDevice = [[JSONReader sharedReader] getPathForDeviceAtIndex:0];
    if (strFirstDevice)
    {
        
        [RestoreManager sharedManager].currentDevice = [[Device alloc] initWithFilePath:strFirstDevice];
        
    }
    
    
    
    
    self.window.rootViewController=tabBarController;
}


/*- (NSUInteger)application:(UIApplication *)application
 supportedInterfaceOrientationsForWindow:(UIWindow *)window {
 if (!enablePortrait)
 return UIInterfaceOrientationMaskLandscape |
 UIInterfaceOrientationMaskPortrait;
 return UIInterfaceOrientationMaskPortrait;
 }*/

- (void)applicationWillResignActive:(UIApplication *)application {
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [APPDELEGATE dispatchEvent:[[GAIDictionaryBuilder createEventWithCategory:@"UI" action:@"App became active" label:nil value:nil] build]];
    
    
    [GAI sharedInstance].optOut =
    ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Quit"];
    
    if ([[JSONReader sharedReader] getDevicesArray].count == 0)
    {
        exit(0);
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    if (self.didUserRequestForRemindLaterOption)
    {
        //show iRate pop up
        [self showiRatePopUp];
    }
   
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [self saveContext];
}

- (void)saveContext {
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] &&
            ![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack


- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL =
    [[NSBundle mainBundle] URLForResource:@"Restor" withExtension:@"momd"];
    _managedObjectModel =
    [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory]
                       URLByAppendingPathComponent:@"Restor.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                   initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                   configuration:nil
                                                             URL:storeURL
                                                         options:nil
                                                           error:&error]) {
        
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager]
             URLsForDirectory:NSDocumentDirectory
             inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Initiate view controllers
- (void)initiateViewControllers {
    homeViewController = [[HomeViewController alloc] init];
    socialViewController = [[SocialViewController alloc] init];
    searchViewController = [[SearchViewController alloc] init];
    settingsViewController = [[SettingsViewController alloc] init];
    
    homeNavController = [[UINavigationController alloc]
                         initWithRootViewController:homeViewController];
    socialNavController = [[UINavigationController alloc]
                           initWithRootViewController:socialViewController];
    searchNavController = [[UINavigationController alloc]
                           initWithRootViewController:searchViewController];
    settingsNavController = [[UINavigationController alloc]
                             initWithRootViewController:settingsViewController];
    
    homeNavController.navigationBar.translucent = NO;
    socialNavController.navigationBar.translucent = NO;
    searchNavController.navigationBar.translucent = NO;
    settingsNavController.navigationBar.translucent = NO;
}

- (void)setupMixpanel {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    NSString *mixpanelUUID = [ud objectForKey:@"MixpanelUUID"];
    
    if (!mixpanelUUID) {
        mixpanelUUID = [[NSUUID UUID] UUIDString];
        [ud setObject:mixpanelUUID forKey:@"MixpanelUUID"];
    }
    [mixpanel identify:mixpanelUUID];
    NSDictionary *properties = @{ @"APIVersion" : @"1.0" };
    [mixpanel registerSuperProperties:properties];
}

- (void)setJsonArrayForMessages {
    NSString *filePath =
    [[NSBundle mainBundle] pathForResource:@"messages" ofType:@"json"];
    NSError *error = nil;
    NSData *data1 = [[NSFileManager defaultManager] contentsAtPath:filePath];
    messageArray =
    [NSJSONSerialization JSONObjectWithData:data1
                                    options:NSJSONReadingMutableContainers
                                      error:&error];
    if (error) {
        NSLog(@"Json parsing error: %@", [error localizedDescription]);
    } else {
        NSLog(@"Json is : %@", [messageArray description]);
    }
    [contentDictionary setValue:messageArray forKey:@"messages"];
}
- (void)setJsonArrayForNotes {
    NSString *filePath =
    [[NSBundle mainBundle] pathForResource:@"notes" ofType:@"json"];
    NSError *error = nil;
    NSData *data1 = [[NSFileManager defaultManager] contentsAtPath:filePath];
    notesArray =
    [NSJSONSerialization JSONObjectWithData:data1
                                    options:NSJSONReadingMutableContainers
                                      error:&error];
    if (error) {
        NSLog(@"Json parsing error: %@", [error localizedDescription]);
    } else {
        NSLog(@"Json is : %@", [notesArray description]);
    }
    [contentDictionary setValue:notesArray forKey:@"notes"];
}

- (void)setJsonArrayForCalender {
    NSString *filePath =
    [[NSBundle mainBundle] pathForResource:@"calender" ofType:@"json"];
    NSError *error = nil;
    NSData *data1 = [[NSFileManager defaultManager] contentsAtPath:filePath];
    calenderArray =
    [NSJSONSerialization JSONObjectWithData:data1
                                    options:NSJSONReadingMutableContainers
                                      error:&error];
    if (error) {
        NSLog(@"Json parsing error: %@", [error localizedDescription]);
    } else {
        NSLog(@"Json is : %@", [calenderArray description]);
    }
    [contentDictionary setValue:calenderArray forKey:@"calendar"];
}

- (void)setJsonArrayForCallsHistory {
    NSString *filePath =
    [[NSBundle mainBundle] pathForResource:@"callHistory" ofType:@"json"];
    NSError *error = nil;
    NSData *data1 = [[NSFileManager defaultManager] contentsAtPath:filePath];
    callsHistoryArray =
    [NSJSONSerialization JSONObjectWithData:data1
                                    options:NSJSONReadingMutableContainers
                                      error:&error];
    if (error) {
        NSLog(@"Json parsing error: %@", [error localizedDescription]);
    } else {
        NSLog(@"Json is : %@", [callsHistoryArray description]);
    }
    [contentDictionary setValue:callsHistoryArray forKey:@"callshistory"];
}

- (void)setJsonArrayForWhatsAppChat {
    NSString *filePath =
    [[NSBundle mainBundle] pathForResource:@"whatsappMessages"
                                    ofType:@"json"];
    NSError *error = nil;
    NSData *data1 = [[NSFileManager defaultManager] contentsAtPath:filePath];
    whatsappMessagesArray =
    [NSJSONSerialization JSONObjectWithData:data1
                                    options:NSJSONReadingMutableContainers
                                      error:&error];
    if (error) {
        NSLog(@"Json parsing error: %@", [error localizedDescription]);
    } else {
        NSLog(@"Json is : %@", [whatsappMessagesArray description]);
    }
    [contentDictionary setValue:whatsappMessagesArray forKey:@"whatsapp"];
}

- (void)setJsonArrayForContacts {
    NSString *filePath =
    [[NSBundle mainBundle] pathForResource:@"contacts" ofType:@"json"];
    NSError *error = nil;
    NSData *data1 = [[NSFileManager defaultManager] contentsAtPath:filePath];
    contactsArray =
    [NSJSONSerialization JSONObjectWithData:data1
                                    options:NSJSONReadingMutableContainers
                                      error:&error];
    if (error) {
        NSLog(@"Json parsing error: %@", [error localizedDescription]);
    } else {
        NSLog(@"Json is : %@", [contactsArray description]);
    }
    [contentDictionary setValue:contactsArray forKey:@"contacts"];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return NO;
}



- (void)showMessage:(NSString *)text withTitle:(NSString *)title
{
    [[[UIAlertView alloc] initWithTitle:title
                                message:text
                               delegate:self
                      cancelButtonTitle:NSLocalizedString(@"OK",@"")
                      otherButtonTitles:nil] show];
}

#pragma mark - Set Root View Controller Based on Authentication
- (void)setRootViewControllerToHomeViewController
{
    self.window.rootViewController = tabBarController;
    tabBarController.selectedIndex = 0;
}
/*
- (void)setRootViewControllerToLoginViewController
{
    self.window.rootViewController = loginNavController;
}

- (void)setRootViewControllerToFirstTimeLoginViewController
{
    self.window.rootViewController = loginViaCodeViewController;
}
 */


#pragma mark - getting user id and token using Facebook access token
- (void)getUserIDandTokenWithFacebookAccessToken:
(NSString *)facebookAccessToken {
    NSDictionary *userInfoDictionary = @{ @"accessToken" : facebookAccessToken };
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:userInfoDictionary
                        options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care
                        // about the readability of
                        // the generated string
                        error:&error];
    if (!jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
 /*       NSString *jsonString =
        [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Json String is : %@", jsonString);
   */ }
    
    NSURL *url =
    [NSURL URLWithString:@"https://107.170.28.5:8443/java-rest/user/login"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    [request setHTTPMethod:@"POST"];
    
    AFHTTPRequestOperation *operation =
    [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,
                                               id responseObject) {
        NSError *error1 = nil;
        NSDictionary *dictionary =
        [NSJSONSerialization JSONObjectWithData:responseObject
                                        options:NSJSONReadingMutableContainers
                                          error:&error1];
        [defaults setObject:[dictionary valueForKeyPath:@"userId"]
                     forKey:@"restorUserID"];
        [defaults setObject:[dictionary valueForKeyPath:@"token"]
                     forKey:@"restoreUserToken"];
        [defaults synchronize];
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate setRootViewControllerToHomeViewController];
        NSLog(@"response token is   : %@", [dictionary description]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView *alertView =
        [[UIAlertView alloc] initWithTitle:@"Error Retrieving Weather"
                                   message:[error localizedDescription]
                                  delegate:nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [alertView show];
    }];
    
    [operation start];
}


#pragma mark- Dispatch Google Analytic Event
//mv
- (void)dispatchEvent:(NSDictionary*)event
{
    [[GAI sharedInstance].defaultTracker send:event];
    [[GAI sharedInstance] dispatch];
}

/*This is called when the user pressed the rate button in the rating prompt. This is useful if you want to log user interaction with iRate. This method is only called if you are using the standard iRate alert view prompt and will not be called automatically if you provide a custom rating implementation or call the openRatingsPageInAppStore method directly.*/
- (void)iRateUserDidAttemptToRateApp
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"RateApp" properties:@{ @"Rating": @"Attempted"}];
    
}

/*This is called when the user declines to rate the app. This is useful if you want to log user interaction with iRate. This method is only called if you are using the standard iRate alert view prompt and will not be called automatically if you provide a custom rating implementation.*/
- (void)iRateUserDidDeclineToRateApp
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"RateApp" properties:@{ @"Rating": @"Declined"}];
}


- (void)iRateUserDidRequestReminderToRateApp
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"RateApp" properties:@{ @"Rating": @"Reminded"}];
    
}

@end
