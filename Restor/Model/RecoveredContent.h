//
//  RecoveredContent.h
//  Restor
//
//  Created by Deva Palanisamy on 10/08/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecoveredContent : NSObject{
    NSArray *notes;
    NSArray *messages;
    NSArray *whatsapp;
    NSArray *contacts;
    NSArray *callsHistory;
    NSArray *calendar;
    
    NSMutableDictionary *list;
    
    int contentCount;
    
    NSMutableArray *fileListArray;
    NSMutableDictionary *uploadIDDictionary;
    NSMutableDictionary *badgeDictionary;
    NSArray *titlesArray;
    NSUserDefaults *defaults;
}

@property (strong, nonatomic) NSArray *notes;
@property (strong, nonatomic) NSArray *messages;
@property (strong, nonatomic) NSArray *whatsapp;
@property (strong, nonatomic) NSArray *contacts;
@property (strong, nonatomic) NSArray *callsHistory;
@property (strong, nonatomic) NSArray *calendar;

@property (strong, nonatomic) NSMutableDictionary *list;

+ (RecoveredContent*)sharedInstance;
- (void)getContentFromStoredFiles;
- (void)dropViewDidBeginRefreshing;
@end
