//
//  RecoveredContent.m
//  Restor
//
//  Created by Deva Palanisamy on 10/08/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import "RecoveredContent.h"
#import "AFHTTPRequestOperation.h"
#import "TokenManager.h"
@implementation RecoveredContent
@synthesize  messages,calendar,contacts,callsHistory,whatsapp,notes,list;
- (instancetype)init
{
    self = [super init];
    if (self) {
        titlesArray = @[@"messages",
                        @"contacts",
                        @"callhistory",
                        @"calendar",
                        @"whatsapp",
                        @"notes"];
        list=[[NSMutableDictionary alloc]init];
        contentCount=0;
        defaults=[NSUserDefaults standardUserDefaults];
        
    }
    return self;
}

+ (RecoveredContent*)sharedInstance
{
    static RecoveredContent *_sharedInstance = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[RecoveredContent alloc] init];
    });
    return _sharedInstance;
}

-(void)getContentFromStoredFiles{
    defaults=[NSUserDefaults standardUserDefaults];
    NSMutableArray *filesList=[[[defaults valueForKey:@"profiles"]
                                valueForKey:[defaults valueForKey:@"selectedDevice"]]
                               valueForKey:@"filesList"];
    if (filesList!=nil)
    {
        for (NSString* filename in filesList)
        {
            NSData *data=[NSData dataWithContentsOfFile:filename];
            NSString* type=[[filename lastPathComponent] stringByDeletingPathExtension];
            if ([type isEqualToString:@"notes"])
            {
                notes=[[NSKeyedUnarchiver unarchiveObjectWithData:data] valueForKey:@"notes"];
            }
            else if ([type isEqualToString:@"messages"])
            {
                messages=[[NSKeyedUnarchiver unarchiveObjectWithData:data] valueForKey:@"conversation"];
            }
            else if ([type isEqualToString:@"whatsapp"])
            {
                whatsapp=[[NSKeyedUnarchiver unarchiveObjectWithData:data] valueForKey:@"conversation"];
            }
            else if ([type isEqualToString:@"calendar"])
            {
                calendar=[[NSKeyedUnarchiver unarchiveObjectWithData:data] valueForKey:@"calendar"];
            }
            else if ([type isEqualToString:@"contacts"])
            {
                contacts=[[NSKeyedUnarchiver unarchiveObjectWithData:data] valueForKey:@"contacts"];
            }
            else if ([type isEqualToString:@"callhistory"])
            {
                callsHistory=[[NSKeyedUnarchiver unarchiveObjectWithData:data] valueForKey:@"callhistory"];
            }
        }
        
        if (messages)
        {
            [list setValue:messages forKey:@"Messages"];
        }
        if (contacts)
        {
            [list setValue:contacts forKey:@"Contacts"];
        }
        if (callsHistory)
        {
            [list setValue:callsHistory forKey:@"Call History"];
        }
        if (calendar)
        {
            [list setValue:calendar forKey:@"Calender"];
        }
        if (whatsapp)
        {
            [list setValue:whatsapp forKey:@"Chat History"];
        }
        if (notes)
        {
            [list setValue:notes forKey:@"Notes"];
        }
        
    }
}

- (void)dropViewDidBeginRefreshing
{
    
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //[refreshControl endRefreshing];
    });
    
    if (contentCount==0)
    {
        if (fileListArray==nil)
        {
            fileListArray=[[NSMutableArray alloc]init];
            uploadIDDictionary=[[NSMutableDictionary alloc]init];
            badgeDictionary=[[NSMutableDictionary alloc]init];
        }
        else
        {
            [fileListArray removeAllObjects];
            //[uploadIDDictionary removeAllObjects];
        }
    }
    
    if (contentCount < 6) {
        [self getRecoveredContent:[titlesArray objectAtIndex:contentCount]];
        contentCount++;
    }else{
        contentCount=0;
        if ([fileListArray count]>0) {
            NSMutableDictionary *profiles=[[defaults valueForKey:@"profiles"]mutableCopy];
            NSMutableDictionary* deviceProfile=[[profiles valueForKey:[defaults valueForKey:@"selectedDevice"]]mutableCopy];
            [deviceProfile setObject:fileListArray forKey:@"filesList"];
            [deviceProfile setObject:uploadIDDictionary forKey:@"uploadIDDictionary"];
            [deviceProfile setObject:badgeDictionary forKey:@"badgeDictionary"];
            [profiles setObject:deviceProfile forKey:[defaults valueForKey:@"selectedDevice"]];
            [defaults setObject:profiles forKey:@"profiles"];
            [defaults synchronize];
        }
        [self getContentFromStoredFiles];


        
        [[NSNotificationCenter defaultCenter]
                                        postNotificationName:@"stopAnimating"
                                                      object:self
                                                    userInfo:nil];
    }
}

-(void)getRecoveredContent:(NSString*)contentName{
    NSDictionary* profiles=[defaults valueForKey:@"profiles"];
    NSDictionary* deviceProfile=[profiles valueForKey:[defaults valueForKey:@"selectedDevice"]];
    
    NSDate *expiryDate=[deviceProfile valueForKey:@"expiryTime"];
    NSDate *date=[NSDate date];
    
    if ([date compare:expiryDate]==NSOrderedDescending) {
        TokenManager *tokenManager=[[TokenManager alloc]init];
        [tokenManager getToken:[deviceProfile valueForKey:@"refreshToken"] byType:YES];
        return;
    }
    
    NSString *urlString=[NSString stringWithFormat:@"https://107.170.28.5:8443/smartphonedatarecovery/v1.0/users/%@/%@",[deviceProfile valueForKey:@"userID"],contentName];
    
    NSString* uploadID=[[deviceProfile valueForKey:@"uploadIDDictionary"]valueForKey:[NSString stringWithFormat:@"%@UploadID",contentName]];
    if (uploadID!=nil) {
        urlString=[urlString stringByAppendingPathComponent:uploadID];
    }
    
    NSURL *url=[NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]initWithURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[@"Bearer " stringByAppendingString:[deviceProfile valueForKey:@"accessToken"]] forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"GET"];
    
    AFHTTPRequestOperation* operation=[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.securityPolicy.allowInvalidCertificates = YES; // added by sunil on 6-Nov-2014 for allow all certificates and remove certificates error
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error1=nil;
        NSDictionary *dictionary=[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error1];
        NSString* documentDirectoryPath=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        documentDirectoryPath=[documentDirectoryPath stringByAppendingPathComponent:[defaults valueForKey:@"selectedDevice"]];
        if (![[NSFileManager defaultManager] fileExistsAtPath:documentDirectoryPath])
            [[NSFileManager defaultManager] createDirectoryAtPath:documentDirectoryPath withIntermediateDirectories:NO attributes:nil error:&error1];
        NSString* filePath=[documentDirectoryPath stringByAppendingPathComponent:[contentName stringByAppendingFormat:@".plist"]];
        NSString* uploadIdName=[NSString stringWithFormat:@"%@UploadID",contentName];
        NSString* badgeName=[NSString stringWithFormat:@"%@Badge",contentName];
        BOOL isFileWritten=NO;
        NSData *data=nil;
        BOOL num=(BOOL)[[dictionary valueForKey:@"empty"] boolValue];
        if (!num) {
            data=[NSKeyedArchiver archivedDataWithRootObject:dictionary];
            isFileWritten=[data writeToFile:filePath atomically:YES];
        }
        
        
        if (isFileWritten)
        {
            NSLog(@"%@ file written to documents directory",contentName);
            [fileListArray addObject:filePath];
            [uploadIDDictionary setObject:[dictionary valueForKey:@"uploadId"] forKey:uploadIdName];
            [badgeDictionary setObject:[NSNumber numberWithBool:YES] forKey:badgeName];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateBadge"
                                                                object:self
                                                              userInfo:@{@"contentName":contentName}];
            NSURL *URL=[NSURL fileURLWithPath:filePath];
            NSError *error = nil;
            BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                          forKey: NSURLIsExcludedFromBackupKey error: &error];
            if(!success)
            {
                NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
            }
        }
        else
        {
            NSLog(@"%@ file not written",contentName);
        }
        [self dropViewDidBeginRefreshing];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *errorTitle=[NSString stringWithFormat:@"Error retrieving %@",contentName];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:errorTitle
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"stopAnimating"
         object:self
         userInfo:nil];
    }];
    
    [operation start];
    
}



@end
