//
//  HomeTableViewCell.m
//  Restor
//
//  Created by Deva Palanisamy on 29/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import "HomeTableViewCell.h"

@implementation HomeTableViewCell
@synthesize thumbnail,badge,title,descriptionLabel,selectedDeviceLabel;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    [self setViews];
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    [self setViews];
    // Configure the view for the selected state
}

- (void)setViews{
    thumbnail=[UIImageView new];
    [thumbnail setBackgroundColor:[UIColor clearColor]];
    [thumbnail setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    
    badge=[UIButton buttonWithType:UIButtonTypeCustom];
    [badge setBackgroundColor:[UIColor clearColor]];
    [badge sizeToFit];
    [badge setTranslatesAutoresizingMaskIntoConstraints:NO];
    badge.layer.borderWidth=0;
    
    
    title=[UILabel new];
    [title setBackgroundColor:[UIColor clearColor]];
    [title setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    descriptionLabel=[UILabel new];
    [descriptionLabel setBackgroundColor:[UIColor clearColor]];
    [descriptionLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    selectedDeviceLabel=[UILabel new];
    [descriptionLabel setBackgroundColor:[UIColor clearColor]];
    [selectedDeviceLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [self.contentView addSubview:thumbnail];
    [self.contentView addSubview:title];
    [self.contentView addSubview:descriptionLabel];
    [self.contentView addSubview:badge];
    [self.contentView addSubview:selectedDeviceLabel];

    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:thumbnail attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1 constant:5]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:thumbnail attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1 constant:5]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:thumbnail attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1 constant:-5]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:thumbnail attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:thumbnail attribute:NSLayoutAttributeHeight multiplier:1 constant:0]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:title attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:thumbnail attribute:NSLayoutAttributeRight multiplier:1 constant:10]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:title attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1 constant:26]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:title attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1 constant:-20]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:title attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:0.35 constant:0]];
   
    
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:descriptionLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1 constant:-16]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:descriptionLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1 constant:30]];
   // [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:descriptionLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:thumbnail attribute:NSLayoutAttributeHeight multiplier:.35 constant:0]];
    
    
 /*   [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:descriptionLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:thumbnail attribute:NSLayoutAttributeRight multiplier:1 constant:10]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:descriptionLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:title attribute:NSLayoutAttributeBottom multiplier:1 constant:5]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:descriptionLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeWidth multiplier:0.55 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:descriptionLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1 constant:-5]];
    */
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:badge attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:thumbnail attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:badge attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:thumbnail attribute:NSLayoutAttributeRight multiplier:1 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:badge attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:thumbnail attribute:NSLayoutAttributeHeight multiplier:.35 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:badge attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:thumbnail attribute:NSLayoutAttributeWidth multiplier:.45 constant:0]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:selectedDeviceLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:.18 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:selectedDeviceLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1 constant:-10]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:selectedDeviceLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1 constant:-5]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:selectedDeviceLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeWidth multiplier:.15 constant:0]];


}
@end
