//
//  MessagesViewController.m
//  Restor
//
//  Created by Deva Palanisamy on 27/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import "GAIFields.h"
#import "MessagesViewController.h"

@interface MessagesViewController ()

@end

@implementation MessagesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    contentArray=[messagesDict valueForKey:@"messages"];

    
    chatTableView=[[UIBubbleTableView alloc]initWithFrame:self.view.bounds];
    chatTableView.bubbleDataSource=self;
    chatTableView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.view.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:chatTableView];
    
    UILabel *titleLabel=[UIViewController getTitleLabelWithFrame:self.view.frame];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.text=self.title;
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    self.navigationItem.titleView=titleLabel;
    
    
    
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor greenColor];
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    contentArray=[messagesDict valueForKey:@"messages"];
    

    if (!isWhatsApp  && UI_USER_INTERFACE_IDIOM () != UIUserInterfaceIdiomPad && ![[[UIDevice currentDevice] model] hasPrefix:@"iPad"] )
    {
        UIBarButtonItem *replyButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Reply",@"") style:UIBarButtonItemStylePlain target:self action:@selector(sendInAppSMS:)];
        replyButton.tintColor = [UIColor whiteColor];
        self.navigationItem.rightBarButtonItem = replyButton;
    }
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    isWhatsApp= NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setMessagesDictionary:(NSDictionary*)dict{
    messagesDict=dict;
}

-(NSDate*)convertStringToDate:(NSString*)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    [dateFormatter setDateFormat:@"MM-dd-yyyy HH:mm "];
    // voila!
    NSDate *dateFromString = [dateFormatter dateFromString:dateString];
    return dateFromString;
}

#pragma mark - Table View Datasource implementation
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [contentArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict=[contentArray objectAtIndex:indexPath.row];
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    cell.textLabel.text=[dict valueForKey:@"message"];
    
    /*
    Message *object = [array objectAtIndex:indexPath.row];
    cell.titleLabel.text = object.name;
    NSString* dateString=[(Messages *)[object.messages firstObject] date];
    cell.timeLabel.text=[self convertDateToRelativeString:dateString];
    cell.detailTextLabel.text=[(Messages *)[object.messages firstObject] message];
    */
    
    return cell;
}

#pragma mark - chat view implementation
- (NSInteger)rowsForBubbleTable:(UIBubbleTableView *)tableView
{
    return [contentArray count];
}

- (NSBubbleData *)bubbleTableView:(UIBubbleTableView *)tableView dataForRow:(NSInteger)row
{
    NSDictionary *dict=[contentArray objectAtIndex:row];
    NSBubbleData *data=nil;
    NSString *text=[dict valueForKey:@"message"];
    NSDate *date=[self convertStringToDate:[dict valueForKey:@"date"]];
    
    if (date == nil)
    {
        date = [[NSDate alloc] init];
    }
    if ([[dict valueForKey:@"type"]isEqualToString:@"received"])
    {
        data=[NSBubbleData dataWithText:text date:date type:2];
    }
    else
    {
        data=[NSBubbleData dataWithText:text date:date type:1];
    }
    
    return data;
}


#pragma mark - Reply to conversation
-(void) sendInAppSMS:(id) sender
{
    if (isWhatsApp)
    {
        NSURL *whatsappURL = [NSURL URLWithString:@"whatsapp://send?abid=22&text=hello"];
        if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
            [[UIApplication sharedApplication] openURL: whatsappURL];
        }
    }
    else
    {
	MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
	if([MFMessageComposeViewController canSendText])
	{
		controller.body = NSLocalizedString(@"Start Typing...",@"");
        NSString *contactNo=[messagesDict valueForKey:@"phone"];
		controller.recipients = [NSArray arrayWithObjects:contactNo, nil];
		controller.messageComposeDelegate = self;
		[self presentViewController:controller animated:YES completion:nil];
        //[self.navigationController pushViewController:controller animated:YES];
	}
    }
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
	switch (result) {
		case MessageComposeResultCancelled:
        {
			NSLog(@"SMS Cancelled");
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"SMS Sharing Cancelled"];
        }
			break;
		case MessageComposeResultFailed:
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Announcement" message: @"Unknown Error" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            //			[alert show];
            NSLog(@"Unknown Error");
			break;
		case MessageComposeResultSent:
        {
            NSLog(@"SMS sent");
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Shared Via SMS"];
        }
			break;
		default:
			break;
	}
    
	[self dismissViewControllerAnimated:YES completion:nil];
}

-(void)setWhatsAppKey:(BOOL)isWhatsapp{
    isWhatsApp=isWhatsapp;
}

@end
