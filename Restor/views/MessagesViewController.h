//
//  MessagesViewController.h
//  Restor
//
//  Created by Deva Palanisamy on 27/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIBubbleTableView.h"
#import "UIViewController+Helper.h"
#import <MessageUI/MFMessageComposeViewController.h>

//mv
@interface MessagesViewController : GAITrackedViewController<UIBubbleTableViewDataSource,MFMessageComposeViewControllerDelegate>{
    __weak NSDictionary *messagesDict;
    __weak NSArray *contentArray;
    UITableView *messagesTableView;
    UIBubbleTableView *chatTableView;
    BOOL isWhatsApp;
}
-(void)setMessagesDictionary:(NSDictionary*)dict;
-(void)setWhatsAppKey:(BOOL)isWhatsapp;
@end
