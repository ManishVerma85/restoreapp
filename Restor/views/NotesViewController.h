//
//  NotesViewController.h
//  Restor
//
//  Created by Deva Palanisamy on 28/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import <UIKit/UIKit.h>

//mv
@interface NotesViewController : GAITrackedViewController{
    UITextView *textview;
    __weak NSDictionary *notesDict;
}
- (void)setNotesDictionary:(NSDictionary*)dict;
@end
