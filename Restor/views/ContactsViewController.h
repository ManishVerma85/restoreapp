//
//  ContactsViewController.h
//  Restor
//
//  Created by Deva Palanisamy on 28/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
//mv
@interface ContactsViewController : GAITrackedViewController{
   __weak NSDictionary *contactsDictionary;
}
-(void)setDictionary:(NSDictionary*)dictionary;
@end
