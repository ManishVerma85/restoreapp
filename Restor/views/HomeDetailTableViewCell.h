//
//  HomeDetailTableViewCell.h
//  Restor
//
//  Created by Deva Palanisamy on 01/06/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeDetailTableViewCell : UITableViewCell
{
    
}
@property (strong,nonatomic) UILabel *titleLabel;
@property (strong,nonatomic) UILabel *timeLabel;
@property (strong,nonatomic) UILabel *detailText;
@property (strong,nonatomic) UILabel *startTimeLabel;
@property (strong,nonatomic) UILabel *endTimeLabel;
@property (strong,nonatomic) UILabel *borderLabel;
@property (strong,nonatomic) UILabel *dateLabel;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withType:(NSString*)type;
@end
