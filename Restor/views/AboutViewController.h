//
//  AboutViewController.h
//  Restor
//
//  Created by Deva Palanisamy on 24/06/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+Helper.h"
#import "WebViewController.h"
#import "ContactUsViewController.h"
//mv
@interface AboutViewController : GAITrackedViewController<UITableViewDataSource,UITableViewDelegate>{
    UITableView* tableview;
    NSArray* aboutContentList;
}

@end
