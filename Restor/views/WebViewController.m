//
//  WebViewController.m
//  Restor
//
//  Created by Deva Palanisamy on 25/06/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import "TJSpinner.h"
#import "WebViewController.h"
#import "HTProgressHUD.h"

@interface WebViewController ()
{
   // HTProgressHUD *HUD;
    
    TJSpinner *_circularSpinner;
}

@end

@implementation WebViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    webview.delegate=nil;
    webview=nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view=[[UIView alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    self.view.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.view.autoresizesSubviews=YES;
    
    UILabel *title=[UIViewController getTitleLabelWithFrame:self.view.frame];
    title.backgroundColor=[UIColor clearColor];
    title.text=self.title;
    title.textColor = [UIColor whiteColor];
    [title sizeToFit];
    self.navigationItem.titleView=title;
    
    webview=[[UIWebView alloc]initWithFrame:self.view.bounds];
    webview.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    webview.autoresizesSubviews=YES;
    webview.delegate=self;
    NSURLRequest *request=[NSURLRequest requestWithURL:url];
    [webview loadRequest:request];
    [self.view addSubview:webview];
    
    
    _circularSpinner = [[TJSpinner alloc] initWithSpinnerType:kTJSpinnerTypeCircular];
    _circularSpinner.hidesWhenStopped = YES;
    _circularSpinner.radius = 10;
    _circularSpinner.pathColor = [UIColor whiteColor];
    _circularSpinner.fillColor = [UIColor colorWithRed:(38.0f/255.0f) green:(142.0f/255.0f) blue:(217.0f/255.0f) alpha:1.0f];// [UIColor redColor];
    _circularSpinner.thickness = 7;
    //[spinner release];
    
    
    float xpos = (self.view.bounds.size.width-(_circularSpinner.bounds.size.width))/2;
    float  ypos = (self.view.bounds.size.height-(_circularSpinner.bounds.size.height))/2;
    
    
    NSLog(@"frame width = %f  height = %f ",self.view.frame.size.width, self.view.frame.size.height);
    
    CGRect newf;
    newf.origin.x = xpos;
    newf.origin.y = ypos;
    newf.size.height = _circularSpinner.bounds.size.height;//.radius *4;
    newf.size.width = _circularSpinner.bounds.size.width;//.radius *4;
    
    NSLog(@"spinner boundsframe width = %f  height = %f ",_circularSpinner.bounds.size.width, _circularSpinner.bounds.size.height);
    NSLog(@"spinner boundsframe width = %f  height = %f ",xpos, ypos);
    
    _circularSpinner.frame = newf;
    
    _circularSpinner.autoresizingMask =UIViewAutoresizingFlexibleLeftMargin   |
    UIViewAutoresizingFlexibleRightMargin  |
    UIViewAutoresizingFlexibleTopMargin    |
    UIViewAutoresizingFlexibleBottomMargin;
    
    
    
    
    
    /*  spinnerView = [spinnerArray objectAtIndex:i];
     [spinnerView setBounds:CGRectMake(0,0 , [spinnerView frame].size.width, [spinnerView frame].size.height)];
     [spinnerView setCenter:CGPointMake((spinnerViewWidth/2.00)+i*spinnerViewWidth, cell.contentView.center.y)];
     [cell addSubview:spinnerView];
     */
    [self.view addSubview:_circularSpinner];
    
    [_circularSpinner startAnimating];
    
    NSLog(@"spinner boundsframe width = %f  height = %f ",_circularSpinner.frame.origin.x, _circularSpinner.frame.origin.y);
    
  /*  // how we stop refresh from freezing the main UI thread
    dispatch_queue_t downloadQueue = dispatch_queue_create("downloader", NULL);
    dispatch_async(downloadQueue, ^{
        
        // do our long running process here
        [NSThread sleepForTimeInterval:10];
        
        // do any UI stuff on the main UI thread
        dispatch_async(dispatch_get_main_queue(), ^{
            // self.myLabel.text = @"After!";
            
            
            [self performSelectorOnMainThread:@selector(phoneSelected:) withObject:deviceName waitUntilDone:YES];
            
            
            
            [circularSpinner stopAnimating];
        });
        
    });*/
    
    
    
    
    
    
    /*
    HUD=[[HTProgressHUD alloc] init];
    HUD.textLabel.numberOfLines = 2;
    HUD.textLabel.text=@"Accessing \n Web Page";
    [HUD showInView:self.view];*/
}

-(void)setUrlForWebview:(NSURL*)Url
{
    url=Url;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_circularSpinner stopAnimating];
//    [HUD hideAfterDelay:1];
}

@end
