//
//  ContactUsViewController.h
//  Restor
//
//  Created by Deva Palanisamy on 25/06/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebViewController.h"
#import "UIViewController+Helper.h"
#import <MessageUI/MFMailComposeViewController.h>
//mv
@interface ContactUsViewController : GAITrackedViewController<UITextViewDelegate,MFMailComposeViewControllerDelegate>{
    UITextView *textview;
    UIButton *faqButton;
    UILabel *titleLabel;
    CGFloat height;
}

@end
