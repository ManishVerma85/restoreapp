//
//  ContactUsViewController.m
//  Restor
//
//  Created by Deva Palanisamy on 25/06/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import "ContactUsViewController.h"
#import "AppDelegate.h"
@interface ContactUsViewController ()

@end

@implementation ContactUsViewController
- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(myNotificationMethod:) name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(settextContents:) name:UIDeviceOrientationDidChangeNotification object:nil];
        
        
        
        //some method like viewDidLoad, where you set up your observer.
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];

        height=0;
    }
    return self;
}

- (void)keyboardWillChange:(NSNotification *)notification
{
    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil]; //this is it!
    height = keyboardRect.size.height+10;
}

- (void)viewDidLoad
{
    
    self.view.userInteractionEnabled = YES; // superview may blocks touches
    self.view.clipsToBounds = YES; // superview may clips your textfield, you will see it
    
    [super viewDidLoad];
    self.view=[[UIView alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    self.view.backgroundColor=[UIColor whiteColor];
    self.view.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.view.autoresizesSubviews=YES;
    
    UILabel *title=[UIViewController getTitleLabelWithFrame:self.view.frame];
    title.backgroundColor=[UIColor clearColor];
    title.text=NSLocalizedString(@"Feedback",@"");
    title.textColor = [UIColor whiteColor];
    [title sizeToFit];
    self.navigationItem.titleView=title;
    
    //mv
    [self prepareLayout];
    
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
  //  [textView becomeFirstResponder];
    return YES;
}

- (void) viewWillAppear:(BOOL)animated{
    
    
    [super viewWillAppear:YES];
       self.navigationController.navigationBarHidden=NO;
   /* [self prepareLayout];
    
    textview.userInteractionEnabled=YES;
    
  
    
    [textview becomeFirstResponder];
    textview.delegate = self;*/
}

- (void)prepareLayout
{
  /*  ((AppDelegate *)[[UIApplication sharedApplication] delegate]).enablePortrait= YES;
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
    
    UINavigationController* nc = [[UINavigationController alloc] init];
    [self.navigationController presentViewController:nc animated:NO completion:^{
    }];
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
 */
    
    textview=[UITextView new];
    textview.userInteractionEnabled=YES;
    textview.delegate = self;
    textview.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    textview.autoresizesSubviews=YES;
    [textview setTranslatesAutoresizingMaskIntoConstraints:NO];
    textview.backgroundColor=[UIColor whiteColor];
    textview.tintColor=[UIColor blackColor];
    textview.font=[UIFont boldSystemFontOfSize:18.0f];
    [textview becomeFirstResponder];
    [self.view addSubview:textview];
    
    
    faqButton=[UIButton new];
    faqButton.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    faqButton.autoresizesSubviews=YES;
    [faqButton addTarget:self action:@selector(showFaq:) forControlEvents:UIControlEventTouchUpInside];
    
    faqButton.titleLabel.textAlignment=NSTextAlignmentLeft;
    [faqButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    faqButton.titleLabel.font= [UIFont boldSystemFontOfSize:16.0f];
    [faqButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [self.view addSubview:faqButton];
    
    titleLabel=[UILabel new];
    
    titleLabel.textColor=[UIColor blackColor];
    titleLabel.backgroundColor=[UIColor whiteColor];
    titleLabel.font= [UIFont boldSystemFontOfSize:12.0f];
    [titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view addSubview:titleLabel];
    
    UIBarButtonItem *sendBarBtn=[UIBarButtonItem new];
    sendBarBtn.title=NSLocalizedString(@"Done",@"Title of a button");
    [sendBarBtn setAction:@selector(showEmail:)];
    [self.navigationItem setRightBarButtonItem:sendBarBtn animated:NO];
    
    self.navigationController.navigationBarHidden=NO;

    /*
    
    NSDictionary *dictionary=NSDictionaryOfVariableBindings(textview,faqButton,titleLabel);
    NSDictionary *metricsDict={@{@"height": [NSNumber numberWithFloat:height]}};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[textview]-0-|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:dictionary ]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[faqButton]-0-|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:dictionary ]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[titleLabel]-0-|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:dictionary ]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-1-[titleLabel(==15)]-2-[textview]-10-[faqButton(==44)]-(==height)-|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:metricsDict views:dictionary ]];

    
    faqButton.layer.borderWidth=1.0;
    faqButton.layer.borderColor=[UIColor grayColor].CGColor;
    titleLabel.text=@"Please enter your message below";
    [faqButton setTitle:@"Have you read our FAQ yet?" forState:UIControlStateNormal];*/

}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    ((AppDelegate *)[[UIApplication sharedApplication] delegate]).enablePortrait= NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation

{
    
    return UIInterfaceOrientationPortrait;
    
}

-(BOOL)shouldAutorotate
{
    return NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return ( UIInterfaceOrientationIsPortrait(interfaceOrientation));
}

- (void)myNotificationMethod:(NSNotification*)notification
{
    
    NSLog(@"myNotificationMethod");//received
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    NSLog(@"Keyboard frame : %@",NSStringFromCGRect(keyboardFrameBeginRect));
    height=keyboardFrameBeginRect.size.height+10;
    
    
    NSDictionary *dictionary=NSDictionaryOfVariableBindings(textview,faqButton,titleLabel);
    NSDictionary *metricsDict={@{@"height": [NSNumber numberWithFloat:height]}};
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[textview]-0-|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:dictionary ]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[faqButton]-0-|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:dictionary ]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[titleLabel]-0-|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:dictionary ]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-1-[titleLabel(==15)]-2-[textview]-10-[faqButton(==44)]-(==height)-|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:metricsDict views:dictionary ]];
    faqButton.layer.borderWidth=1.0;
    faqButton.layer.borderColor=[UIColor grayColor].CGColor;
    titleLabel.text=NSLocalizedString(@"Please Enter your Message Below",@"");
    [faqButton setTitle:NSLocalizedString(@"Have you read our FAQ yet?",@"") forState:UIControlStateNormal];
    
}

-(void)settextContents:(NSNotification*)notification{
    NSLog(@"Orientation changed in feedback viewcontroller");
}
-(void)showFaq:(id)sender
{
    WebViewController* wvc=[[WebViewController alloc]init];
    wvc.title=NSLocalizedString(@"FAQ",@" abbreviation of Frequenlty Asked Questions");
    NSURL *url=[NSURL URLWithString:NSLocalizedString(@"http://www.enigma-recovery.com/support-app",@"support web page, doesnt need translating as such but the appropriate country page needs placing in here")];
    [wvc setUrlForWebview:url];
    [self.navigationController pushViewController:wvc animated:YES];
}

#pragma mark - implementing text view delegates
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    textview.text=@"";
}

#pragma mark - compose a mail

- (void)showEmail:(id)sender
{
    NSLog(@"showEmail");
 
    
    if ([MFMailComposeViewController canSendMail])
    {
        // Email Subject
        NSString *emailTitle = NSLocalizedString(@"Restore App issue ",@"title of an email message");
        // Email Content
        NSString *messageBody = [textview.text stringByReplacingOccurrencesOfString: @"\n" withString: @"<br />"]; // Change the message body to HTML
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:NSLocalizedString(@"support@enigma-recovery.com",@"doesnt need translating as such but appropriate countries support email address needs to be placed here")];
    
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
    
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:YES];
        [mc setToRecipients:toRecipents];
    
        /*
        UILabel *title=[UIViewController getTitleLabelWithFrame:self.view.frame];
        title.backgroundColor=[UIColor clearColor];
        title.text=emailTitle;
        title.textColor = [UIColor whiteColor];
        [title sizeToFit];
        mc.navigationItem.titleView=title;
        */
    
    
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    
    }
    else
    {
        UIAlertView *showMessage = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Not Available",@"email is not available") message:NSLocalizedString(@"Email has not been configured. Please setup an email account to send emails.",!"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
        [showMessage show];
    }

    
}

#pragma mark - Implementing mail view delegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    ((AppDelegate *)[[UIApplication sharedApplication] delegate]).enablePortrait= YES;
    switch (result)
    {
        case MFMailComposeResultCancelled:
        {
            NSLog(@"Issue reporting cancelled");
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Issue reporting Cancelled"];
        }
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Issue reporting Mail saved");
            break;
        case MFMailComposeResultSent:
        {
            NSLog(@"Issue reporting Mail sent");
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Issue reported Via Email"];
        }
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Issue reporting mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    UIBarButtonItem *sendBarBtn=[UIBarButtonItem new];
    sendBarBtn.title=NSLocalizedString(@"Done",@" a done button on the top of a navigation bar UI control");
    [sendBarBtn setAction:@selector(showEmail:)];
    [self.navigationItem setRightBarButtonItem:sendBarBtn animated:NO];
}

@end
