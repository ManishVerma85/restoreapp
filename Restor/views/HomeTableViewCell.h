//
//  HomeTableViewCell.h
//  Restor
//
//  Created by Deva Palanisamy on 29/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeTableViewCell : UITableViewCell{
//    UIImageView *thumbnail;
//    UILabel *title;
//    UILabel *descriptionLabel;
//    UILabel *badge;
}
@property (strong,nonatomic) UIImageView *thumbnail;
@property (strong,nonatomic) UILabel *title;
@property (strong,nonatomic) UILabel *descriptionLabel;
@property (strong,nonatomic) UIButton *badge;
@property (strong,nonatomic) UILabel *selectedDeviceLabel;
@end
