//
//  ContactsViewController.m
//  Restor
//
//  Created by Deva Palanisamy on 28/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import "ContactsViewController.h"

@interface ContactsViewController ()

@end

@implementation ContactsViewController


- (void)viewDidLoad
{
    NSLog(@"zyzz");
    [super viewDidLoad];
    self.view=[[UIView alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save To Contacts",@"") style:UIBarButtonItemStylePlain target:self action:@selector(saveContact:)];
    self.navigationItem.rightBarButtonItem = saveButton;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)setDictionary:(NSDictionary*)dictionary{
    contactsDictionary=dictionary;
}

- (void) addToContacts
{
    ABAddressBookRef iPhoneAddressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    ABRecordRef newPerson = ABPersonCreate();
    
    // add infos
    ABRecordSetValue(newPerson, kABPersonFirstNameProperty, CFSTR("Alberto"), nil);
    ABRecordSetValue(newPerson, kABPersonLastNameProperty, CFSTR("Pasca"), nil);
    ABRecordSetValue(newPerson, kABPersonOrganizationProperty, CFSTR("albertopasca.it"), nil);
    
    ABMutableMultiValueRef multiPhone = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    
    ABMultiValueAddValueAndLabel(multiPhone, @"328-1111111", kABHomeLabel, NULL);
    ABMultiValueAddValueAndLabel(multiPhone,@"02-222222", kABWorkLabel, NULL);
    ABRecordSetValue(newPerson, kABPersonPhoneProperty, multiPhone,nil);
    CFRelease(multiPhone);
    
    ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    
    ABMultiValueAddValueAndLabel(multiEmail, @"info@albertopasca.it", kABHomeLabel, NULL);
    ABRecordSetValue(newPerson, kABPersonEmailProperty, multiEmail, nil);
    
    CFRelease(multiEmail);
    
    ABAddressBookAddRecord(iPhoneAddressBook, newPerson, nil);
    
    CFRelease(newPerson);
    CFRelease(iPhoneAddressBook);
}

-(void)saveContact:(id)sender{
    // create person record
    
    ABRecordRef person = ABPersonCreate();
    
    // set name and other string values
    
    NSString* venueName=@"Ramrakesh";
    NSString* venueUrl=@"http://www.google.com";
    NSString* venueEmail=@"a.ramrakesh@gmail.com";
    NSString* venuePhone=@"00000000000 or 12342321211";
    
    ABRecordSetValue(person, kABPersonOrganizationProperty, (__bridge CFStringRef) venueName, NULL);
    
    if (venueUrl)
    {
        ABMutableMultiValueRef urlMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueAddValueAndLabel(urlMultiValue, (__bridge CFStringRef) venueUrl, kABPersonHomePageLabel, NULL);
        ABRecordSetValue(person, kABPersonURLProperty, urlMultiValue, nil);
        CFRelease(urlMultiValue);
    }
    
    if (venueEmail)
    {
        ABMutableMultiValueRef emailMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueAddValueAndLabel(emailMultiValue, (__bridge CFStringRef) venueEmail, kABWorkLabel, NULL);
        ABRecordSetValue(person, kABPersonEmailProperty, emailMultiValue, nil);
        CFRelease(emailMultiValue);
    }
    
    if (venuePhone)
    {
        ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        NSArray *venuePhoneNumbers = [venuePhone componentsSeparatedByString:@" or "];
        for (NSString *venuePhoneNumberString in venuePhoneNumbers)
            ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFStringRef) venuePhoneNumberString, kABPersonPhoneMainLabel, NULL);
        ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil);
        CFRelease(phoneNumberMultiValue);
    }
    
    // add address
    
    ABMutableMultiValueRef multiAddress = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
    NSMutableDictionary *addressDictionary = [[NSMutableDictionary alloc] init];
    
//    if (venueAddress1)
//    {
//        if (venueAddress2)
//            addressDictionary[(NSString *) kABPersonAddressStreetKey] = [NSString stringWithFormat:@"%@\n%@", venueAddress1, venueAddress2];
//        else
//            addressDictionary[(NSString *) kABPersonAddressStreetKey] = venueAddress1;
//    }
//    if (venueCity)
//        addressDictionary[(NSString *)kABPersonAddressCityKey] = venueCity;
//    if (venueState)
//        addressDictionary[(NSString *)kABPersonAddressStateKey] = venueState;
//    if (venueZip)
//        addressDictionary[(NSString *)kABPersonAddressZIPKey] = venueZip;
//    if (venueCountry)
//        addressDictionary[(NSString *)kABPersonAddressCountryKey] = venueCountry;
    
    ABMultiValueAddValueAndLabel(multiAddress, (__bridge CFDictionaryRef) addressDictionary, kABWorkLabel, NULL);
    ABRecordSetValue(person, kABPersonAddressProperty, multiAddress, NULL);
    CFRelease(multiAddress);
    
    // let's show view controller
    
    ABUnknownPersonViewController *controller = [[ABUnknownPersonViewController alloc] init];
    
    controller.displayedPerson = person;
    controller.allowsAddingToAddressBook = YES;
    
    // current view must have a navigation controller
    
    [self.navigationController pushViewController:controller animated:YES];
    
    CFRelease(person);
}
@end
