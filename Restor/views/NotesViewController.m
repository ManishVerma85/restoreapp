//
//  NotesViewController.m
//  Restor
//
//  Created by Deva Palanisamy on 28/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import "NotesViewController.h"

@interface NotesViewController ()

@end

@implementation NotesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view=[[UIView alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    self.view.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.view.autoresizesSubviews=YES;
    textview=[[UITextView alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    textview.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    textview.autoresizesSubviews=YES;
    textview.font=UI_FONT_FOR_NOTES;
    textview.editable=NO;
    [self.view addSubview:textview];

    UIBarButtonItem *copyButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Copy To Clipboard",@"") style:UIBarButtonItemStylePlain target:self action:@selector(copyText:)];
    copyButton.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = copyButton;
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    textview.text=[notesDict valueForKey:@"note"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setNotesDictionary:(NSDictionary*)dict{
    notesDict=dict;
}

-(void)copyText:(id)sender{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = [notesDict valueForKey:@"note"];
    
    UILabel *alert=[UILabel new];
    alert.backgroundColor=[UIColor clearColor];
    alert.alpha=1.0f;
    alert.text=@"Note copied";
    alert.textColor=self.navigationController.navigationBar.tintColor;
    alert.font=UI_FONT_FOR_ALERT;
    alert.textAlignment=NSTextAlignmentCenter;
    [alert sizeToFit];
    [alert setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view addSubview:alert];

    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:alert attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1 constant:-60]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:alert attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:40]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:alert attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1 constant:5]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:alert attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1 constant:-5]];
    
    [UIView animateWithDuration:0.5 delay:1.0 options:0 animations:^{
        alert.alpha = 0.0f;
    } completion:^(BOOL finished) {
        alert.hidden=YES;
        [alert removeFromSuperview];
    }];
}
@end
