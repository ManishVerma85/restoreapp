//
//  HomeDetailTableViewCell.m
//  Restor
//
//  Created by Deva Palanisamy on 01/06/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import "HomeDetailTableViewCell.h"

@implementation HomeDetailTableViewCell
@synthesize titleLabel,timeLabel,detailTextLabel,startTimeLabel,endTimeLabel,borderLabel,dateLabel;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withType:(NSString*)type
{
    NSArray *types = @[NSLocalizedString(@"Messages",@""),
                       NSLocalizedString(@"Contacts",@""),
                       NSLocalizedString(@"Call History",@""),
                       NSLocalizedString(@"Calendar",@""),
                       NSLocalizedString(@"Chat History",@""),
                       NSLocalizedString(@"Notes",@"")];
    
    
    NSUInteger item = [types indexOfObject:type];
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        switch (item) {
            case 0:
                {
                [self setupMessageCell];
                break;
                }
            case 1:
                {
                [self setupContactsCell];
                break;
                }
            case 2:
                {
                [self setupCallHistoryCell];
                break;
                }
            case 3:
                {
                [self setupCalenderCell];
                break;
                }
            case 4:
                {
                [self setupMessageCell];
                break;
                }
            case 5:
                {
                [self setupNotesCell];
                break;
                }
            default:
                break;
        }
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupMessageCell
{
    titleLabel=[UILabel new];
    timeLabel=[UILabel new];
    detailTextLabel=[UILabel new];
    
    titleLabel.font=UI_FONT_FOR_MESSAGE_TITLE;
    
    timeLabel.font=UI_FONT_FOR_MESSAGE_TIME_LABEL;
    detailTextLabel.font=UI_FONT_FOR_MESSAGE_TEXT_LABEL;
    detailTextLabel.numberOfLines=0;
    timeLabel.textAlignment=NSTextAlignmentRight;
    self.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    
    timeLabel.textColor=detailTextLabel.textColor=[UIColor grayColor];
    
    titleLabel.backgroundColor=timeLabel.backgroundColor=detailTextLabel.backgroundColor=[UIColor clearColor];
    [titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [timeLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [detailTextLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    
  /*  CGRect textLabelFrame = titleLabel.frame;
    textLabelFrame.origin.x+=100;
    textLabelFrame.origin.y-=140;
    //textLabelFrame.size.width -= xOffset;

    
    titleLabel.frame  = textLabelFrame;
    */
    [self.contentView addSubview:titleLabel];
    [self.contentView addSubview:timeLabel];
    [self.contentView addSubview:detailTextLabel];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1 constant:8]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeWidth multiplier:.45 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:10 constant:8]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:0.25 constant:0]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:timeLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1 constant:-5]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:timeLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:titleLabel attribute:NSLayoutAttributeRight multiplier:1 constant:5]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:timeLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:0.25 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:timeLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1 constant:8]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:detailTextLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1 constant:8]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:detailTextLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1 constant:-5]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:detailTextLabel attribute:NSLayoutAttributeRight   relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1 constant:-5]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:detailTextLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:titleLabel attribute:NSLayoutAttributeBottom multiplier:1 constant:5]];
}

-(void)setupCalenderCell{
    startTimeLabel=[UILabel new];
    endTimeLabel=[UILabel new];
    titleLabel=[UILabel new];
    detailTextLabel=[UILabel new];
    borderLabel=[UILabel new];
    dateLabel=[UILabel new];
    
    detailTextLabel.font=UI_FONT_FOR_CALENDAR_LOCATION;
    titleLabel.font=UI_FONT_FOR_CALENDAR_TITLE;
    startTimeLabel.font=UI_FONT_FOR_TIME_LABEL;
    endTimeLabel.font=UI_FONT_FOR_TIME_LABEL;
    dateLabel.font=UI_FONT_FOR_CALENDAR_DATE;
    
    detailTextLabel.textColor=[UIColor grayColor];
    dateLabel.textColor=[UIColor blackColor];
    
    startTimeLabel.numberOfLines=0;
    endTimeLabel.numberOfLines=0;
    titleLabel.numberOfLines=0;
    
    startTimeLabel.backgroundColor=endTimeLabel.backgroundColor=titleLabel.backgroundColor=borderLabel.backgroundColor=[UIColor clearColor];
    borderLabel.backgroundColor=[UIColor redColor];
    detailTextLabel.backgroundColor=[UIColor clearColor];
    dateLabel.backgroundColor=[UIColor clearColor];
    [startTimeLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [endTimeLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [borderLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [detailTextLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [dateLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.contentView addSubview:startTimeLabel];
    [self.contentView addSubview:endTimeLabel];
    [self.contentView addSubview:titleLabel];
    [self.contentView addSubview:borderLabel];
    [self.contentView addSubview:detailTextLabel];
    [self.contentView addSubview:dateLabel];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:dateLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1 constant:6]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:dateLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1 constant:-8]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:dateLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1 constant:3]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:dateLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:0.25 constant:0]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:startTimeLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1 constant:5]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:startTimeLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:dateLabel attribute:NSLayoutAttributeBottom multiplier:1 constant:2]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:startTimeLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeWidth multiplier:0.13 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:startTimeLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:0.3 constant:0]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:endTimeLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1 constant:5]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:endTimeLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1 constant:-5]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:endTimeLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeWidth multiplier:0.13 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:endTimeLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:0.3 constant:0]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:borderLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:startTimeLabel attribute:NSLayoutAttributeRight multiplier:1 constant:5]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:borderLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:dateLabel attribute:NSLayoutAttributeBottom multiplier:1 constant:10]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:borderLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1 constant:-10]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:borderLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:1]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:borderLabel attribute:NSLayoutAttributeRight multiplier:1 constant:8]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:0.35 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1 constant:-5]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:dateLabel attribute:NSLayoutAttributeBottom multiplier:1 constant:2]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:detailTextLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:borderLabel attribute:NSLayoutAttributeRight multiplier:1 constant:8]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:detailTextLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:0.25 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:detailTextLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1 constant:-5]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:detailTextLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:titleLabel attribute:NSLayoutAttributeBottom multiplier:1 constant:5]];
    
}

-(void)setupContactsCell
{
    titleLabel=[UILabel new];
    [titleLabel setBackgroundColor:[UIColor greenColor]];
    [titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    titleLabel.font=UI_FONT_FOR_CONTACT_TITLE;
    
    NSDictionary *elementDict=NSDictionaryOfVariableBindings(titleLabel);
    
    titleLabel.backgroundColor=timeLabel.backgroundColor=detailTextLabel.backgroundColor=[UIColor clearColor];
    [titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [timeLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [detailTextLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.contentView addSubview:titleLabel];
    //[self.contentView addSubview:timeLabel];
    //[self.contentView addSubview:detailTextLabel];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[titleLabel]-2-|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:elementDict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[titleLabel]-2-|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:elementDict]];
}

-(void)setupCallHistoryCell{
    titleLabel=[UILabel new];
    timeLabel=[UILabel new];
    detailTextLabel=[UILabel new];
    
    titleLabel.font=UI_FONT_FOR_CALLHISTORY_NAME_LABEL;
    timeLabel.font=UI_FONT_FOR_CALLHISTORY_NUMBER_LABEL;
    detailTextLabel.font=UI_FONT_FOR_CALLHISTORY_DETAILTEXT_LABEL;
    timeLabel.textAlignment=NSTextAlignmentRight;
    
    if (floor(NSFoundationVersionNumber) >= NSFoundationVersionNumber_iOS_6_1)
    {
        // Load resources for iOS 6.1 or earlier
        self.accessoryType=UITableViewCellAccessoryDetailButton;
    }
    else
    {
        // Load resources for iOS 7 or later
        self.accessoryType=UITableViewCellAccessoryDetailButton;
    }
    
    
    
    titleLabel.backgroundColor=timeLabel.backgroundColor=detailTextLabel.backgroundColor=[UIColor clearColor];
    detailTextLabel.textColor=[UIColor blackColor];
    [titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [timeLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [detailTextLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.contentView addSubview:titleLabel];
    [self.contentView addSubview:timeLabel];
    [self.contentView addSubview:detailTextLabel];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1 constant:8]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeWidth multiplier:.45 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1 constant:8]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:titleLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:0.30 constant:0]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:timeLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1 constant:-15]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:timeLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:titleLabel attribute:NSLayoutAttributeRight multiplier:1 constant:5]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:timeLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:0.25 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:timeLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1 constant:8]];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:detailTextLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1 constant:8]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:detailTextLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1 constant:-5]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:detailTextLabel attribute:NSLayoutAttributeRight   relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1 constant:-5]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:detailTextLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:titleLabel attribute:NSLayoutAttributeBottom multiplier:1 constant:5]];

    
}
-(void)setupNotesCell{
    titleLabel=[UILabel new];
    titleLabel.font=[UIFont systemFontOfSize:18];
    //[titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    titleLabel.font=UI_FONT_FOR_NOTES_TABLE;
    
    timeLabel=[UILabel new];
    [timeLabel setBackgroundColor:[UIColor clearColor]];
    [timeLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    timeLabel.font=UI_FONT_FOR_NOTES_TIME_LABEL;
    timeLabel.textColor=[UIColor grayColor];
    timeLabel.textAlignment=NSTextAlignmentRight;
    [self.contentView addSubview:timeLabel];
    [self.contentView addSubview:titleLabel];
    
    NSDictionary *elementDict=NSDictionaryOfVariableBindings(titleLabel,timeLabel);
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[titleLabel]-10-[timeLabel(==75)]-|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:elementDict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[titleLabel]-2-|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:elementDict]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-12-[timeLabel]-12-|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:elementDict]];

}
@end
