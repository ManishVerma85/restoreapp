//
//  WebViewController.h
//  Restor
//
//  Created by Deva Palanisamy on 25/06/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+Helper.h"

//mv
@interface WebViewController : GAITrackedViewController<UIWebViewDelegate>{
    UIWebView* webview;
    NSURL* url;
}
-(void)setUrlForWebview:(NSURL*)Url;
@end
