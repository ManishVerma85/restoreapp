//
//  AboutViewController.m
//  Restor
//
//  Created by Deva Palanisamy on 24/06/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
  //  aboutContentList=[NSArray arrayWithObjects:@"Feedback",@"Help Centre",@"Terms of Use",@"Privacy Policy", nil];
    aboutContentList=[NSArray arrayWithObjects:@"Feedback",@"Terms of Use",@"Privacy Policy", nil];
    
    self.navigationController.navigationBar.translucent=NO;
    self.view=[[UIView alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    self.view.backgroundColor=[UIColor groupTableViewBackgroundColor];
    
    self.title=NSLocalizedString(@"About",@"used as a title of a screen");
    
    tableview=[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    tableview.backgroundColor=[UIColor whiteColor];
    tableview.delegate=self;
    tableview.dataSource=self;
    tableview.scrollEnabled=YES;
    tableview.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    tableview.autoresizesSubviews=YES;
    
    self.navigationController.navigationBarHidden=NO;
    
    UILabel *titleLabel=[UIViewController getTitleLabelWithFrame:self.view.frame];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.text=NSLocalizedString(@"About",@"used as a title of a screen");
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    self.navigationItem.titleView=titleLabel;
    [self.view addSubview:tableview];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden=NO;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
  //  [self.navigationController.navigationItem.leftBarButtonItem setTitle:@"Settings"];
    
    
    /*UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Settings"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(handleBack:)];
    backButton.tintColor = [UIColor whiteColor];
    
    self.navigationItem.leftBarButtonItem = backButton;
*/
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];

}

- (void) handleBack:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
 //   array=nil;
   // filterContent=nil;
  //  nonFilterContent=nil;
  //  titles=nil;
}
-(void) dismissEvent:(id)sender
{
    [self.navigationController  dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0)
    {
        return 1;
    }
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    UITableViewCell *cell;// = [tableview dequeueReusableCellWithIdentifier:MyIdentifier];
    
    //if (YES)
   // {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:MyIdentifier];
    //}
    
    if (indexPath.section==0)
    {
        
        
        UIImageView *backGroundImageView=[UIImageView new];
        [backGroundImageView setTranslatesAutoresizingMaskIntoConstraints:NO];
        backGroundImageView.contentMode=UIViewContentModeScaleToFill;//UIViewContentModeScaleToFill;
        backGroundImageView.image=[UIImage imageNamed:@"about-us-background"];
        [cell.contentView addSubview:backGroundImageView];
        
        
        
        UIImageView *imageView=[UIImageView new];
        UILabel *appNameLabel=[UILabel new];
        UILabel *appVersion=[UILabel new];
        [imageView setTranslatesAutoresizingMaskIntoConstraints:NO];
        [appNameLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        [appVersion setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [imageView setImage:[UIImage imageNamed:@"restorLogo"]];
        [imageView setContentMode:UIViewContentModeCenter];
        imageView.layer.cornerRadius=10;
        imageView.layer.masksToBounds=YES;
        imageView.layer.borderWidth=2;
        imageView.layer.borderColor=[UIColor clearColor].CGColor;
        imageView.backgroundColor=[UIColor clearColor];
        appNameLabel.backgroundColor=[UIColor clearColor];
        appVersion.backgroundColor=[UIColor clearColor];
        
      /*  appNameLabel.text=@"Restore by Enigma Recovery";
        appNameLabel.textAlignment=NSTextAlignmentCenter;
        appNameLabel.font=[UIFont systemFontOfSize:15];  //[UIFont fontWithName:@"Gotham-Book" size:15];
        appNameLabel.textColor=[UIColor whiteColor];
        *///mv
//        appVersion.text=@"Version 1.0.0";
        NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
     //   appVersion.text = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
     //   appVersion.text=@"Version 1.5";
        
        NSString * versionString =  NSLocalizedString(@"Version",@"");
        NSString * versionText = [[NSString alloc] initWithFormat:@"%@ %@",versionString,[infoDictionary objectForKey:@"CFBundleShortVersionString"]];
        
        appVersion.text = versionText;
        
        appVersion.textAlignment=NSTextAlignmentCenter;
        appVersion.font=[UIFont systemFontOfSize:10];//[UIFont fontWithName:@"Gotham-Book" size:10];
        appVersion.textColor=[UIColor whiteColor];
        appVersion.shadowColor=[UIColor blackColor];
        appVersion.shadowOffset= CGSizeMake(0, 1);
        
        
        
     //   [cell.contentView addSubview:imageView];
     //   [cell.contentView addSubview:appNameLabel];
        [cell.contentView addSubview:appVersion];
        

        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:backGroundImageView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:backGroundImageView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeLeft multiplier:1 constant:0]];
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:backGroundImageView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeBottom multiplier:1 constant:0]];
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:backGroundImageView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeRight multiplier:1 constant:0]];
       /*  
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeTop multiplier:1 constant:30]];
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:76]];
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:imageView attribute:NSLayoutAttributeWidth multiplier:1 constant:0]];
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
        
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:appNameLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:imageView attribute:NSLayoutAttributeBottom multiplier:1 constant:10]];
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:appNameLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:220]];
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:appNameLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:30]];
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:appNameLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
   
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:appVersion attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:appNameLabel attribute:NSLayoutAttributeBottom multiplier:1 constant:-10]];
      [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:appVersion attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:100]];
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:appVersion attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:30]];
     */
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:appVersion attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
        
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:appVersion attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeBottom multiplier:1 constant:-20]];

    }
    else if(indexPath.row < 3)
    {
        cell.textLabel.text=[aboutContentList objectAtIndex:indexPath.row];
        cell.textLabel.font=[UIFont systemFontOfSize:15];//[UIFont fontWithName:@"Gotham-Book" size:15];
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    }
    else
    {
        cell.backgroundColor = [UIColor groupTableViewBackgroundColor];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section==0 && indexPath.row==0)
    {
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
        {
            return 450;
        }
        else
        {
            return 225;
        }

    }
    else
    {
        return 50;
    }
}

#pragma mark- UITableViewDelegate Methods

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    return @"";
    
    switch (section) {
        case 0:
            return @"section 0";
            
        case 1:
            return @"section 1";
            
        default:
            return @"section default";
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *urlString;
    
    if (indexPath.section==1)
    {
        switch (indexPath.row)
        {
            case 0:
                break;
       /*     case 1: //this is the help centre now removed for the time being
            {
                urlString=NSLocalizedString(@"http://www.enigma-recovery.com/support-app", @"support url doesnt need translating but the correct url for that country")  ;
                break;
            }
         */   case 1:
            {
                urlString=NSLocalizedString(@"http://www.enigma-recovery.com/terms-and-conditions", @"terms and conditions url doesnt need translating but the correct url for that country");
                break;
            }
            case 2:
            {
                urlString=NSLocalizedString(@"http://www.enigma-recovery.com/enigma-recovery-privacy-policy/", @"support url doesnt need translating but the correct url for that country");
                break;
            }
            default:
                return;//nothing else to display
                break;
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section==1 && indexPath.row!=0)
    {
        WebViewController* wvc=[[WebViewController alloc]init];
        wvc.title=[aboutContentList objectAtIndex:indexPath.row];
        NSURL *url=[NSURL URLWithString:urlString];
        [wvc setUrlForWebview:url];
        [self.navigationController pushViewController:wvc animated:YES];
    }
    else if (indexPath.section==1 && indexPath.row==0)
    {
        ContactUsViewController *cuvc=[[ContactUsViewController alloc]init];
        [self.navigationController pushViewController:cuvc animated:YES];
    }
    
}


- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    /*if (section == 0)
        return 1.0f;
    return 32.0f;*/
    
    return 0.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
 //   if (section == tableView.numberOfSections - 1)
    {
        UIView * viewBack = [UIView new];
           viewBack.backgroundColor = [UIColor groupTableViewBackgroundColor];
        return viewBack;
    }
   // return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    /*if (section == tableView.numberOfSections - 1) {
        return 1;
    }*/
    return 0;
}

@end
