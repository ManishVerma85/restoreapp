//
//  UIViewController+Helper.h
//  Restor
//
//  Created by Deva Palanisamy on 30/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Helper)
+(UILabel*)getTitleLabelWithFrame:(CGRect)frame;
@end
