//
//  SettingsViewController.h
//  Restor
//
//  Created by Deva Palanisamy on 18/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "UIViewController+Helper.h"
#import "AllMacros.h"
#import "AboutViewController.h"

//mv
@interface SettingsViewController : GAITrackedViewController<UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate,UIAlertViewDelegate>{
    UITableView *tableView;    
    NSArray *infoArray;
    UIButton *fbButton;
    UIButton *shareBtn;
    NSUserDefaults *defaults;
}

//mv
@property (nonatomic, copy) NSString *screenName;

@end
