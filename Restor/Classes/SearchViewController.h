//
//  SearchViewController.h
//  Restor
//
//  Created by Deva Palanisamy on 18/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+Helper.h"
#import "HomeDetailTableViewCell.h"
#import "NSDate+NVTimeAgo.h"
#import "MessagesViewController.h"
#import "NotesViewController.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>

//mv
@interface SearchViewController : GAITrackedViewController<UISearchBarDelegate,UITableViewDelegate,UITextFieldDelegate,UITableViewDataSource>{
    UITableView *tableView;
    UISearchBar *searchBar;
    BOOL isSearching;
     NSArray* messages;
     NSArray* contacts;
     NSArray* callsHistory;
     NSArray* calendar;
     NSArray* whatsapp;
     NSArray* notes;
    __weak NSDictionary *contentDictionary;
    NSMutableArray* searchResults;
    NSPredicate* predicate;
    NSArray *types;
    NSString* type;
    EKEventStore * eventStore;
    NSUserDefaults *defaults;
}

//mv
@property (nonatomic, copy) NSString *screenName;


@property (atomic,retain) NSPredicate * namePredicate;
-(void)prepareSearchContent;
@end
