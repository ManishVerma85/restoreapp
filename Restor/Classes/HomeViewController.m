//
//  HomeViewController.m
//  Restor
//
//  Created by Deva Palanisamy on 18/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import "fileUtlities.h"
#import "TJSpinner.h"
#import "HomeViewController.h"
#import "AFHTTPRequestOperation.h"
//#import "TokenManager.h"
#import "HTProgressHUD.h"
#import "KoaPullToRefresh.h"
#import "UserDefaultManager.h"
#import "AppDelegate.h"

@interface HomeViewController ()
{
    HTProgressHUD *HUD;
}

@end

@implementation HomeViewController


-(NSString *)getDocumentDirectoryPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSLog(@"%@",paths);
    return documentsDirectory;
}

-(void)checkForDeviceAndLoadData
{
    NSLog(@"CheckForDeviceAndLoadData");
    
    
    if ([RestoreManager sharedManager].currentDevice==nil)
    {
        
        [self performSelector:@selector(showiRateMessage)  withObject:nil afterDelay:26.0f];
        
        [self reloadTable];
        
        
    }
    
  /*      TJSpinner *spinner = [[TJSpinner alloc] initWithSpinnerType:kTJSpinnerTypeActivityIndicator];
        spinner.hidesWhenStopped = YES;
        [spinner setColor:[UIColor blackColor]];
        [spinner setInnerRadius:15];
        [spinner setOuterRadius:35];
        [spinner setStrokeWidth:6];
        [spinner setNumberOfStrokes:8];
        [spinner setPatternLineCap:kCGLineCapButt];
        [spinner setPatternStyle:TJActivityIndicatorPatternStyleDot];
        
    */
        
        
  /*1      TJSpinner *circularSpinner = [[TJSpinner alloc] initWithSpinnerType:kTJSpinnerTypeCircular];
        circularSpinner.hidesWhenStopped = YES;
        circularSpinner.radius = 10;
        circularSpinner.pathColor = [UIColor whiteColor];
        circularSpinner.fillColor = [UIColor colorWithRed:(38.0f/255.0f) green:(142.0f/255.0f) blue:(217.0f/255.0f) alpha:1.0f];// [UIColor redColor];
        circularSpinner.thickness = 7;
        //[spinner release];
        
        
        float xpos = (self.view.bounds.size.width-(circularSpinner.bounds.size.width))/2;
        float  ypos = (self.view.bounds.size.height-(circularSpinner.bounds.size.height))/2;
        
        
        NSLog(@"frame width = %f  height = %f ",self.view.bounds.size.width, self.view.bounds.size.height);
              
        CGRect newf;
        newf.origin.x = xpos;
        newf.origin.y = ypos;
        newf.size.height = circularSpinner.bounds.size.height;//.radius *4;
        newf.size.width = circularSpinner.bounds.size.width;//.radius *4;
        
        NSLog(@"spinner boundsframe width = %f  height = %f ",circularSpinner.bounds.size.width, circularSpinner.bounds.size.height);
        NSLog(@"spinner boundsframe width = %f  height = %f ",xpos, ypos);
        
        circularSpinner.frame = newf;
        */
      /*  spinnerView = [spinnerArray objectAtIndex:i];
        [spinnerView setBounds:CGRectMake(0,0 , [spinnerView frame].size.width, [spinnerView frame].size.height)];
        [spinnerView setCenter:CGPointMake((spinnerViewWidth/2.00)+i*spinnerViewWidth, cell.contentView.center.y)];
        [cell addSubview:spinnerView];
*/
  /*1      [self.view addSubview:circularSpinner];
        
        [circularSpinner startAnimating];
        
        
        // how we stop refresh from freezing the main UI thread
        dispatch_queue_t downloadQueue = dispatch_queue_create("downloader", NULL);
        dispatch_async(downloadQueue, ^{
            
            // do our long running process here
          //  [NSThread sleepForTimeInterval:1];
            
            // do any UI stuff on the main UI thread
            dispatch_async(dispatch_get_main_queue(), ^{
               // self.myLabel.text = @"After!";
                
                
                
                NSString *strFirstDevice = [[JSONReader sharedReader] getPathForDeviceAtIndex:0];
                
                if (strFirstDevice)
                {
  */                  /*  if ([[strFirstDevice lastPathComponent] rangeOfString:@"#"].location==NSNotFound)
                     {
                     [RestoreManager sharedManager].currentDevice = [[Device alloc] initWithOldDataFilePath:strFirstDevice];
                     }
                     else
                     {
                     */
                    
  /*1                  [RestoreManager sharedManager].currentDevice = [[Device alloc] initWithFilePath:strFirstDevice];
                    //}
                }
                
                [tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
                
                
                
                [circularSpinner stopAnimating];
            });
            
        });
    //    dispatch_release(downloadQueue);
        
        
    }
    
    
    NSLog(@"spinner starting to animate");
   */
      //  UIActivityIndicatorView
//        [spinner  showWithAnimation:YES aboveView:self.view whileExecutingBlock:^
        
        //[spinner stopAnimating];
      //  HTProgressHUD *hud = [[HTProgressHUD alloc] init];
       // [hud showWithAnimation:YES aboveView:self.view whileExecutingBlock:^
   //      {
 //        NSString *strFirstDevice = [self getDocumentDirectoryPath];//[[JSONReader sharedReader] getPathForDeviceAtIndex:0];
   
  /*           NSString *strFirstDevice = [[JSONReader sharedReader] getPathForDeviceAtIndex:0];
         
             if (strFirstDevice)
             {
    */       /*  if ([[strFirstDevice lastPathComponent] rangeOfString:@"#"].location==NSNotFound)
             {
                 [RestoreManager sharedManager].currentDevice = [[Device alloc] initWithOldDataFilePath:strFirstDevice];
             }
             else
             {
             */
             
  /*               [RestoreManager sharedManager].currentDevice = [[Device alloc] initWithFilePath:strFirstDevice];
             //}
             }
             // sleep(1.0f);
    */        // [spinner stopAnimating];
        /* FILE *file = fopen("the-file.dat", "rb");
         if(file == NULL)
             ; // handle error
         char theBuffer[1000];  // make sure this is big enough!!
         size_t bytesRead = fread(theBuffer, 1, 1000, file);
         if(bytesRead < 1000)
             ; // handle error
         fclose(file);
         */
         
//         [self performSelectorOnMainThread:@selector(showiRateMessage) withObject:nil waitUntilDone:YES];
         
 /*        [tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
         
//         NSData *output1 = [NSData dataWithContentsOfFile:
         
                            
  //      NSMutableData *testFile;
         
    //     [testFile ]
         
         
//          [self performSelector:@selector(showiRateMessage) withObject:nil afterDelay:0.3];
//          [APPDELEGATE showiRatePopUp];
//         }
//        ];
        
  */
  //  }
}



//mv
- (void)showiRateMessage
{
    //mv
//    [APPDELEGATE showiRatePopUp];
    
    //mv
    //set the bundle ID. normally you wouldn't need to do this
    //as it is picked up automatically from your Info.plist file
    //but we want to test with an app that's actually on the store
    [iRate sharedInstance].appStoreID = 910568421;
    [iRate sharedInstance].applicationBundleID = @"74Y2QYH7DF";
    [iRate sharedInstance].onlyPromptIfLatestVersion = NO;
    
    
    //enable preview mode
    [iRate sharedInstance].previewMode = YES;
    
    
    if ([[iRate sharedInstance] shouldPromptForRating])
    {
        NSLog(@"about to prompt for rating");
        [[iRate sharedInstance ] promptIfAllCriteriaMet];
    };
    
}



-(void) tableView:(UITableView *)tableViewLocal willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
   /* if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row)
    {
        //Finished loading visible part of your table
        if (APPDELEGATE.didUserRequestForRemindLaterOption == NO)
        {
//            [self showiRateMessage];
        }
        
    }*/
}



- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        UITabBarItem *barItem = [[UITabBarItem alloc]initWithTitle:NSLocalizedString(@"Home",@"") image:[UIImage imageNamed:@"home"] tag:0];
        [self setTabBarItem:barItem];
        
      //  [self performSelector:@selector(checkForDeviceAndLoadData) withObject:nil afterDelay:2.0f];
        
        [self setInitialValues];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData) name:@"DownloadContentNotification" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTable) name:@"RefreshHomeView" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disPlaySelectedDeviceContent) name:@"deviceSelected" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBadge:) name:@"updateBadge" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopAnimating) name:@"stopAnimating" object:nil];
        
        
        //mv
        // Set screen name.
        self.screenName = NSLocalizedString(@"Home Screen",@"");
        
    }
    
    return self;
}

- (void)viewDidLoad
{

    [super viewDidLoad];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    self.view=[[UIView alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    self.view.backgroundColor=[UIColor yellowColor];
    
    UILabel *titleLabel=[UIViewController getTitleLabelWithFrame:self.view.frame];
    titleLabel.text=NSLocalizedString(@"Restore",@"");
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.backgroundColor=[UIColor clearColor];
    [titleLabel sizeToFit];
    self.navigationItem.titleView=titleLabel;
    self.navigationController.navigationBar.translucent=NO;

    self.view.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.navigationController.navigationBarHidden=NO;
    self.view.autoresizesSubviews=YES;
    self.view.clipsToBounds=YES;
    tableView=[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [self setTabelViewCellHeight];
    tableView.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    tableView.clipsToBounds=YES;
    tableView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    tableView.autoresizesSubviews=YES;
    tableView.scrollEnabled=YES;
    tableView.dataSource=self;
    tableView.delegate=self;
    [tableView reloadData];
    [self.view addSubview:tableView];
    


    
    [tableView.pullToRefreshView setFontAwesomeIcon:@"restorLogo"];
    [tableView.pullToRefreshView setTextFont:[UIFont fontWithName:@"OpenSans-Bold" size:14]];
    
    
    // = [NSTimer scheduledTimerWithTimeInterval:2.6 target:self selector:@selector(checkForData) userInfo:nil repeats:YES];
    
    _isVirgin=YES;
    
    // run the check for data timer in the background
    _checkDataTimer = [NSTimer timerWithTimeInterval:5.2
                                             target:self
                                           selector:@selector(checkForData)
                                           userInfo:nil repeats:YES];
    
    [[NSRunLoop mainRunLoop] addTimer:_checkDataTimer forMode:NSRunLoopCommonModes];
    
    __weak typeof(UITableView*) weaktableView = tableView;
    
    /*TJSpinner *circularSpinner = [[TJSpinner alloc] initWithSpinnerType:kTJSpinnerTypeCircular];
    circularSpinner.hidesWhenStopped = YES;
    circularSpinner.radius = 10;
    circularSpinner.pathColor = [UIColor whiteColor];
    circularSpinner.fillColor = [UIColor colorWithRed:(38.0f/255.0f) green:(142.0f/255.0f) blue:(217.0f/255.0f) alpha:1.0f];// [UIColor redColor];
    circularSpinner.thickness = 7;
    
    
    
    float xpos = (self.view.bounds.size.width-(circularSpinner.bounds.size.width))/2;
    float  ypos = (self.view.bounds.size.height-(circularSpinner.bounds.size.height))/2;
    
    
    NSLog(@"frame width = %f  height = %f ",self.view.bounds.size.width, self.view.bounds.size.height);
    
    CGRect newf;
    newf.origin.x = xpos;
    newf.origin.y = ypos;
    newf.size.height = circularSpinner.bounds.size.height;//.radius *4;
    newf.size.width = circularSpinner.bounds.size.width;//.radius *4;
    
    NSLog(@"spinner boundsframe width = %f  height = %f ",circularSpinner.bounds.size.width, circularSpinner.bounds.size.height);
    NSLog(@"spinner boundsframe width = %f  height = %f ",xpos, ypos);
    
    circularSpinner.frame = newf;
    
    
    [weaktableView addSubview:circularSpinner];
    
    [circularSpinner startAnimating];*/
    
    [tableView addPullToRefreshWithActionHandler:^
    {
        TJSpinner *circularSpinner = [[TJSpinner alloc] initWithSpinnerType:kTJSpinnerTypeCircular];
        circularSpinner.hidesWhenStopped = YES;
        circularSpinner.radius = 10;
        circularSpinner.pathColor = [UIColor whiteColor];
        circularSpinner.fillColor = [UIColor colorWithRed:(38.0f/255.0f) green:(142.0f/255.0f) blue:(217.0f/255.0f) alpha:1.0f];// [UIColor redColor];
        circularSpinner.thickness = 7;
        
        
        
        float xpos = (self.view.bounds.size.width-(circularSpinner.bounds.size.width))/2;
        float  ypos = (self.view.bounds.size.height-(circularSpinner.bounds.size.height))/2;
        
        
        NSLog(@"frame width = %f  height = %f ",self.view.bounds.size.width, self.view.bounds.size.height);
        
        CGRect newf;
        newf.origin.x = xpos;
        newf.origin.y = ypos;
        newf.size.height = circularSpinner.bounds.size.height;//.radius *4;
        newf.size.width = circularSpinner.bounds.size.width;//.radius *4;
        
        NSLog(@"spinner boundsframe width = %f  height = %f ",circularSpinner.bounds.size.width, circularSpinner.bounds.size.height);
        NSLog(@"spinner boundsframe width = %f  height = %f ",xpos, ypos);
        
        circularSpinner.frame = newf;
        
       
        [weaktableView addSubview:circularSpinner];
        
        [circularSpinner startAnimating];
        
        
        
        
        // Create your dispatch queue
        dispatch_queue_t myNewQueue = dispatch_queue_create("my Queue", NULL);
        
        // Dispatch work to your queue
        dispatch_async(myNewQueue, ^{
            
            // Perform long activity here.
            
            [[RestoreManager sharedManager].currentDevice refreshContent];
            
            [weaktableView reloadData];
            
            
            
            // Dispatch work back to the main queue for your UIKit changes
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // update your UI here from your changes.
                [circularSpinner stopAnimating];
                [weaktableView.pullToRefreshView stopAnimating];
            });
        });
        
        
        
        //[spinner release];
        
      //  [tableView.pullToRefreshView startAnimating];
   /*     [[RestoreManager sharedManager].currentDevice refreshContent];
        
        [weaktableView reloadData];

        [weaktableView.pullToRefreshView stopAnimating];
        
     */
        
    //
      //  [circularSpinner stopAnimating];
        
        //[(HomeViewController *)weaktableView.superview reloadTable];
    }];
}



/**
 Determine if data has been found*/
-(void)checkForData
{
    NSLog(@"checking for data homeViewscreen");
    
    
    
    
    if (_isVirgin)
    {
        _currentDataCountMain = [fileUtlities getNumberDirectories:[RestoreManager sharedManager].currentDevice.contentFilePath];
        _isVirgin=NO;
    }
    else
    {
        int testCount = [fileUtlities getNumberDirectories:[RestoreManager sharedManager].currentDevice.contentFilePath];
        
        if (testCount!=_currentDataCountMain)
        {
            //more data has been added now check
            NSLog(@"more data has been added");
            //[self reloadTable];
            
            //[tableView reloadData];
            [self performSelectorOnMainThread:@selector(reloadTable) withObject:nil waitUntilDone:YES];
            _currentDataCountMain = testCount;
        }
        
    }
    
    
    
}




-(UIRectEdge)edgesForExtendedLayout
{
    return[super edgesForExtendedLayout] ^ UIRectEdgeBottom;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self performSelector:@selector(checkForDeviceAndLoadData) withObject:nil afterDelay:2.0f];
    contentCount=0;
    //Mixpanel *mixpanel = [Mixpanel sharedInstance];
    //[mixpanel track:@"Home"];
    //[tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)setInitialValues
{
    title = @[NSLocalizedString(@"Messages",@""),
              NSLocalizedString(@"Contacts",@""),
              NSLocalizedString(@"Call History",@""),
              NSLocalizedString(@"Calendar",@""),
              NSLocalizedString(@"Chat History",@""),
              NSLocalizedString(@"Notes",@"")];
    
    descriptionText = @[NSLocalizedString(@"All restored Messages",@""),
                     NSLocalizedString(@"All restored Contacts",@""),
                     NSLocalizedString(@"All restored Call History",@""),
                     NSLocalizedString(@"All restored Calendar Events",@""),
                     NSLocalizedString(@"All restored Chat History",@""),
                     NSLocalizedString(@"All restored Notes",@"")];
    
    images = @[@"SMS_on.png",
                @"imessage_on.png",
                @"Notes_on.png",
                @"Calls_on.png",
                @"Whats_App_on.png",
                @"Calendar_on.png",
                @"Contacts_on.png"];
}

#pragma mark - Table view delegate implementation
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableViewLocal
{
    //makes the lines go all the way across
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
  //  tableView.layoutMargins = UIEdgeInsetsZero;
    
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [title count];
}

- (void)tableView:(UITableView *)tableview didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    /*
    RecoveredContent *recoveredContent=[RecoveredContent sharedInstance];
    if ([recoveredContent.list valueForKey:[title objectAtIndex:indexPath.row]]==nil) {
        return;
    }
     */
    [tableview deselectRowAtIndexPath:indexPath animated:YES];
    [self selectViewController:indexPath];
}

- (HomeTableViewCell *)tableView:(UITableView *)tableview cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    

   // if ([tableView respondsToSelector:@selector(setSeparatorStyle:)]) { [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine]; }
    
    static NSString *MyIdentifier = @"MyIdentifier";

    HomeTableViewCell* cell = [[HomeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    
    //makes the lines go all the way across
  //  cell.layoutMargins = UIEdgeInsetsZero;
    
    
   /* CGRect textLabelFrame = cell.detailTextLabel.frame;
    textLabelFrame.origin.x+=100;
    textLabelFrame.origin.y-=140;
    
    cell.detailTextLabel.frame=textLabelFrame;
    
    //textLabelFrame.size.width -= xOffset;
    */
    UIImage *image1=nil;
    
    
 //   cell.titleLabel.frame = textLabelFrame;
 //   cell.detailTextLabel.frame =textLabelFrame;
 //   cell.detailText.frame = textLabelFrame;
    
    
    
    if ([[title objectAtIndex:indexPath.row] isEqualToString:NSLocalizedString(@"Chat History",@"")])
    {
        image1=[UIImage imageNamed:@"Whatsapp"];
        [cell.thumbnail setImage:image1];
    }
    else if ([[title objectAtIndex:indexPath.row] isEqualToString:NSLocalizedString(@"Call History",@"")])
    {
        image1=[UIImage imageNamed:@"Calls"];
        [cell.thumbnail setImage:image1];
    }
    else
    {
        image1=[UIImage imageNamed:[title objectAtIndex:indexPath.row]];
        cell.thumbnail.image=image1;
    }


    cell.title.text=[title objectAtIndex:indexPath.row];
    cell.title.font= [UIFont systemFontOfSize:16];//[UIFont fontWithName:@"System" size:16];
    cell.thumbnail.contentMode=UIViewContentModeScaleAspectFit;
    cell.thumbnail.layer.cornerRadius=20;
    cell.thumbnail.layer.masksToBounds=NO;
    cell.thumbnail.clipsToBounds=YES;
    cell.descriptionLabel.text=[descriptionText objectAtIndex:indexPath.row];
    NSInteger dataCount=0;
    
    switch (indexPath.row)
    {
        case 0:
            dataCount = [RestoreManager sharedManager].currentDevice.messages.count;
            break;
        case 1:
            dataCount = [RestoreManager sharedManager].currentDevice.contacts.count;
            break;
        case 2:
            dataCount = [RestoreManager sharedManager].currentDevice.callHistory.count;
            break;
        case 3:
            dataCount = [RestoreManager sharedManager].currentDevice.calender.count;
            break;
        case 4:
            dataCount = [RestoreManager sharedManager].currentDevice.whatsApp.count;
            break;
        case 5:
            dataCount = [RestoreManager sharedManager].currentDevice.notes.count;
            break;
        default:
            break;
    }
    
    UIColor *deepRed = [UIColor colorWithRed:195.0f/255.0f
                                               green:36.0f/255.0f
                                                blue:45.0f/255.0f
                                               alpha:1.0f];
  //  cell.descriptionLabel.text=[NSString
   //                             stringWithFormat:@"%ld restored %@",(long)dataCount,[title objectAtIndex:indexPath.row]];
    
    
    if ((long)dataCount!=0)
    {
        

    
    cell.descriptionLabel.text=[NSString stringWithFormat:@" %ld ",(long)dataCount];
        cell.descriptionLabel.font= [UIFont systemFontOfSize:14];//   [UIFont fontWithName:@"System" size:6];
    cell.descriptionLabel.textColor = [UIColor whiteColor];
    cell.descriptionLabel.backgroundColor=deepRed;
    

    
  /*  UIColor *backGroundColor = [UIColor colorWithRed:54.0f/255.0f
                                               green:62.0f/255.0f
                                                blue:73.0f/255.0f
                                               alpha:1.0f];
    
    */
    cell.descriptionLabel.layer.cornerRadius =8;
    cell.descriptionLabel.layer.masksToBounds=NO;
    cell.descriptionLabel.clipsToBounds=YES;

        
    }
    else
    {
        
        UIColor *lightGrey = [UIColor colorWithRed:118.0f/255.0f
                                                   green:118.0f/255.0f
                                                    blue:118.0f/255.0f
                                                   alpha:1.0f];
        
        cell.title.textColor = lightGrey;
        cell.descriptionLabel.text=@"";
        
    }
    
        [cell.descriptionLabel sizeToFit];
    
   /* CGRect descriptionFrame =  cell.descriptionLabel.frame;
    
    descriptionFrame.size.height +=107;
    
    descriptionFrame.origin.y -=40;
    
    cell.descriptionLabel.frame = descriptionFrame;
    */
    

    
    [cell.badge setNeedsDisplay];
    [cell.thumbnail setNeedsDisplay];
    [cell setNeedsDisplay];
    
    
    return cell;
}

#pragma mark - Get custom table view cells
-(void)selectViewController:(NSIndexPath*)indexPath
{
    [tableView.pullToRefreshView stopAnimating];
    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
    
    @autoreleasepool
    {
        HomeDetailViewController* hdvc=[[HomeDetailViewController alloc]init];
        hdvc.title=[title objectAtIndex:indexPath.row];
        //hdvc.filePath=[filesList objectAtIndex:[self findfObjectInFiles:filesList indexPath:indexPath]];
        //[hdvc setArrayValues:[list valueForKey:hdvc.title]];
        switch (indexPath.row) {
            case 0:
                hdvc.array = [[RestoreManager sharedManager].currentDevice.messages mutableCopy];
                break;
            case 1:
                hdvc.array = [[RestoreManager sharedManager].currentDevice.contacts mutableCopy];
                break;
            case 2:
                hdvc.array = [[RestoreManager sharedManager].currentDevice.callHistory mutableCopy];
                break;
            case 3:
                hdvc.array = [[RestoreManager sharedManager].currentDevice.calender mutableCopy];
                break;
            case 4:
                hdvc.array = [[RestoreManager sharedManager].currentDevice.whatsApp mutableCopy];
                break;
            case 5:
                hdvc.array = [[RestoreManager sharedManager].currentDevice.notes mutableCopy];
                break;
            default:
                break;
        }
        [self.navigationController pushViewController:hdvc animated:YES];
    }
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:[title objectAtIndex:indexPath.row]];
}

-(void)setTabelViewCellHeight{
        tableView.rowHeight=75;
}

-(void)loadData
{
    HUD=[[HTProgressHUD alloc] init];
    HUD.textLabel.numberOfLines = 2;
    HUD.textLabel.text=@"Please wait while \n data is downloaded";
    [HUD showInView:self.view];
}

-(void)reloadTable
{
    NSLog(@"reload Table");
    
    
    
    
    TJSpinner *circularSpinner = [[TJSpinner alloc] initWithSpinnerType:kTJSpinnerTypeCircular];
    circularSpinner.hidesWhenStopped = YES;
    circularSpinner.radius = 10;
    circularSpinner.pathColor = [UIColor whiteColor];
    circularSpinner.fillColor = [UIColor colorWithRed:(38.0f/255.0f) green:(142.0f/255.0f) blue:(217.0f/255.0f) alpha:1.0f];// [UIColor redColor];
    circularSpinner.thickness = 7;
    //[spinner release];
    
    
    float xpos = (self.view.bounds.size.width-(circularSpinner.bounds.size.width))/2;
    float  ypos = (self.view.bounds.size.height-(circularSpinner.bounds.size.height))/2;
    
    
    NSLog(@"frame width = %f  height = %f ",self.view.bounds.size.width, self.view.bounds.size.height);
    
    CGRect newf;
    newf.origin.x = xpos;
    newf.origin.y = ypos;
    newf.size.height = circularSpinner.bounds.size.height;//.radius *4;
    newf.size.width = circularSpinner.bounds.size.width;//.radius *4;
    
    NSLog(@"spinner boundsframe width = %f  height = %f ",circularSpinner.bounds.size.width, circularSpinner.bounds.size.height);
    NSLog(@"spinner boundsframe width = %f  height = %f ",xpos, ypos);
    
    circularSpinner.frame = newf;
    
    /*  spinnerView = [spinnerArray objectAtIndex:i];
     [spinnerView setBounds:CGRectMake(0,0 , [spinnerView frame].size.width, [spinnerView frame].size.height)];
     [spinnerView setCenter:CGPointMake((spinnerViewWidth/2.00)+i*spinnerViewWidth, cell.contentView.center.y)];
     [cell addSubview:spinnerView];
     */
    [self.view addSubview:circularSpinner];
    
    [circularSpinner startAnimating];
    
    
    // how we stop refresh from freezing the main UI thread
    dispatch_queue_t downloadQueue = dispatch_queue_create("downloader", NULL);
    dispatch_async(downloadQueue, ^{
        
        // do our long running process here
        //  [NSThread sleepForTimeInterval:1];
        
        // do any UI stuff on the main UI thread
        dispatch_async(dispatch_get_main_queue(), ^{
            // self.myLabel.text = @"After!";
            
            
          /*  if ([RestoreManager sharedManager].currentDevice ==nil )
            {
                NSString *strFirstDevice = [[JSONReader sharedReader] getPathForDeviceAtIndex:0];
                if (strFirstDevice)
                {
                   
                    [RestoreManager sharedManager].currentDevice = [[Device alloc] initWithFilePath:strFirstDevice];
                
                }

            }*/
            
 //           NSString *strFirstDevice = [[JSONReader sharedReader] getPathForDeviceAtIndex:0];
            
//            if (strFirstDevice)
            {
                /*  if ([[strFirstDevice lastPathComponent] rangeOfString:@"#"].location==NSNotFound)
                 {
                 [RestoreManager sharedManager].currentDevice = [[Device alloc] initWithOldDataFilePath:strFirstDevice];
                 }
                 else
                 {
                 */
                
  //              [RestoreManager sharedManager].currentDevice = [[Device alloc] initWithFilePath:strFirstDevice];
                //}
            }
            
           // [RestoreManager sharedManager].currentDevice = [[Device alloc] initWithFilePath:strFirstDevice];
            
            [tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
            
            
            [circularSpinner stopAnimating];
        });
        
    });

    
   // [tableView reloadData];
}


-(void)disPlaySelectedDeviceContent
{
    //RecoveredContent *recoveredContent=[RecoveredContent sharedInstance];
    //[recoveredContent getContentFromStoredFiles];
    [self reloadTable];
}

-(void)updateBadge:(NSString*)contentName
{
    //[badgeDictionary setValue:[NSNumber numberWithBool:YES] forKey:contentName];
}

-(void)updateDatasource
{
    [[RestoreManager sharedManager].currentDevice refreshContent];
    [tableView.pullToRefreshView stopAnimating];
}

-(void)stopAnimating
{
    [tableView reloadData];
    [tableView.pullToRefreshView stopAnimating];
    [HUD hideAfterDelay:2];
}

@end
