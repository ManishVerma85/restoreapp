//
//  HomeViewController.h
//  Restor
//
//  Created by Deva Palanisamy on 18/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeDetailViewController.h"
#import "HomeTableViewCell.h"
#import "UIViewController+Helper.h"

//mv
@interface HomeViewController : GAITrackedViewController<UITableViewDataSource,UITableViewDelegate>{
    @private
    UITableView *tableView;
    NSArray *title;
    NSArray *descriptionText;
    NSMutableDictionary *list;
    NSArray *images;
    int contentCount;
    //HomeDetailViewController* hdvc;
    
    NSTimer * _checkDataTimer;
    
    int _dataFoundCount;
    
    int _currentDataCountMain;
    int _currentDataCountSub;
    
    BOOL _noDevice;
    BOOL _isVirgin;
}

//mv
@property (nonatomic, copy) NSString *screenName;

-(void)stopAnimating;
@end
