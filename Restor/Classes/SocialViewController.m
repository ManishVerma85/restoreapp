//
//  SocialViewController.m
//  Restor
//
//  Created by Deva Palanisamy on 18/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import "SocialViewController.h"
#import "GPPShare.h"
#import "GooglePlus.h"
#import "GoogleOpenSource.h"

@interface SocialViewController ()

@end

@implementation SocialViewController
- (instancetype)init
{
    self = [super init];
    if (self)
    {
        UITabBarItem *barItem = [[UITabBarItem alloc]initWithTitle:NSLocalizedString(@"Tell a friend",@"") image:[UIImage imageNamed:@"tellAfriend"] tag:1];
        self.title=NSLocalizedString(@"Tell a friend",@"");
        [self setTabBarItem:barItem];
        sharingOptionsList=[NSArray arrayWithObjects:NSLocalizedString(@" Via SMS",@"please keep space at front for formatting"),NSLocalizedString(@" Via Email",@"please keep space at front for formatting"),NSLocalizedString(@" Via Facebook",@"please keep space at front for formatting"),NSLocalizedString(@" Via Twitter",@"please keep space at front for formatting"),NSLocalizedString(@" Via Google+",@"please keep space at front for formatting@"), nil];
        
        //mv
        // Set screen name.
        self.screenName = @"Social Screen";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    self.view.backgroundColor=[UIColor whiteColor];
    self.view.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    UILabel *titleLabel=[UIViewController getTitleLabelWithFrame:self.view.frame];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.text=NSLocalizedString(@"Tell a friend",@"");
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    self.navigationItem.titleView=titleLabel;
    
    self.view=[[UIView alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    self.view.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.view.autoresizesSubviews=YES;
    tableView=[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    tableView.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    tableView.autoresizesSubviews=YES;
    tableView.delegate=self;
    tableView.dataSource=self;
    [self.view addSubview:tableView];
}

-(void) tableView:(UITableView *)tableViewLocal willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    /* if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row)
     {
     //Finished loading visible part of your table
     if (APPDELEGATE.didUserRequestForRemindLaterOption == NO)
     {
     //            [self showiRateMessage];
     }
     
     }*/
}

-(void)viewWillAppear:(BOOL)animated
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"tell a friend"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)FaceBookShareAction
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *Facebook = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        Facebook.completionHandler=^(SLComposeViewControllerResult result) {
            switch(result) {
                    //  This means the user cancelled without sending the Tweet
                case SLComposeViewControllerResultCancelled:
                {
                    NSLog(@"Facebook Sheet is cancelled");
                    Mixpanel *mixpanel = [Mixpanel sharedInstance];
                    [mixpanel track:@"Facebook Sharing Cancelled"];
                }
                    break;
                    //  This means the user hit 'Send'
                case SLComposeViewControllerResultDone:
                {
                    NSLog(@"Facebook Sheet is sent");
                    Mixpanel *mixpanel = [Mixpanel sharedInstance];
                    [mixpanel track:@"Shared On Facebook"];
                }
                    break;
            }
            
            //  dismiss the Tweet Sheet
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:NO completion:^{
                    NSLog(@"Tweet Sheet has been dismissed.");
                }];
            });
        };

     //   [Facebook setInitialText:[NSString stringWithFormat:@"Check out this iOS data recovery solution. It restores deleted data back to your iOS device! Download it from www.enigma-recovery.com "]];
      //  [Facebook setInitialText:NSLocalizedString(@"Check out this data transfer solution. It transfers data between different smartphones!",@"initial")];
        
     //   NSString *initialTextString = @"Check out this Tweet!";
    //    [composeViewController setInitialText:initialTextString];
        
     //   [Facebook setInitialText:initialTextString];
        [Facebook addURL: [NSURL URLWithString:@"https://itunes.apple.com/gb/app/restore/id910568421?mt=8"]];
    //    UIImage *calendarTestImage = [UIImage imageNamed:@"Calendar.png"];
        
    //    [Facebook addImage:calendarTestImage];
        
        [self presentViewController:Facebook animated:YES completion:nil];
    }
    else
    {
        UIAlertView *showMessage = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Not Available",@"") message:NSLocalizedString(@"Facebook App can't be accessed",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
        [showMessage show];
    }
}

- (void)TwitterShareAction
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *Twitter = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        Twitter.completionHandler=^(SLComposeViewControllerResult result) {
            switch(result) {
                    //  This means the user cancelled without sending the Tweet
                case SLComposeViewControllerResultCancelled:
                    {
                    NSLog(@"Tweet Sheet is cancelled");
                    Mixpanel *mixpanel = [Mixpanel sharedInstance];
                    [mixpanel track:@"Twitter Sharing Cancelled"];
                    }
                    break;
                    //  This means the user hit 'Send'
                case SLComposeViewControllerResultDone:
                    {
                        NSLog(@"Tweet Sheet is sent");
                        Mixpanel *mixpanel = [Mixpanel sharedInstance];
                        [mixpanel track:@"Shared On Twitter"];
                    }
                    break;
            }
            
            //  dismiss the Tweet Sheet
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:NO completion:^{
                    NSLog(@"Tweet Sheet has been dismissed.");
                }];
            });
        };
        
        //[Twitter setInitialText:[NSString stringWithFormat:@"Check out this iOS data recovery solution. It restores deleted data back to your iOS device! Download it from www.enigma-recovery.com "]];
        
        [Twitter setInitialText:NSLocalizedString(@"Check out this data transfer solution. It transfers data between different smartphones! Download it here http://apple.co/1iU0GVk #RestoreApp",@"")];
        
        
        [self presentViewController:Twitter animated:YES completion:nil];
    }
    else
    {
        UIAlertView *showMessage = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Not Available",@"") message:NSLocalizedString(@"Twitter can't be accessed, please install Twitter and Login",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
        [showMessage show];
    }
}

-(void) sendInAppSMS:(id) sender
{
	MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    
	if([MFMessageComposeViewController canSendText])
	{
		controller.body = NSLocalizedString(@"Check out this data transfer solution. It transfers data between different smartphones! Download it here http://apple.co/1iU0GVk",@"");
		controller.messageComposeDelegate = self;
		[self presentViewController:controller animated:YES completion:nil];
	}
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
	switch (result) {
		case MessageComposeResultCancelled:
            {
			NSLog(@"SMS Cancelled");
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"SMS Sharing Cancelled"];
            }
			break;
		case MessageComposeResultFailed:
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Announcement" message: @"Unknown Error" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            //			[alert show];
            NSLog(@"Unknown Error");
			break;
		case MessageComposeResultSent:
            {
                NSLog(@"SMS sent");
                Mixpanel *mixpanel = [Mixpanel sharedInstance];
                [mixpanel track:@"Shared Via SMS"];
            }
			break;
		default:
			break;
	}
    
	[self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - compose a mail

- (void)showEmail:(id)sender
{
    
    if ([MFMailComposeViewController canSendMail])
    {
        // Email Subject
        NSString *emailTitle = NSLocalizedString(@"Restore App link ",@"");
        // Email Content
    
        
        NSString *messageBody = NSLocalizedString(@"<p>Check out this data transfer solution. It transfers data between different smartphones! Download it <a href=\"https://itunes.apple.com/gb/app/restore/id910568421?mt=8)\">here</a>.</p>",@"");
        
                                                  
                                                  // (Please put the Link in the text for ‘App Store’: https://itunes.apple.com/gb/app/restore/id910568421?mt=8)",@"");
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:@""];
    
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:YES];
        [mc setToRecipients:toRecipents];
    
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"Shared Via Email"];
    }
    else
    {
        UIAlertView *showMessage = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Not Available",@"") message:NSLocalizedString(@"Email has not been configured. Please setup an email account to send emails.",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
        [showMessage show];
    }
    
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            {
            NSLog(@"Mail cancelled");
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Email Sharing Cancelled"];
            }
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            {
            NSLog(@"Mail sent");
                Mixpanel *mixpanel = [Mixpanel sharedInstance];
                [mixpanel track:@"Shared Via Email"];
            }
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - google plus sharing
- (void) shareViaGooglePlus: (id)sender
{
    NSURL *gPlusUrl = [NSURL URLWithString:@"gplus://plus.google.com/share?url=http%3A%2F%2Fwww.infinity-wireless.com"];
    
    if ([[UIApplication sharedApplication] canOpenURL:gPlusUrl])
    {
        [[UIApplication sharedApplication] openURL:gPlusUrl];
    }
    else
    {
        UIAlertView *showMessage = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Not Available",@"") message:NSLocalizedString(@"Google+ can't be accessed",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
        [showMessage show];
    }
}

#pragma mark - Tableview delegate and data source implementation

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [sharingOptionsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableview cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseIndetifier=@"cell";
    
    UITableViewCell *cell=[tableview dequeueReusableCellWithIdentifier:reuseIndetifier];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIndetifier];
    }
    switch (indexPath.row) {
        case 0:
        {
            cell.imageView.image=[UIImage imageNamed:@"sms"];
        }
            break;
        case 1:
        {
            cell.imageView.image=[UIImage imageNamed:@"email"];
        }
            break;
        case 2:
        {
            cell.imageView.image=[UIImage imageNamed:@"facebook"];
        }
            break;
        case 3:
        {
            cell.imageView.image=[UIImage imageNamed:@"twitter"];
        }
            break;
        case 4:
        {
            cell.imageView.image=[UIImage imageNamed:@"google-plus"];
        }
            break;
    }
    cell.imageView.layer.cornerRadius=5;
    cell.imageView.layer.masksToBounds=NO;
    cell.imageView.clipsToBounds=YES;
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text=[sharingOptionsList objectAtIndex:indexPath.row];
    cell.textLabel.font= [UIFont boldSystemFontOfSize:15.0f];//[UIFont fontWithName:@"Gotham-Book" size:15];
    return cell;
}

- (void)tableView:(UITableView *)tableview didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableview deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
                {
                    [self sendInAppSMS:nil];
                }
                break;
        case 1:
                {
                    [self showEmail:nil];
                }
                break;
        case 2:
                {
                    [self FaceBookShareAction];
                }
                break;
        case 3:
                {
                    [self TwitterShareAction];
                }
                break;
        case 4:
                {
                    [self shareViaGooglePlus:nil];
                }
                break;
    }
}
@end
