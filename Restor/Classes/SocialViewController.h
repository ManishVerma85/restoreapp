//
//  SocialViewController.h
//  Restor
//
//  Created by Deva Palanisamy on 18/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "UIViewController+Helper.h"

static CGRect fbButtonFrame={{100,70},{130,130}};
static CGRect twitterButtonFrame={{100,230},{130,130}};

//mv
@interface SocialViewController : GAITrackedViewController<MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate,UIActionSheetDelegate,UITableViewDataSource,UITableViewDelegate>{
    UIButton *fbButton;
    UIButton *shareBtn;
    UITableView *tableView;
    NSArray *sharingOptionsList;

}


//mv
@property (nonatomic, copy) NSString *screenName;

@end
