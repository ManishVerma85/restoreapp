//
//  SettingsViewController.m
//  Restor
//
//  Created by Deva Palanisamy on 18/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import "TJSpinner.h"
#import "SettingsViewController.h"
#import "AppDelegate.h"
#import "HTProgressHUD.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        UITabBarItem *barItem = [[UITabBarItem alloc]initWithTitle:NSLocalizedString(@"Settings",@"") image:[UIImage imageNamed:@"settings"] tag:4];
        [self setTabBarItem:barItem];
        //mv
//        infoArray=[NSArray arrayWithObjects:@"About",@"Tell a friend",@"Add a new device",@"Select device", nil];
        infoArray=[NSArray arrayWithObjects:NSLocalizedString(@"About",@""),NSLocalizedString(@"Tell a friend",@""),NSLocalizedString(@"Select device",@""), nil];
        
        //mv
        // Set screen name.
        self.screenName = @"Settings Screen";
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    defaults=[NSUserDefaults standardUserDefaults];
    self.view=[[UIView alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    self.view.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.view.autoresizesSubviews=YES;
    
    tableView=[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    tableView.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    tableView.autoresizesSubviews=YES;
    tableView.delegate=self;
    tableView.dataSource=self;
    self.view.backgroundColor=[UIColor groupTableViewBackgroundColor];
    
    UILabel *titleLabel=[UIViewController getTitleLabelWithFrame:self.view.frame];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.text=NSLocalizedString(@"Settings",@"");
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    
    
    self.navigationItem.titleView=titleLabel;
    [self.view addSubview:tableView];
    
}

-(void) tableView:(UITableView *)tableViewLocal willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    /* if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row)
     {
     //Finished loading visible part of your table
     if (APPDELEGATE.didUserRequestForRemindLaterOption == NO)
     {
     //            [self showiRateMessage];
     }
     
     }*/
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Settings"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view protocols implementation
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return [infoArray count];
            
        case 1:
            return 0;
        default:
            return 0;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    switch (section) {
        case 0:
            return @"";
            return @"Information";
            
        case 1:
            return @"";
            return @"Information1";
        default:
            return @"";
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableview cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableview dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    // Set up the cell...
    if (indexPath.section==0 && indexPath.row==0)
    {
        cell.imageView.image=[UIImage imageNamed:@"about"];
    }
    else if (indexPath.section==0 && indexPath.row==1)
    {
        cell.imageView.image=[UIImage imageNamed:@"tellafriend2"];
    }
  //  else if (indexPath.section==1 && indexPath.row==0)
  //  {
 //       cell.imageView.image=[UIImage imageNamed:@"logout"];
  //  }
    else if (indexPath.section==0 && indexPath.row==2)
    {
        cell.imageView.image=[UIImage imageNamed:@"add-device"];
    }
    else if (indexPath.section==0 && indexPath.row==3)
    {
        cell.imageView.image=[UIImage imageNamed:@"select-device"];
    }
    
    cell.imageView.layer.cornerRadius=5;
    cell.imageView.layer.masksToBounds=NO;
    cell.imageView.clipsToBounds=YES;
    
    
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
//    if(indexPath.section == 0)
        cell.textLabel.text = [infoArray objectAtIndex:indexPath.row];
 //   else
 //       cell.textLabel.text = @"Logout";
    
    cell.textLabel.font=UI_FONT_SETTINGS_TABLE;
    return cell;
}

- (void)tableView:(UITableView *)tableview didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableview deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section==0 && indexPath.row==1)
    {
        [self displayActionSheet:nil];
    }
    else if (indexPath.section==0 && indexPath.row==0)
    {
        AboutViewController *avc=[[AboutViewController alloc]init];
        [self.navigationController pushViewController:avc animated:YES];
    }
//    else if (indexPath.section==1 && indexPath.row==0)
//    {
//        [self logout];
//    }
    //mv since client removed the optin 'Add a new device' from the settigs options, we need to change the indexpath.row here also
    /*
    else if (indexPath.section==0 && indexPath.row==2){
        [self addNewDevice];
    }else if (indexPath.section==0 && indexPath.row==3){
        [self selectDevice];
    }
     */
    else if (indexPath.section==0 && indexPath.row==2){
       [self selectDevice];
    }

}

- (void)displayActionSheet:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Tell a friend about Restore via...",@"") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"SMS",@""), NSLocalizedString(@"Email",@""), NSLocalizedString(@"Facebook",@""), NSLocalizedString(@"Twitter",@""), NSLocalizedString(@"Google+",@""), nil];
    actionSheet.tag=0;
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault; [actionSheet showInView:self.view];
}

-(void) shareViaGooglePlus: (id)sender
{
    NSURL *gPlusUrl = [NSURL URLWithString:@"gplus://plus.google.com/share?url=http%3A%2F%2Fwww.infinity-wireless.com"];
    
    if ([[UIApplication sharedApplication] canOpenURL:gPlusUrl]) {
        [[UIApplication sharedApplication] openURL:gPlusUrl];
    }else{
        UIAlertView *showMessage = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Not Available",@"") message:NSLocalizedString(@"Google+ App can't be accessed",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
        [showMessage show];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag==0) {
        switch (buttonIndex)
        {
            case 1:
                [self showEmail:nil];
                break;
            case 0:
                [self sendInAppSMS:nil];
                break;
            case 3:
                [self TwitterShareAction];
                break;
            case 2:
                [self FaceBookShareAction];
                break;
            case 4:
                [self shareViaGooglePlus:nil];
                break;
        }

    }
    else if (actionSheet.tag==1)
    {
        if (buttonIndex!=(actionSheet.numberOfButtons-1))
        {
            NSString *deviceName = [[JSONReader sharedReader] getPathForDeviceAtIndex:buttonIndex];
            
            TJSpinner *circularSpinner = [[TJSpinner alloc] initWithSpinnerType:kTJSpinnerTypeCircular];
            circularSpinner.hidesWhenStopped = YES;
            circularSpinner.radius = 10;
            circularSpinner.pathColor = [UIColor whiteColor];
            circularSpinner.fillColor = [UIColor colorWithRed:(38.0f/255.0f) green:(142.0f/255.0f) blue:(217.0f/255.0f) alpha:1.0f];// [UIColor redColor];
            circularSpinner.thickness = 7;
            //[spinner release];
            
            
            float xpos = (self.view.bounds.size.width-(circularSpinner.bounds.size.width))/2;
            float  ypos = (self.view.bounds.size.height-(circularSpinner.bounds.size.height))/2;
            
            
            NSLog(@"frame width = %f  height = %f ",self.view.bounds.size.width, self.view.bounds.size.height);
            
            CGRect newf;
            newf.origin.x = xpos;
            newf.origin.y = ypos;
            newf.size.height = circularSpinner.bounds.size.height;//.radius *4;
            newf.size.width = circularSpinner.bounds.size.width;//.radius *4;
            
            NSLog(@"spinner boundsframe width = %f  height = %f ",circularSpinner.bounds.size.width, circularSpinner.bounds.size.height);
            NSLog(@"spinner boundsframe width = %f  height = %f ",xpos, ypos);
            
            circularSpinner.frame = newf;
            
            /*  spinnerView = [spinnerArray objectAtIndex:i];
             [spinnerView setBounds:CGRectMake(0,0 , [spinnerView frame].size.width, [spinnerView frame].size.height)];
             [spinnerView setCenter:CGPointMake((spinnerViewWidth/2.00)+i*spinnerViewWidth, cell.contentView.center.y)];
             [cell addSubview:spinnerView];
             */
            [self.view addSubview:circularSpinner];
            
            [circularSpinner startAnimating];
            
            
            // how we stop refresh from freezing the main UI thread
            dispatch_queue_t downloadQueue = dispatch_queue_create("downloader", NULL);
            dispatch_async(downloadQueue, ^{
                
                // do our long running process here
                [NSThread sleepForTimeInterval:10];
                
                // do any UI stuff on the main UI thread
                dispatch_async(dispatch_get_main_queue(), ^{
                    // self.myLabel.text = @"After!";
                    
                    
                    [self performSelectorOnMainThread:@selector(phoneSelected:) withObject:deviceName waitUntilDone:YES];
                    
                    
                    
                    [circularSpinner stopAnimating];
                });
                
            });

            
            
            
            
            
            
            
            /*  HTProgressHUD *hud = [[HTProgressHUD alloc] init];
            
    
            
            [hud showWithAnimation:YES aboveView:self.view whileExecutingBlock:^{
                
    */            /*
                 if (strFirstDevice)
                 {
                 [RestoreManager sharedManager].currentDevice = [[Device alloc] initWithFilePath:strFirstDevice];
                 }
                 [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"deviceSelected"
                 object:self
                 userInfo:nil];
                 */
      /*          [self performSelectorOnMainThread:@selector(phoneSelected:) withObject:deviceName waitUntilDone:YES];
                
            }];
            
        */    /*
             [defaults setObject:[deviceList objectAtIndex:buttonIndex]
             forKey:@"selectedDevice"];
             [[NSNotificationCenter defaultCenter]
             postNotificationName:@"deviceSelected"
             object:self
             userInfo:nil];
             NSLog(@"Selected Device : %@",[deviceList objectAtIndex:buttonIndex]);
             */
            
        }
        
        
    }
}

-(void)phoneSelected:(NSString *)strFirstDevice
{
    NSLog(@"phone selected %@",strFirstDevice);
    
    if (strFirstDevice)
    {
        [RestoreManager sharedManager].currentDevice = [[Device alloc] initWithFilePath:strFirstDevice];
    }
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"deviceSelected"
     object:self
     userInfo:nil];
}

#pragma mark - social sharing implementation methods
- (void)FaceBookShareAction
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *Facebook = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        Facebook.completionHandler=^(SLComposeViewControllerResult result) {
            switch(result)
            {
                case SLComposeViewControllerResultCancelled:
                {
                    NSLog(@"Facebook Sheet is cancelled");
                    Mixpanel *mixpanel = [Mixpanel sharedInstance];
                    [mixpanel track:@"Facebook Sharing Cancelled"];
                }
                    break;
                case SLComposeViewControllerResultDone:
                {
                    NSLog(@"Facebook Sheet is sent");
                    Mixpanel *mixpanel = [Mixpanel sharedInstance];
                    [mixpanel track:@"Shared On Facebook"];
                }
                    break;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:NO completion:^{
                    NSLog(@"Tweet Sheet has been dismissed.");
                }];
            });
        };
        
        [Facebook setInitialText:[NSString stringWithFormat:NSLocalizedString(@"Check out this iOS data recovery solution. It restores deleted data back to your iOS device! Download it from www.enigma-recovery.com ",@"")]];
        [self presentViewController:Facebook animated:YES completion:nil];
    }
    else
    {
        UIAlertView *showMessage = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Not Available",@"") message:NSLocalizedString(@"Facebook App can't be accessed",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
        [showMessage show];
    }
}

- (void)TwitterShareAction
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *Twitter = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        Twitter.completionHandler=^(SLComposeViewControllerResult result) {
            switch(result) {
                    //  This means the user cancelled without sending the Tweet
                case SLComposeViewControllerResultCancelled:
                {
                    NSLog(@"Tweet Sheet is cancelled");
                    Mixpanel *mixpanel = [Mixpanel sharedInstance];
                    [mixpanel track:@"Twitter Sharing Cancelled"];
                }
                    break;
                    //  This means the user hit 'Send'
                case SLComposeViewControllerResultDone:
                {
                    NSLog(@"Tweet Sheet is sent");
                    Mixpanel *mixpanel = [Mixpanel sharedInstance];
                    [mixpanel track:@"Shared On Twitter"];
                }
                    break;
            }
            
            //  dismiss the Tweet Sheet
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:NO completion:^{
                    NSLog(@"Tweet Sheet has been dismissed.");
                }];
            });
        };
        
        [Twitter setInitialText:[NSString stringWithFormat:NSLocalizedString(@"Check out this iOS data recovery solution. It restores deleted data back to your iOS device! Download it from www.enigma-recovery.com ",@"")]];
        [self presentViewController:Twitter animated:YES completion:nil];
    }
    else
    {
        UIAlertView *showMessage = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Not Available",@"") message:NSLocalizedString(@"Twitter can't be accessed, please install Twitter and Login",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
        [showMessage show];
    }
}

-(void) sendInAppSMS:(id) sender
{
	MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
	if([MFMessageComposeViewController canSendText])
	{
		controller.body = NSLocalizedString(@"Check out this iOS data recovery solution. It restores deleted data back to your iOS device! Download it from www.enigma-recovery.com ",@"");
		controller.messageComposeDelegate = self;
		[self presentViewController:controller animated:YES completion:nil];

	}
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
	switch (result)
    {
		case MessageComposeResultCancelled:
        {
			NSLog(@"SMS Cancelled");
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"SMS Sharing Cancelled"];
        }
			break;
		case MessageComposeResultFailed:
            //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Announcement" message: @"Unknown Error" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            //			[alert show];
            NSLog(@"Unknown Error");
			break;
		case MessageComposeResultSent:
        {
            NSLog(@"SMS sent");
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Shared Via SMS"];
        }
			break;
		default:
			break;
	}
    
	[self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - compose a mail

- (void)showEmail:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        
        // Email Subject
        NSString *emailTitle = NSLocalizedString(@"Restore App link ",@"");
        // Email Content
        NSString *messageBody = NSLocalizedString(@"<p>Check out this iOS data recovery solution. It restores data back to your iOS device! Download it at www.enigma-recovery.com/ios. Download the free Restore app from the <a href=\"https://itunes.apple.com/gb/app/restore/id910568421?mt=8)\">App Store</a></p>",@"");
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:@""];
    
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:YES];
        [mc setToRecipients:toRecipents];
    
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:nil];
    }
    else
    {
        UIAlertView *showMessage = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Not Available",@"") message:NSLocalizedString(@"Email has not been configured. Please setup an email account to send emails.",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil];
        [showMessage show];
    }

}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
        {
            NSLog(@"Mail cancelled");
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Email Sharing Cancelled"];
        }
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
        {
            NSLog(@"Mail sent");
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Shared Via Email"];
        }
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*-(void)logout
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Info"
                                                    message: @"Logout will remove all data from app. Would you like to proceed ?"
                                                   delegate: self
                                          cancelButtonTitle:@"YES"
                                          otherButtonTitles:@"NO",nil];
    [alert show];
}*/

/*
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0)
    {
        [self reset];
    }
    else
    {
        NSLog(@"user pressed Cancel");
    }
}
*/

/*-(void)reset
{
    AppDelegate *delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    delegate.window.rootViewController=delegate.loginViaCodeViewController;
    
    NSMutableArray *filesList=[[[defaults valueForKey:@"profiles"]
                                valueForKey:[defaults valueForKey:@"selectedDevice"]]
                               valueForKey:@"filesList"];
    if (filesList!=nil)
    {
        NSFileManager *fileManager=[NSFileManager defaultManager];
        NSError *error=nil;
        for (NSString* filepath in filesList)
        {
            BOOL isFileDeleted=[fileManager removeItemAtPath:filepath error:&error];
            if (isFileDeleted)
            {
                NSLog(@"%@ : file deleted",[filepath lastPathComponent]);
            }
            else
            {
               NSLog(@"%@ : file not deleted",[filepath lastPathComponent]);
            }
        }
    }
    
    [defaults removeObjectForKey:@"profiles"];
    //[defaults removePersistentDomainForName:@"com.infinity-wireless.Restor"];
    [defaults synchronize];
    
}*/
/*
-(void)addNewDevice
{
    AppDelegate *delegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    [self.navigationController pushViewController:delegate.loginViaCodeViewController animated:YES];
    NSLog(@"nav:%@", self.navigationController);
}*/

- (void)selectDevice
{
    NSArray *deviceList=[[JSONReader sharedReader] getDevicesNames];
    UIActionSheet *actionSheet = [[UIActionSheet alloc]init];
    actionSheet.tag=1;
    actionSheet.title=NSLocalizedString(@"Select Device",@"");
    actionSheet.delegate=self;    
    
    for (NSString* deviceName in deviceList)
    {
        [actionSheet addButtonWithTitle:deviceName];
    }
    
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel",@"")];
    actionSheet.cancelButtonIndex=actionSheet.numberOfButtons-1;
    //actionSheet.cancelButtonIndex=[actionSheet addButtonWithTitle:@"Cancel"];
    [actionSheet showInView:self.view];
}
@end
