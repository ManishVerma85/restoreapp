//
//  HomeDetailViewController.m
//  Restor
//
//  Created by Deva Palanisamy on 23/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import "GAIFields.h"
#import "HomeDetailViewController.h"

@interface NSString ( containsCategory )
- (BOOL) containsString: (NSString*) substring;
@end

// - - - -

@implementation NSString ( containsCategory )

- (BOOL) containsString: (NSString*) substring
{
    NSRange range = [self rangeOfString : substring options:NSCaseInsensitiveSearch];
    BOOL found = ( range.location != NSNotFound );
    return found;
}

@end


@interface HomeDetailViewController ()<EKEventViewDelegate>

@end

@implementation HomeDetailViewController

@synthesize array,nonFilterContent,filterContent;

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setArrayValues:) name:@"contentSelected" object:nil];
        
        //mv
        // Set screen name.
        self.screenName = @"HomeDetail View Screen";
    }
    
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self->tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [self->tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    UILabel *titleLabel=[UIViewController getTitleLabelWithFrame:self.view.frame];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.text=self.title;
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    self.navigationItem.titleView=titleLabel;
    tableView=[[UITableView alloc]initWithFrame:self.view.bounds];
    [self setTabelViewCellHeight];
    isSearching=NO;
    searchbar=[[UISearchBar alloc]initWithFrame:tableView.bounds];
    searchbar.delegate=self;
    tableView.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    [searchbar sizeToFit];
    tableView.delegate=self;
    tableView.dataSource=self;
    tableView.allowsMultipleSelectionDuringEditing=YES;
    [self.view addSubview:tableView];
    segmentedControl = [[UISegmentedControl alloc]initWithItems:[NSArray arrayWithObjects:NSLocalizedString(@"All",@""), NSLocalizedString(@"Missed",@""), nil]];
    
    //segmentedControl.segmentedControlStyle=UISegmentedControlStyleBar;
    
   // NSDictionary *attributes = [NSDictionary dictionaryWithObject:[UIFont fontWithName:@"Gotham-Book" size:13]
  //                                                         forKey:NSFontAttributeName];
  //  [segmentedControl setTitleTextAttributes:attributes
  //                                  forState:UIControlStateNormal];
    [segmentedControl sizeToFit];
    //segmentedControl.frame = CGRectMake(0, 0, 30, 30);
    [segmentedControl setWidth:60 forSegmentAtIndex:0];
    [segmentedControl setWidth:60 forSegmentAtIndex:1];
    [segmentedControl setSelectedSegmentIndex:0];
    segmentedControl.center=tableView.tableHeaderView.center;
    [segmentedControl addTarget:self action:@selector(filterCallHistory:) forControlEvents:UIControlEventValueChanged];
    
    segmentedControl.tintColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.translucent = NO;

    
    //  homeNavController.navigationBar.tintColor = [UIColor whiteColor];//[UIColor colorWithRed:(26.0f/255.0f) green:(103.0f/255.0f) blue:(159.0f/255.0f) alpha:1.0f]
    
    //  homeNavController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    
    
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    
    
    
    titles = @[NSLocalizedString(@"Messages",@""),
              NSLocalizedString(@"Contacts",@""),
              NSLocalizedString(@"Call History",@""),
              NSLocalizedString(@"Calendar",@""),
              NSLocalizedString(@"Chat History",@""),
              NSLocalizedString(@"Notes",@"")];
    
    /*UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(handleBack:)];
    backButton.tintColor = [UIColor whiteColor];
    
    self.navigationItem.leftBarButtonItem = backButton;
    */
    self.navigationController.navigationBar.topItem.title = NSLocalizedString(@"Back",@"");
    
    
}

-(void) dismissEvent:(id)sender
{
    [self.navigationController  dismissViewControllerAnimated:YES completion:nil];
}

- (void) handleBack:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    array=nil;
    filterContent=nil;
    nonFilterContent=nil;
    titles=nil;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    isSetEditMode=NO;
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    if (self.title!=nil)
    {
        [tracker set:kGAIScreenName value:self.title];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }

    
    
    if ([self.title isEqualToString:NSLocalizedString(@"Call History",@"")])
    {
        self.navigationItem.titleView=segmentedControl;
        nonFilterContent=array;
    }
    else
    {
        tableView.tableHeaderView=searchbar;
    }
    
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Edit",@"") style:UIBarButtonItemStyleDone target:self action:@selector(setEditMode)];
    
    rightButton.tintColor = [UIColor whiteColor];
    
    self.navigationItem.rightBarButtonItem=rightButton;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark- UITableViewDataSource Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [array count];
}





- (HomeDetailTableViewCell *)tableView:(UITableView *)tableview cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    HomeDetailTableViewCell *cell = [[HomeDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:MyIdentifier withType:self.title];

    //NSDictionary *dict=[array objectAtIndex:indexPath.row];
    
    
 /*   CGRect textLabelFrame = cell.detailTextLabel.frame;
    textLabelFrame.origin.x+=100;
    textLabelFrame.origin.y-=140;
    //textLabelFrame.size.width -= xOffset;
*/
    
    
    
    if ([self.title isEqualToString:NSLocalizedString(@"Messages",@"")])
    {
        Message *object = [array objectAtIndex:indexPath.row];
         cell.titleLabel.text = object.name;
     //   cell.titleLabel
         NSString* dateString=[(Messages *)[object.messages firstObject] date];
         cell.timeLabel.text=[self convertDateToRelativeString:dateString];
         cell.detailTextLabel.text=[(Messages *)[object.messages firstObject] message];
   //         cell.titleLabel.frame = textLabelFrame;
   //     cell.detailTextLabel.frame =textLabelFrame;
   //     cell.detailText.frame = textLabelFrame;
        
    }
    else if ([self.title isEqualToString:NSLocalizedString(@"Contacts",@"")])
    {
        Contact *object = [array objectAtIndex:indexPath.row];
        
        if (object.name.length==0)
        {
            cell.titleLabel.text= [object getUnknownContactName];
        }
        else
        {
            cell.titleLabel.text=object.name;
        }

    }
    else if ([self.title isEqualToString:NSLocalizedString(@"Chat History",@"")])
    {
        Whatsapp *object = [array objectAtIndex:indexPath.row];
        cell.titleLabel.text=object.name;
        NSString* dateString=[(Messages *)[object.messages firstObject] date];
        cell.timeLabel.text=[self convertDateToRelativeString:dateString];
        cell.detailTextLabel.text=[(Messages *)[object.messages firstObject] message];
    }
    else if ([self.title isEqualToString:NSLocalizedString(@"Call History",@"")])
    {
        CallHistory *object = [array objectAtIndex:indexPath.row];
        
        if ([object.name isEqualToString:@""])
        {
            cell.titleLabel.text=object.phone;
            cell.timeLabel.text=@"";
        }
        else
        {
            cell.titleLabel.text=object.name;
            cell.timeLabel.text=object.phone;
        }
        
        cell.timeLabel.text=[self convertDateToRelativeString:object.date];
        NSArray *dateArray =[object.date componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if (object.date.length > 0 && dateArray.count > 0)
        {
            NSString *time=[dateArray objectAtIndex:1];
            cell.detailTextLabel.text=[NSString stringWithFormat:@"%@ %@ %@",time,object.type,object.duration];
        }
       
        if ([object.duration isEqualToString:NSLocalizedString(@"Missed",@"")])
        {
            cell.titleLabel.textColor=[UIColor redColor];
        }
    }
    else if ([self.title isEqualToString:NSLocalizedString(@"Calendar",@"")])
    {
        self.navigationItem.hidesBackButton=NO;
        Calender *object = [array objectAtIndex:indexPath.row];
        cell.titleLabel.text=object.title;
        cell.detailTextLabel.text=object.location;
        
        if (object.startsDate.length > 0 && [object.startsDate componentsSeparatedByString:@" "].count > 0)
        {
            cell.startTimeLabel.text=[[object.startsDate componentsSeparatedByString:@" "] objectAtIndex:1];
        }

        if (object.endsDate.length > 0 && [object.endsDate componentsSeparatedByString:@" "].count > 0)
        {
            cell.endTimeLabel.text=[[object.endsDate componentsSeparatedByString:@" "] objectAtIndex:1];
        }
        
        cell.dateLabel.text=[[self convertStringToDate:object.startsDate] stringByLongDateFormat];
    }
    else if ([self.title isEqualToString:NSLocalizedString(@"Notes",@"")])
    {
        Notes *object = [array objectAtIndex:indexPath.row];
        cell.titleLabel.text=object.note;
        cell.timeLabel.text=[self convertDateToRelativeString:object.date];
        
    }
   
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    return cell;
}

#pragma mark- UITableViewDelegate Methods
- (void)tableView:(UITableView *)tableview didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.editing) {
        NSLog(@"Selected row is : %ld",(long)indexPath.row);
        [deleteIndexPaths addObject:indexPath];
        [deleteIndexes addIndex:indexPath.row];
        return;
    }
    
    [searchbar resignFirstResponder];
       // NSDictionary *dict1 = [array objectAtIndex:indexPath.row];
    NSUInteger index=[titles indexOfObject:self.title];
    [tableview deselectRowAtIndexPath:indexPath animated:YES];
    switch (index) {
        case 0:
                {
                    MessagesViewController *mvc=[[MessagesViewController alloc]init];
                    [mvc setMessagesDictionary:[array objectAtIndex:indexPath.row]];
                    mvc.title=[[array objectAtIndex:indexPath.row] valueForKey:@"name"];
                    [mvc setWhatsAppKey:NO];
                    [self.navigationController pushViewController:mvc animated:YES];
                    mvc=nil;
                }
                break;
        case 1:
                {
                    //CHANGE NAME ITS TOTALLY INCORRECT!
                    [self saveContact:[array objectAtIndex:indexPath.row]];
                    //[self addContact:nil];
                }
            break;
        case 2:
                {
                    //[self displayContact:dict1];
                    UIAlertView *calert=nil;
                    NSString *phNo = [[array objectAtIndex:indexPath.row] valueForKey:@"phone"];
                    NSString *cleanedString = [[phNo componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
                    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",cleanedString]];
                    
                    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
                        [[UIApplication sharedApplication] openURL:phoneUrl];
                    } else
                    {
                        calert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Alert",@"") message:NSLocalizedString(@"Call facility is not available.",@"displayed as an error messsage to the user") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok",@"") otherButtonTitles:nil, nil];
                        [calert show];
                    }
                }
            break;
        case 3:
                {
                    [self displayEvent:[array objectAtIndex:indexPath.row]];
                }
            break;
        case 4:
                {
                    MessagesViewController *mvc=[[MessagesViewController alloc]init];
                    [mvc setMessagesDictionary:[array objectAtIndex:indexPath.row]];
                    mvc.title=[[array objectAtIndex:indexPath.row] valueForKey:@"name"];
                    [mvc setWhatsAppKey:YES];
                    [self.navigationController pushViewController:mvc animated:YES];
                }
            break;
        case 5:
                {
                    NotesViewController *nvc=[[NotesViewController alloc]init];
                    [nvc setNotesDictionary:[array objectAtIndex:indexPath.row]];
                    [self.navigationController pushViewController:nvc animated:YES];
                }
            break;
        default:
            break;
    }
}

-(void) tableView:(UITableView *)tableViewLocal willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    /* if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row)
     {
     //Finished loading visible part of your table
     if (APPDELEGATE.didUserRequestForRemindLaterOption == NO)
     {
     //            [self showiRateMessage];
     }
     
     }*/
}
-(void)setTabelViewCellHeight
{
    if ([self.title isEqualToString:NSLocalizedString(@"Messages",@"")])
    {
        tableView.rowHeight=73;
    }
    else if ([self.title isEqualToString:NSLocalizedString(@"Chat History",@"")])
    {
        tableView.rowHeight=73;
    }
    else if ([self.title isEqualToString:NSLocalizedString(@"Notes",@"")])
    {
        tableView.rowHeight=50;
    }
    else if ([self.title isEqualToString:NSLocalizedString(@"Call History",@"")])
    {
        tableView.rowHeight=60;
    }
    else if ([self.title isEqualToString:NSLocalizedString(@"Contacts",@"")])
    {
        tableView.rowHeight=50;
    }
    else if ([self.title isEqualToString:NSLocalizedString(@"Calendar",@"")])
    {
        tableView.rowHeight=73;
    }
}

-(void)setArrayValues:(NSNotification*)notification{
    //NSString *contentName=notification.userInfo[@"contentName"];
    //RecoveredContent *recoveredContent=[RecoveredContent sharedInstance];
    //array=[recoveredContent.list valueForKey:contentName];
}

#pragma mark - Display contacts
-(void)saveContact:(NSDictionary*)dictionary{
    

    ABRecordRef person = ABPersonCreate();
    
    NSLog(@"%@",dictionary);
    NSString* venueName=[dictionary valueForKey:@"name"];
    NSString* venueOther=nil;
    NSString* venueHomeEmail=[dictionary valueForKey:@"homeEmail"];
    NSString* venueWorkEmail=[dictionary valueForKey:@"workEmail"];
    NSString* venueOtherEmail=[dictionary valueForKey:@"otherEmail"];
    NSString* venuePhone=[dictionary valueForKey:@"main"];
    NSString* venueMobile=[dictionary valueForKey:@"mobile"];
    
    
    if (venuePhone.length==0)
    {
         venuePhone=[dictionary valueForKey:@"work"];
    }
    
    
    if (venuePhone.length==0)
    {
        venuePhone=[dictionary valueForKey:@"home"];
    }
    
    if (venueMobile.length==0)
    {
        venueMobile=[dictionary valueForKey:@"other"];
    }
    
    
    NSString * unknownNumber;
    
    if (venuePhone.length==0)
    {
        unknownNumber = venueMobile;
        
    }
    else
    {
        unknownNumber=venuePhone;
    }
    
    
    if (venueName.length==0)
    {
        venueName= [[NSString alloc] initWithFormat:@"Unknown - %@",unknownNumber];
    }
    
    
    
    ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFStringRef) venueName, NULL);
    
    if (![venueOther isEqualToString:@""] )
    {
        ABMutableMultiValueRef urlMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueAddValueAndLabel(urlMultiValue, (__bridge CFStringRef) venueOther, kABOtherLabel, NULL);
        ABRecordSetValue(person, kABPersonURLProperty, urlMultiValue, nil);
        CFRelease(urlMultiValue);
    }
    
    if (![venueHomeEmail isEqualToString:@""] || ![venueWorkEmail isEqualToString:@""] || ![venueOtherEmail isEqualToString:@""])
    {
        ABMutableMultiValueRef emailMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        
        if (![venueHomeEmail isEqualToString:@""] )ABMultiValueAddValueAndLabel(emailMultiValue, (__bridge CFStringRef) venueHomeEmail, kABPersonHomePageLabel, NULL);
        if (![venueWorkEmail isEqualToString:@""] )ABMultiValueAddValueAndLabel(emailMultiValue, (__bridge CFStringRef) venueWorkEmail, kABWorkLabel, NULL);
        if (![venueOtherEmail isEqualToString:@""] )ABMultiValueAddValueAndLabel(emailMultiValue, (__bridge CFStringRef) venueOtherEmail, kABOtherLabel, NULL);
        
        
        ABRecordSetValue(person, kABPersonEmailProperty, emailMultiValue, nil);
        CFRelease(emailMultiValue);
    }
    
    if (![venuePhone isEqualToString:@""] || ![venueMobile isEqualToString:@""])
    {
        ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        
         if (![venuePhone isEqualToString:@""])
         {
        NSArray *venuePhoneNumbers = [venuePhone componentsSeparatedByString:@" or "];
        for (NSString *venuePhoneNumberString in venuePhoneNumbers)
            ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFStringRef) venuePhoneNumberString, kABPersonPhoneMainLabel, NULL);
         }
        if (![venueMobile isEqualToString:@""])
        {
        NSArray *venueMobileNumbers = [venueMobile componentsSeparatedByString:@" or "];
        for (NSString *venueMobileNumberString in venueMobileNumbers)
            ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFStringRef) venueMobileNumberString, kABPersonPhoneMobileLabel, NULL);
        }
        ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil);
        CFRelease(phoneNumberMultiValue);
    }
    
    
    //ABMutableMultiValueRef multiAddress = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
    //NSMutableDictionary *addressDictionary = [[NSMutableDictionary alloc] init];
    
    //ABMultiValueAddValueAndLabel(multiAddress, (__bridge CFDictionaryRef) addressDictionary, kABWorkLabel, NULL);
    //ABRecordSetValue(person, kABPersonAddressProperty, multiAddress, NULL);
    //CFRelease(multiAddress);
    
    ABUnknownPersonViewController *controller = [[ABUnknownPersonViewController alloc] init];
    
    controller.displayedPerson = person;
    controller.allowsAddingToAddressBook = YES;
    UILabel *titleLabel=[UIViewController getTitleLabelWithFrame:self.view.frame];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.text=venueName;
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    controller.navigationItem.titleView=titleLabel;
    
    [self.navigationController pushViewController:controller animated:YES];

    CFRelease(person);
}

#pragma mark - Display Event
-(void)displayEvent:(NSDictionary*)dictionary{
    eventStore = [[EKEventStore alloc] init];
    if([eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)]) {
        [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            [self performSelectorOnMainThread:@selector(displayCalenderView:) withObject:dictionary waitUntilDone:YES];
        }];
    }

}

-(void)displayCalenderView:(NSDictionary*)dictionary
{
    EKEvent * event = [EKEvent eventWithEventStore:eventStore];
    event.title     = [dictionary valueForKey:@"title"];
    event.location  = [dictionary valueForKey:@"location"];
    event.startDate = [self convertStringToDate:[dictionary valueForKey:@"startsDate"]];
    event.endDate   = [self convertStringToDate:[dictionary valueForKey:@"endsDate"]];
    event.notes     = [dictionary valueForKey:@"note"];

    evc=[[EKEventViewController alloc]init];
    evc.event=event;
    UILabel *titleLabel=[UIViewController getTitleLabelWithFrame:self.view.frame];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.text=NSLocalizedString(@"Event Details",@"");
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    evc.navigationItem.titleView=titleLabel;
    evc.navigationItem.hidesBackButton = NO;
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back",@"")
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(dismissEvent:)];
    
    backButton.tintColor = [UIColor whiteColor];
    
    evc.navigationItem.leftBarButtonItem = backButton;
    
    
    evc.delegate = self;
    
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:evc];

    UIColor *backGroundColor = [UIColor colorWithRed:54.0f/255.0f
                                               green:62.0f/255.0f
                                                blue:73.0f/255.0f
                                               alpha:1.0f];
    
    
     nav.navigationBar.barTintColor = backGroundColor;
    
     nav.navigationBar.topItem.title=NSLocalizedString(@"Back",@"");
     [nav.navigationBar setTintColor:[UIColor whiteColor]];
    
 /*   UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(handleBack:)];
    
    nav.navigationItem.leftBarButtonItem = backButton;
  //  nav.navigationItem.l
  //  nav.navigationItem.titleView = backButton;*/
    [self presentViewController:nav animated:YES completion:nil];
    //[self.navigationController pushViewController:evc animated:YES];
}

-(void)eventViewController:(EKEventViewController *)controller didCompleteWithAction:(EKEventViewAction)action
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)eventEditViewController:(EKEventEditViewController *)controller
         didCompleteWithAction:(EKEventEditViewAction)action {
    
    switch (action) {
        case EKEventEditViewActionCanceled:
            break;
        case EKEventEditViewActionSaved:
            break;
        case EKEventEditViewActionDeleted:
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSDate*)convertStringToDate:(NSString*)datestring{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM-dd-yyyy HH:mm"];
    
    NSDate *date = [dateFormat dateFromString:datestring];
    NSDateFormatter *newDateFormatter = [[NSDateFormatter alloc]init];
    [newDateFormatter setDateFormat:@"MM/dd/yyyy"];
    return date;
}

-(NSString*)convertDateToRelativeString:(NSString*)datestring{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM-dd-yyyy HH:mm"];
    
    NSDate *date = [dateFormat dateFromString:datestring];
    NSDateFormatter *newDateFormatter = [[NSDateFormatter alloc]init];
    [newDateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString *timeAgo=[date formattedAsTimeAgo];
    return timeAgo;
}
#pragma mark - Display call history
-(void)displayContact:(NSDictionary*)dictionary{
    
    //mv
//    Contact *objContact = (Contact *)dictionary;
    Contact *objContact = [Contact modelObjectWithDictionary:dictionary];
    
    ABRecordRef person = ABPersonCreate();

    NSLog(@"%@",dictionary);
    NSString* venueName=objContact.name;
    NSString* venueOther=nil;
    NSString* venueHomeEmail=objContact.homeEmail;
    NSString* venueWorkEmail=objContact.workEmail;
    NSString* venueOtherEmail=objContact.otherEmail;
    NSString* venuePhone=objContact.main;
    NSString* venueMobile=objContact.mobile;
   
   /* NSString* venueName=[dictionary valueForKey:@"name"];
    NSString* venueOther=nil;
    NSString* venueHomeEmail=[dictionary valueForKey:@"homeEmail"];
    NSString* venueWorkEmail=[dictionary valueForKey:@"workEmail"];
    NSString* venueOtherEmail=[dictionary valueForKey:@"otherEmail"];
    NSString* venuePhone=[dictionary valueForKey:@"main"];
    NSString* venueMobile=[dictionary valueForKey:@"mobile"];
    */
    
    [dictionary valueForKey:@"phone"];
    
    if (venuePhone.length==0)
    {
        venuePhone= [dictionary valueForKey:@"phone"];//  [dictionary valueForKey:@"work"];
    }
    
    if (venuePhone.length==0)
    {
        venuePhone=objContact.work;//  [dictionary valueForKey:@"work"];
    }
    
    if (venuePhone.length==0)
    {
        venuePhone=objContact.home;// [dictionary valueForKey:@"home"];
    }
    
    if (venueMobile.length==0)
    {
        venueMobile=objContact.other;//[dictionary valueForKey:@"other"];
    }
    
    NSString * unknownNumber;
    
    if (venuePhone.length==0)
    {
        unknownNumber = venueMobile;
        
    }
    else
    {
        unknownNumber=venuePhone;
    }
    
    if (venueName.length==0)
    {
        NSString * unknown = NSLocalizedString(@"Unknown - ",@"a number is unknown the number is on the other side of the dash");
        venueName= [[NSString alloc] initWithFormat:@"%@%@",unknown,unknownNumber];
    }
    
    ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFStringRef) venueName, NULL);
    
    if (![venueOther isEqualToString:@""] )
    {
        ABMutableMultiValueRef urlMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueAddValueAndLabel(urlMultiValue, (__bridge CFStringRef) venueOther, kABOtherLabel, NULL);
        ABRecordSetValue(person, kABPersonURLProperty, urlMultiValue, nil);
        CFRelease(urlMultiValue);
    }
    
    if (![venueHomeEmail isEqualToString:@""] || ![venueWorkEmail isEqualToString:@""] || ![venueOtherEmail isEqualToString:@""])
    {
        ABMutableMultiValueRef emailMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        
        if (![venueHomeEmail isEqualToString:@""] )ABMultiValueAddValueAndLabel(emailMultiValue, (__bridge CFStringRef) venueHomeEmail, kABPersonHomePageLabel, NULL);
        if (![venueWorkEmail isEqualToString:@""] )ABMultiValueAddValueAndLabel(emailMultiValue, (__bridge CFStringRef) venueWorkEmail, kABWorkLabel, NULL);
        if (![venueOtherEmail isEqualToString:@""] )ABMultiValueAddValueAndLabel(emailMultiValue, (__bridge CFStringRef) venueOtherEmail, kABOtherLabel, NULL);
        
        
        ABRecordSetValue(person, kABPersonEmailProperty, emailMultiValue, nil);
        CFRelease(emailMultiValue);
    }
    
    if (![venuePhone isEqualToString:@""] || ![venueMobile isEqualToString:@""])
    {
        ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        
        if (![venuePhone isEqualToString:@""])
        {
            NSArray *venuePhoneNumbers = [venuePhone componentsSeparatedByString:@" or "];
            for (NSString *venuePhoneNumberString in venuePhoneNumbers)
                ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFStringRef) venuePhoneNumberString, kABPersonPhoneMainLabel, NULL);
        }
        if (![venueMobile isEqualToString:@""])
        {
            NSArray *venueMobileNumbers = [venueMobile componentsSeparatedByString:@" or "];
            for (NSString *venueMobileNumberString in venueMobileNumbers)
                ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFStringRef) venueMobileNumberString, kABPersonPhoneMobileLabel, NULL);
        }
        ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil);
        CFRelease(phoneNumberMultiValue);
    }
    
    ABMutableMultiValueRef multiAddress = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
    NSMutableDictionary *addressDictionary = [[NSMutableDictionary alloc] init];
    
    ABMultiValueAddValueAndLabel(multiAddress, (__bridge CFDictionaryRef) addressDictionary, kABWorkLabel, NULL);
    ABRecordSetValue(person, kABPersonAddressProperty, multiAddress, NULL);
    CFRelease(multiAddress);
    
    ABUnknownPersonViewController *controller = [[ABUnknownPersonViewController alloc] init];
    
    controller.displayedPerson = person;
    controller.allowsAddingToAddressBook = YES;
    
    UILabel *titleLabel=[UIViewController getTitleLabelWithFrame:self.view.frame];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.text=venueName;
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    controller.navigationItem.titleView=titleLabel;
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController pushViewController:controller animated:YES];
    
    CFRelease(person);
}

-(void)filterCallHistory:(id)sender{
    
    if (segmentedControl.selectedSegmentIndex==1)
    {
        //mv below is the kind of data that can be taken as 'Missed' call
        /*
             {
             "name": "irim ",
             "phone": "0784621&�+\u000f",
             "date": "04-02-2015 11:59",
             "type": "Incoming",
             "duration": "Cancelled"
         },
         */
//        NSString* namePredicateString=[NSString stringWithFormat:@"type CONTAINS[cd] 'Incoming' AND duration CONTAINS[cd] 'Cancelled'"];
        NSString* namePredicateString=[NSString stringWithFormat:@"duration CONTAINS[cd] 'Cancelled'"];
        namePredicate=[NSPredicate predicateWithFormat:namePredicateString];
        filterContent=[nonFilterContent filteredArrayUsingPredicate:namePredicate];
        array=[filterContent mutableCopy];
        [tableView reloadData];
    }else{
        array=[nonFilterContent mutableCopy];
        [tableView reloadData];
    }

}

#pragma mark - Show Contact Details in call history using tableview accessoryButton delegate
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
     NSDictionary *dict1 = [array objectAtIndex:indexPath.row];
 //   [self saveContact:dict1];
    [self displayContact:dict1];
}

#pragma mark - Search Implementation
-(void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    if ([self.title isEqualToString:NSLocalizedString(@"Contacts",@"")])
    {
        self.navigationItem.rightBarButtonItem=nil;
    }
    if (!isSearching)
    {
        isSearching=YES;
        nonFilterContent=array;
        [searchBar setShowsCancelButton:YES animated:YES];
        [searchbar becomeFirstResponder];
    }
    else
    {
        
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    if ([self.title isEqualToString:NSLocalizedString(@"Contacts",@"")])
    {
        UIBarButtonItem * rightBarButtonItem =[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save All",@"") style:UIBarButtonItemStyleDone target:self action:@selector(getPermissionToAddContacts)];
        
        rightBarButtonItem.tintColor = [UIColor whiteColor];
        self.navigationItem.rightBarButtonItem=rightBarButtonItem;
    }
    
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    searchbar.text=@"";
    isSearching=NO;
    array=[nonFilterContent mutableCopy];
    filterContent=nil;
    [tableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
   // NSPredicate *localPred;
    NSLog(@"You are typing : %@",filterContent);
    NSUInteger index=[titles indexOfObject:self.title];
    
    
    if (searchText.length==0)
    {
        searchbar.text=@"";
        array=[nonFilterContent mutableCopy];
        filterContent=nil;
        [tableView reloadData];
        
        return;
    }
     NSString * predString = [self parseInputStringforPredicate:searchText];
    
    
    switch (index)
    {
        case 0://Messages
        {
            NSLog(@"%@",predString);

            NSMutableArray *arrMute = [NSMutableArray arrayWithArray:nonFilterContent];
            
            [arrMute removeObjectsAtIndexes:[self modifyArrayForFiltering:nonFilterContent withSearchText:searchText]];

            array=[arrMute mutableCopy];
            [tableView reloadData];
        }
            break;
        case 1:// Contacts
        {
            //NSString* namePredicateString=[NSString stringWithFormat:@"name contains[c] '%@'",predString];
            namePredicate=[NSPredicate predicateWithFormat:@"name contains[c] %@ OR name beginswith[c] %@",predString,predString];
            filterContent=[nonFilterContent filteredArrayUsingPredicate:namePredicate];
            array=[filterContent mutableCopy];
            [tableView reloadData];
        }
            break;
        case 2://Call History
        {

        }
            break;
        case 3://Calendar
        {
            
            namePredicate=[NSPredicate predicateWithFormat:@"title contains[c] %@",predString];
            filterContent=[nonFilterContent filteredArrayUsingPredicate:namePredicate];
            array = [filterContent mutableCopy];
            
            [tableView reloadData];
        }
            break;
        case 4://Chat history / WhatsApp
        {
           
            namePredicate=[NSPredicate predicateWithFormat:@"message contains[c] %@",predString];
            
            NSLog(@"%@",namePredicate);

            NSMutableArray * filterIt = [[NSMutableArray alloc] init];
            
            for (Whatsapp * tempWhats in nonFilterContent)
            {
                
                NSArray *result =[tempWhats.messages filteredArrayUsingPredicate:namePredicate];
                
                if([result count] > 0)
                {
                    [filterIt addObject:tempWhats];
                }
            }
            
 
            array = [filterIt mutableCopy];
            
            
            [tableView reloadData];
        }
            break;
        case 5:
        {
            NSString* namePredicateString=[NSString stringWithFormat:@"note contains[c] '%@'",predString];
            namePredicate=[NSPredicate predicateWithFormat:namePredicateString];
            filterContent=[nonFilterContent filteredArrayUsingPredicate:namePredicate];
            array=[filterContent mutableCopy];
            
            [tableView reloadData];
        }
            break;
        default:
            break;
    }
}


-(NSString *)parseInputStringforPredicate:(NSString *) inputString
{
    NSString * parsedStr;
    
    if (inputString.length>52)
    {
        parsedStr=[inputString substringToIndex:52];
    }
    else
    {
        parsedStr = inputString;
    }
    
     parsedStr = [parsedStr stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    parsedStr = [parsedStr stringByReplacingOccurrencesOfString:@":" withString:@""];
    parsedStr = [parsedStr stringByReplacingOccurrencesOfString:@";" withString:@""];
    parsedStr = [parsedStr stringByReplacingOccurrencesOfString:@"/" withString:@""];
    parsedStr = [parsedStr stringByReplacingOccurrencesOfString:@")" withString:@""];
    parsedStr = [parsedStr stringByReplacingOccurrencesOfString:@"(" withString:@""];
    parsedStr = [parsedStr stringByReplacingOccurrencesOfString:@"£" withString:@""];
    parsedStr = [parsedStr stringByReplacingOccurrencesOfString:@"&" withString:@""];
  //  parsedStr = [parsedStr stringByReplacingOccurrencesOfString:@"." withString:@""];
    
  //  parsedStr = [parsedStr stringByReplacingOccurrencesOfString:@"," withString:@""];
    parsedStr = [parsedStr stringByReplacingOccurrencesOfString:@"?" withString:@""];

    parsedStr = [parsedStr stringByReplacingOccurrencesOfString:@"!" withString:@"?"];
    parsedStr = [parsedStr stringByReplacingOccurrencesOfString:@"\"" withString:@" "];
    
    parsedStr = [parsedStr stringByReplacingOccurrencesOfString:@"@" withString:@""];
    
    parsedStr = [parsedStr stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    
    parsedStr = [parsedStr stringByReplacingOccurrencesOfString:@"'" withString:@"?"];
    parsedStr = [parsedStr stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    parsedStr = [parsedStr stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    
    return parsedStr;
}


//mv
#pragma mark- Modify Array
- (NSIndexSet*)modifyArrayForFiltering:(NSArray*)sourceArray withSearchText:searchText
{
    /*
    {
        messages =     (
                        {
                            date = "10-01-2013 11:32";
                            message = "02/10/2013 11:00 for Zaks 14 Day Manicure with Charl Beauty and Nail Extension Soak Off with Charl Beauty";
                            type = sent;
                        }
                        );
        name = zakshair;
        phone = Unknown;
    }
    */
    
//    NSMutableArray *temp = [NSMutableArray new];
    
    NSMutableIndexSet *indexes = [NSMutableIndexSet indexSet];
    
    for (Message *messageObj in sourceArray)
    {
        Messages *innerMsg =  [messageObj.messages firstObject];
        
        if (!([messageObj.name containsString:searchText] || [messageObj.phone containsString:searchText] || [innerMsg.message containsString:searchText]))
        {
//            [temp addObject:[NSMutableIndexSet indexSetWithIndex:[sourceArray indexOfObject:messageObj]]];
            
            [indexes addIndex:[sourceArray indexOfObject:messageObj]];
        }
    }
    
    return indexes;
//    return [NSArray arrayWithArray:temp];
}


#pragma mark - To all contacts
- (IBAction)addContact:(NSDictionary*)dictionary
{
    ABAddressBookRef addressBook = NULL;
    CFErrorRef error = NULL;
    switch (ABAddressBookGetAuthorizationStatus()) {
        case kABAuthorizationStatusAuthorized: {
            addressBook = ABAddressBookCreateWithOptions(NULL, &error);
            
            [self addAccount:dictionary inAddressBook:addressBook];
            
            if (addressBook != NULL) CFRelease(addressBook);
            break;
        }
        case kABAuthorizationStatusDenied: {
            NSLog(@"Access denied to address book");
            break;
        }
        case kABAuthorizationStatusNotDetermined: {
            addressBook = ABAddressBookCreateWithOptions(NULL, &error);
            ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
                if (granted) {
                    NSLog(@"Access was granted");
                    [self addAccount:dictionary inAddressBook:addressBook];
                }
                else NSLog(@"Access was not granted");
                if (addressBook != NULL) CFRelease(addressBook);
            });
            break;
        }
        case kABAuthorizationStatusRestricted: {
            NSLog(@"access restricted to address book");
            break;
        }
    }
}

- (ABRecordRef)addAccount:(NSDictionary*)dictionary inAddressBook:(ABAddressBookRef)addressBook
{

    CFErrorRef error = NULL;
    
    // create person record
    
    ABRecordRef result = ABPersonCreate();
    
    // set name and other string values
    
    NSLog(@"%@",dictionary);
    NSString* venueName=[dictionary valueForKey:@"name"];
    NSString* venueOther=nil;
    NSString* venueHomeEmail=[dictionary valueForKey:@"homeEmail"];
    NSString* venueWorkEmail=[dictionary valueForKey:@"workEmail"];
    NSString* venueOtherEmail=[dictionary valueForKey:@"otherEmail"];
    NSString* venuePhone=[dictionary valueForKey:@"main"];
    NSString* venueMobile=[dictionary valueForKey:@"mobile"];
    
    
    ABRecordSetValue(result, kABPersonFirstNameProperty, (__bridge CFStringRef) venueName, NULL);
    
    if (![venueOther isEqualToString:@""] )
    {
        ABMutableMultiValueRef urlMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueAddValueAndLabel(urlMultiValue, (__bridge CFStringRef) venueOther, kABOtherLabel, NULL);
        ABRecordSetValue(result, kABPersonURLProperty, urlMultiValue, nil);
        CFRelease(urlMultiValue);
    }
    
    if (![venueHomeEmail isEqualToString:@""] || ![venueWorkEmail isEqualToString:@""] || ![venueOtherEmail isEqualToString:@""])
    {
        ABMutableMultiValueRef emailMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        
        if (![venueHomeEmail isEqualToString:@""] )ABMultiValueAddValueAndLabel(emailMultiValue, (__bridge CFStringRef) venueHomeEmail, kABPersonHomePageLabel, NULL);
        if (![venueWorkEmail isEqualToString:@""] )ABMultiValueAddValueAndLabel(emailMultiValue, (__bridge CFStringRef) venueWorkEmail, kABWorkLabel, NULL);
        if (![venueOtherEmail isEqualToString:@""] )ABMultiValueAddValueAndLabel(emailMultiValue, (__bridge CFStringRef) venueOtherEmail, kABOtherLabel, NULL);
        
        
        ABRecordSetValue(result, kABPersonEmailProperty, emailMultiValue, nil);
        CFRelease(emailMultiValue);
    }
    
    if (![venuePhone isEqualToString:@""] || ![venueMobile isEqualToString:@""])
    {
        ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        
        if (![venuePhone isEqualToString:@""])
        {
            NSArray *venuePhoneNumbers = [venuePhone componentsSeparatedByString:@" or "];
            for (NSString *venuePhoneNumberString in venuePhoneNumbers)
                ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFStringRef) venuePhoneNumberString, kABPersonPhoneMainLabel, NULL);
        }
        if (![venueMobile isEqualToString:@""])
        {
            NSArray *venueMobileNumbers = [venueMobile componentsSeparatedByString:@" or "];
            for (NSString *venueMobileNumberString in venueMobileNumbers)
                ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFStringRef) venueMobileNumberString, kABPersonPhoneMobileLabel, NULL);
        }
        ABRecordSetValue(result, kABPersonPhoneProperty, phoneNumberMultiValue, nil);
        CFRelease(phoneNumberMultiValue);
    }
    
    // add address
    
    ABMutableMultiValueRef multiAddress = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
    NSMutableDictionary *addressDictionary = [[NSMutableDictionary alloc] init];
    
    ABMultiValueAddValueAndLabel(multiAddress, (__bridge CFDictionaryRef) addressDictionary, kABWorkLabel, NULL);
    ABRecordSetValue(result, kABPersonAddressProperty, multiAddress, NULL);
    CFRelease(multiAddress);

    
    //3
    BOOL couldAddPerson = ABAddressBookAddRecord(addressBook, result, &error);
    
    if (couldAddPerson) {
        NSLog(@"Successfully added the person.");
    } else {
        NSLog(@"Failed to add the person.");
        CFRelease(result);
        result = NULL;
        return result;
    }
    
    //4
    if (ABAddressBookHasUnsavedChanges(addressBook)) {
        BOOL couldSaveAddressBook = ABAddressBookSave(addressBook, &error);
        
        if (couldSaveAddressBook) {
            NSLog(@"Succesfully saved the address book.");
        } else {
            NSLog(@"Failed.");
        }
    }
    
    return result;
}

-(void)getPermissionToAddContacts
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Save All Contacts",@"")
                                                    message: NSLocalizedString(@"Would you like to add all recovered contacts to your iPhone Contacts?",@"")
                                                   delegate: self
                                          cancelButtonTitle:NSLocalizedString(@"Cancel",@"")
                                          otherButtonTitles:NSLocalizedString(@"OK",@""),nil];
    [alert show];
}

-(void)saveAllContactsToPhone{
    for (NSDictionary *contact in array) {
        [self addContact:contact];
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Contacts saved",@"")
                                                    message: NSLocalizedString(@"All recovered contacts are in your iPhone Contacts now.",@"")
                                                   delegate: nil
                                          cancelButtonTitle: NSLocalizedString(@"Ok",@"")
                                          otherButtonTitles:nil];
    [alert show];
}

#pragma mark - Implement Alert view delegates
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 1) {
		NSLog(@"user pressed OK");
        [self saveAllContactsToPhone];
	}
	else {
		NSLog(@"user pressed Cancel");
	}
}

#pragma mark - Set Table view to Edit mode
- (void)setEditMode
{
    if ([self.title isEqualToString:NSLocalizedString(@"Contacts",@"")] && !isSetEditMode)
    {
        [self displayActionSheet:nil];
    }
    else
    {
        [tableView setEditing:YES animated:YES];
        tableView.allowsMultipleSelectionDuringEditing=YES;
    
        
        
        self.navigationItem.hidesBackButton = YES;
        
        UIBarButtonItem * rightBarButtonItem =[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel",@"") style:UIBarButtonItemStyleDone target:self action:@selector(restoreNormalMode)];
        rightBarButtonItem.tintColor = [UIColor whiteColor];
        self.navigationItem.rightBarButtonItem= rightBarButtonItem;
        
        
        UIBarButtonItem * leftBarButtonItem =[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Delete",@"") style:UIBarButtonItemStyleDone target:self action:@selector(deleteMultipleRows)];
        leftBarButtonItem.tintColor = [UIColor whiteColor];
        self.navigationItem.leftBarButtonItem=leftBarButtonItem;
        
        deleteIndexPaths=[[NSMutableArray alloc]init];
        deleteIndexes=[[NSMutableIndexSet alloc]init];
    }

}

- (void)restoreNormalMode
{
    [tableView setEditing:NO animated:YES];
    self.navigationItem.hidesBackButton = NO;
    deleteIndexPaths=nil;
    deleteIndexes=nil;
    self.navigationItem.leftBarButtonItem=nil;
    self.navigationItem.leftBarButtonItem.enabled=NO;
    self.navigationItem.hidesBackButton = NO;
    
      UIBarButtonItem * rightBarButtonItem =[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Edit",@"") style:UIBarButtonItemStyleDone target:self action:@selector(setEditMode)];
          rightBarButtonItem.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=rightBarButtonItem;
    isSetEditMode=NO;
}

- (void)tableView:(UITableView *)tableView1 commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"commitEditingStyle delegate");
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [tableView beginUpdates];
        if ([self.title isEqualToString:NSLocalizedString(@"Messages",@"")])
        {
            [[RestoreManager sharedManager].currentDevice deleteMessageAtIndex:indexPath.row];
        }
        else if ([self.title isEqualToString:NSLocalizedString(@"Contacts",@"")])
        {
            [[RestoreManager sharedManager].currentDevice deleteContactAtIndex:indexPath.row];
        }
        else if ([self.title isEqualToString:NSLocalizedString(@"Chat History",@"")])
        {
            [[RestoreManager sharedManager].currentDevice deleteWhatsappAtIndex:indexPath.row];
        }
        else if ([self.title isEqualToString:NSLocalizedString(@"Call History",@"")])
        {
            [[RestoreManager sharedManager].currentDevice deleteCallHistoryAtIndex:indexPath.row];
        }
        else if ([self.title isEqualToString:NSLocalizedString(@"Calendar",@"")])
        {
            [[RestoreManager sharedManager].currentDevice deleteCalenderAtIndex:indexPath.row];
        }
        else if ([self.title isEqualToString:NSLocalizedString(@"Notes",@"")])
        {
            [[RestoreManager sharedManager].currentDevice deleteNoteAtIndex:indexPath.row];
        }
        
        [array removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [tableView endUpdates];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"RefreshHomeView"
         object:self
         userInfo:nil];
    }
}
- (void)tableView:(UITableView *)tableview didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableview.editing) {
        [deleteIndexPaths removeObject:indexPath];
        [deleteIndexes removeIndex:indexPath.row];
        NSLog(@"Deselected row is : %ld",(long)indexPath.row);
        return;
    }
}
-(void)deleteMultipleRows{

    [tableView beginUpdates];
    if ([self.title isEqualToString:NSLocalizedString(@"Messages",@"")])
    {
        [[RestoreManager sharedManager].currentDevice deleteMessagesAtIndexs:deleteIndexPaths];
    }
    else if ([self.title isEqualToString:NSLocalizedString(@"Contacts",@"")])
    {
        [[RestoreManager sharedManager].currentDevice deleteContactsAtIndexs:deleteIndexPaths];
    }
    else if ([self.title isEqualToString:NSLocalizedString(@"Chat History",@"")])
    {
        [[RestoreManager sharedManager].currentDevice deleteWhatsappsAtIndexs:deleteIndexPaths];
    }
    else if ([self.title isEqualToString:NSLocalizedString(@"Call History",@"")])
    {
        [[RestoreManager sharedManager].currentDevice deleteCallHistorysAtIndexs:deleteIndexPaths];
    }
    else if ([self.title isEqualToString:NSLocalizedString(@"Calendar",@"")])
    {
        [[RestoreManager sharedManager].currentDevice deleteCalendersAtIndexs:deleteIndexPaths];
    }
    else if ([self.title isEqualToString:NSLocalizedString(@"Notes",@"")])
    {
        [[RestoreManager sharedManager].currentDevice deleteNotesAtIndexs:deleteIndexPaths];
    }
    
    [array removeObjectsAtIndexes:deleteIndexes];
    [tableView deleteRowsAtIndexPaths:deleteIndexPaths withRowAnimation:UITableViewRowAnimationFade];
    [tableView endUpdates];
    deleteIndexPaths=nil;
    deleteIndexes=nil;
    self.navigationItem.leftBarButtonItem=nil;
    self.navigationItem.leftBarButtonItem.enabled=NO;
    self.navigationItem.hidesBackButton = NO;
    [self restoreNormalMode];
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"RefreshHomeView"
     object:self
     userInfo:nil];
}


#pragma mark - Adding delete or save option in contacts
- (void)displayActionSheet:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Would you like to ..",@"dialog window with opsions to save and delete contacts") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Save All Contacts",@""), NSLocalizedString(@"Delete Contacts",@""), nil];
    
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault; [actionSheet showInView:self.view];
    isSetEditMode=YES;
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:{
                    [self getPermissionToAddContacts];
                    isSetEditMode=NO;
                    break;
                }
        case 1:
                {
                    [self setEditMode];
                    break;
                }
        case 2:
            isSetEditMode=NO;
            break;
    }
}
- (void)actionSheetCancel:(UIActionSheet *)actionSheet
{
    isSetEditMode=NO;
}
@end
