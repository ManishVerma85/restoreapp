//
//  UIViewController+Helper.m
//  Restor
//
//  Created by Deva Palanisamy on 30/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import "UIViewController+Helper.h"

@implementation UIViewController (Helper)
+(UILabel*)getTitleLabelWithFrame:(CGRect)frame{
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:frame];
    titleLabel.font=[UIFont boldSystemFontOfSize:18.0f];//[UIFont fontWithName:@"Gotham-Book" size:18];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    return titleLabel;
}
@end
