//
//  UserDefaultManager.h
//  Restor
//
//  Created by Deva Palanisamy on 27/07/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDefaultManager : NSObject{
    NSUserDefaults *defaults;
}
-(void)saveTokenToUserDefaults:(NSDictionary*)dictionary;
-(void)saveToken:(NSDictionary *)dictionary;
-(void)resetBadgeForContent:(NSString*)badgeName;
@end
