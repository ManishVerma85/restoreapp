//
//  UserDefaultManager.m
//  Restor
//
//  Created by Deva Palanisamy on 27/07/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import "UserDefaultManager.h"

@implementation UserDefaultManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        defaults=[NSUserDefaults standardUserDefaults];
    }
    return self;
}
-(void)saveTokenToUserDefaults:(NSDictionary*)dictionary{
    NSMutableDictionary *deviceProfile=[[NSMutableDictionary alloc]init];
    
    [deviceProfile setObject:[[dictionary valueForKey:@"oauth2AccessToken"]valueForKey:@"access_token"]
                      forKey:@"accessToken"];
    [deviceProfile setObject:[[dictionary valueForKey:@"oauth2AccessToken"]valueForKey:@"refresh_token"]
                      forKey:@"refreshToken"];
    [deviceProfile setObject:[[dictionary valueForKey:@"oauth2AccessToken"]valueForKey:@"token_type"]
                      forKey:@"tokenType"];
    NSTimeInterval timeInterval=[[[dictionary valueForKey:@"oauth2AccessToken"]valueForKey:@"expires_in"] doubleValue];
    NSDate *date=[NSDate date];
    date=[date dateByAddingTimeInterval:timeInterval];
    [deviceProfile setObject:date
                      forKey:@"expiryTime"];
    
    
    
    [deviceProfile setObject:[[dictionary valueForKey:@"apiUser"]valueForKey:@"id"]
                      forKey:@"userID"];
    [deviceProfile setObject:[[dictionary valueForKey:@"apiUser"]valueForKey:@"emailAddress"]
                      forKey:@"emailAddress"];
    
    NSMutableDictionary *profiles=[[defaults valueForKey:@"profiles"]mutableCopy];
    if (profiles==nil)
    {
        profiles=[[NSMutableDictionary alloc]init];
    }
    NSString* profileName=[NSString stringWithFormat:@"Phone %lu",((unsigned long)[profiles count]+1)];
    [profiles setObject:deviceProfile forKey:profileName];
    [defaults setObject:profiles forKey:@"profiles"];
    [defaults setObject:profileName forKey:@"selectedDevice"];
    [defaults synchronize];
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"DownloadContentNotification"
     object:self
     userInfo:nil];
}

-(void)saveToken:(NSDictionary *)dictionary{
    defaults=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary* profiles=[[defaults valueForKey:@"profiles"]mutableCopy];
    NSMutableDictionary* deviceProfile=[[profiles valueForKey:[defaults valueForKey:@"selectedDevice"]]mutableCopy];
    
    [deviceProfile setObject:[dictionary valueForKey:@"access_token"]
                      forKey:@"accessToken"];
    [deviceProfile setObject:[dictionary valueForKey:@"refresh_token"]
                      forKey:@"refreshToken"];
    [deviceProfile setObject:[dictionary valueForKey:@"token_type"]
                      forKey:@"tokenType"];
    NSTimeInterval timeInterval=[[[dictionary valueForKey:@"oauth2AccessToken"]valueForKey:@"expires_in"] doubleValue];
    NSDate *date=[NSDate date];
    date=[date dateByAddingTimeInterval:timeInterval];
    [deviceProfile setObject:date
                      forKey:@"expiryTime"];
    
    
    [profiles setObject:deviceProfile forKey:[defaults valueForKey:@"selectedDevice"]];
    [defaults setObject:profiles forKey:@"profiles"];
    [defaults synchronize];
    
    [[NSNotificationCenter defaultCenter]
                            postNotificationName:@"DownloadContentNotification"
                                          object:self
                                        userInfo:nil];
}

-(void)resetBadgeForContent:(NSString*)badgeName{
    defaults=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary* profiles=[[defaults valueForKey:@"profiles"]mutableCopy];
    NSMutableDictionary* deviceProfile=[[profiles valueForKey:[defaults valueForKey:@"selectedDevice"]]mutableCopy];
    
    NSMutableDictionary *badgeDictionary=[[deviceProfile valueForKey:@"badgeDictionary"]mutableCopy];
    
    [badgeDictionary setObject:[NSNumber numberWithBool:NO] forKey:badgeName];
    
    [deviceProfile setObject:badgeDictionary forKey:@"badgeDictionary"];
        
    [profiles setObject:deviceProfile forKey:[defaults valueForKey:@"selectedDevice"]];
    [defaults setObject:profiles forKey:@"profiles"];
    [defaults synchronize];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"stopAnimating"
     object:self
     userInfo:nil];
}


@end
