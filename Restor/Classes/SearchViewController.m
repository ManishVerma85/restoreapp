//
//  SearchViewController.m
//  Restor
//
//  Created by Deva Palanisamy on 18/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import "SearchViewController.h"

@interface SearchViewController ()

@end

@implementation SearchViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        UITabBarItem *barItem = [[UITabBarItem alloc]initWithTitle:NSLocalizedString(@"Search",@"") image:[UIImage imageNamed:@"search"] tag:2];
    
        
        [self setTabBarItem:barItem];
    
    
        defaults=[NSUserDefaults standardUserDefaults];
        
        //mv
        // Set screen name.
        self.screenName = @"Search Screen";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    self.view=[[UIView alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    self.view.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.view.autoresizesSubviews=YES;
    searchResults=[[NSMutableArray alloc]init];
    tableView=[[UITableView alloc]initWithFrame:self.view.bounds];
    tableView.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    tableView.autoresizesSubviews=YES;
    tableView.delegate=self;
    tableView.dataSource=self;
    tableView.rowHeight=73;
    self.view.backgroundColor=[UIColor whiteColor];
    UILabel *titleLabel=[UIViewController getTitleLabelWithFrame:self.view.frame];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.text=NSLocalizedString(@"Search",@"");
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    self.navigationItem.titleView=titleLabel;
    isSearching=NO;
    
    searchBar = [[UISearchBar alloc] initWithFrame:tableView.bounds];
    [searchBar sizeToFit];
    searchBar.tag=1;
    searchBar.delegate = self;
    
    //self.navigationItem.titleView = searchBar;
    
    tableView.tableHeaderView=searchBar;

    [self.view addSubview:tableView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self prepareSearchContent];
    types = @[NSLocalizedString(@"Messages",@""),
                       NSLocalizedString(@"Contacts",@""),
                       NSLocalizedString(@"Call History",@""),
                       NSLocalizedString(@"Calendar",@""),
                       NSLocalizedString(@"Chat History",@""),
                       NSLocalizedString(@"Notes",@"")];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Search"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - search bar delegate method implementation
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchbar
{
    [searchbar resignFirstResponder];
    [searchbar setShowsCancelButton:NO animated:YES];
    isSearching=NO;
}

-(void) searchBarTextDidBeginEditing:(UISearchBar *)searchbar
{
    if (!isSearching)
    {
        searchbar.tintColor = [UIColor whiteColor];
        
    
        
        isSearching=YES;
        tableView.tableHeaderView=nil;
        self.navigationItem.titleView = searchBar;
        
        [searchBar setShowsCancelButton:YES animated:YES];
        [searchbar becomeFirstResponder];
    }
    else
    {
        
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchbar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    searchbar.tintColor = [UIColor whiteColor];
    [tableView setTableHeaderView:searchBar];
    self.navigationItem.titleView=nil;
    self.navigationItem.title=NSLocalizedString(@"Search",@"");

    
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
    
    //self.
    searchbar.text=@"";
    isSearching=NO;
    searchResults=nil;
    [tableView reloadData];
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
   [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    /* message filter*/
    searchResults=[[NSMutableArray alloc]init];
    if([searchResults count]){
        [searchResults removeAllObjects];
    }
    
    NSString* namePredicateString=[NSString stringWithFormat:@"name contains[c] '%@'",searchText];
    predicate=[NSPredicate predicateWithFormat:namePredicateString];
    [searchResults addObjectsFromArray:[messages filteredArrayUsingPredicate:predicate]];
    
    namePredicateString=[NSString stringWithFormat:@"name contains[c] '%@'",searchText];
    predicate=[NSPredicate predicateWithFormat:namePredicateString];
    [searchResults addObjectsFromArray:[contacts filteredArrayUsingPredicate:predicate]];
    
    namePredicateString=[NSString stringWithFormat:@"name contains[c] '%@'",searchText];
    predicate=[NSPredicate predicateWithFormat:namePredicateString];
    [searchResults addObjectsFromArray:[whatsapp filteredArrayUsingPredicate:predicate]];
    
    namePredicateString=[NSString stringWithFormat:@"name contains[c] '%@'",searchText];
    predicate=[NSPredicate predicateWithFormat:namePredicateString];
    [searchResults addObjectsFromArray:[callsHistory filteredArrayUsingPredicate:predicate]];
    
    namePredicateString=[NSString stringWithFormat:@"note contains[c] '%@'",searchText];
    predicate=[NSPredicate predicateWithFormat:namePredicateString];
    [searchResults addObjectsFromArray:[notes filteredArrayUsingPredicate:predicate]];
    
    namePredicateString=[NSString stringWithFormat:@"title contains[c] '%@'",searchText];
    predicate=[NSPredicate predicateWithFormat:namePredicateString];
    [searchResults addObjectsFromArray:[calendar filteredArrayUsingPredicate:predicate]];
    
    NSLog(@"Results are: %@",[searchResults description]);
    
    [tableView reloadData];
}


-(void)prepareSearchContent{
    notes=[RestoreManager sharedManager].currentDevice.notes;
    messages=[RestoreManager sharedManager].currentDevice.messages;
    whatsapp=[RestoreManager sharedManager].currentDevice.whatsApp;
    calendar=[RestoreManager sharedManager].currentDevice.calender;
    contacts=[RestoreManager sharedManager].currentDevice.contacts;
    callsHistory=[RestoreManager sharedManager].currentDevice.callHistory;
    /*
    NSMutableArray *filesList=[[[defaults valueForKey:@"profiles"]
                                valueForKey:[defaults valueForKey:@"selectedDevice"]]
                               valueForKey:@"filesList"];
    if (filesList!=nil)
    {
        for (NSString* filename in filesList)
        {
            NSData *data=[NSData dataWithContentsOfFile:filename];
            NSString* fileType=[[filename lastPathComponent] stringByDeletingPathExtension];
            if ([fileType isEqualToString:@"notes"])
            {
                notes=[RestoreManager sharedManager].currentDevice.notes;
            }
            else if ([fileType isEqualToString:@"messages"])
            {
                messages=[RestoreManager sharedManager].currentDevice.messages;
            }
            else if ([fileType isEqualToString:@"whatsapp"])
            {
                whatsapp=[RestoreManager sharedManager].currentDevice.whatsApp;
            }
            else if ([fileType isEqualToString:@"calendar"])
            {
                calendar=[RestoreManager sharedManager].currentDevice.calender;
            }
            else if ([fileType isEqualToString:@"contacts"])
            {
                contacts=[RestoreManager sharedManager].currentDevice.contacts;
            }
            else if ([fileType isEqualToString:@"callhistory"])
            {
                callsHistory=[RestoreManager sharedManager].currentDevice.callHistory;
            }
        }
    }
     */

}

-(NSString*)getContentType:(NSDictionary*)dict{
    if ([messages containsObject:dict]) {
        return [types objectAtIndex:0];
    }else if ([whatsapp containsObject:dict]){
        return [types objectAtIndex:4];
    }else if ([notes containsObject:dict]){
        return [types objectAtIndex:5];
    }else if ([calendar containsObject:dict]){
        return [types objectAtIndex:3];
    }else if ([callsHistory containsObject:dict]){
        return [types objectAtIndex:2];
    }else if ([contacts containsObject:dict]){
        return [types objectAtIndex:1];
    }else{
        return @"error";
    }
}

#pragma Tableview Delegate implementation
- (HomeDetailTableViewCell *)tableView:(UITableView *)tableview cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    NSDictionary *dict=[searchResults objectAtIndex:indexPath.row];
    //NSString* type=[self getContentType:dict];
    
    if ([messages containsObject:dict]) {
        type=(NSString*) [types objectAtIndex:0];
    }else if ([whatsapp containsObject:dict]){
        type= (NSString*) [types objectAtIndex:4];
    }else if ([notes containsObject:dict]){
        type= (NSString*) [types objectAtIndex:5];
    }else if ([calendar containsObject:dict]){
        type= (NSString*) [types objectAtIndex:3];
    }else if ([callsHistory containsObject:dict]){
        type= (NSString*) [types objectAtIndex:2];
    }else if ([contacts containsObject:dict]){
        type= (NSString*) [types objectAtIndex:1];
    }

        HomeDetailTableViewCell* cell = [[HomeDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                              reuseIdentifier:MyIdentifier withType:type];
    
    
    //makes the lines go all the way across
  //  cell.layoutMargins = UIEdgeInsetsZero; doesnt work in ios7
    
    if ([type isEqualToString:NSLocalizedString(@"Messages",@"")])
    {
        Message *objMessage = (Message *)dict;
        
        cell.titleLabel.text=objMessage.name;
        NSString* dateString=[(Messages *)[objMessage.messages firstObject] date];
        cell.timeLabel.text=[self convertDateToRelativeString:dateString];
        cell.detailTextLabel.text=[(Messages *)[objMessage.messages firstObject] message];
        
    }
    else if ([type isEqualToString:NSLocalizedString(@"Contacts",@"")])
    {
        Contact *objContact = (Contact *)dict;
        cell.titleLabel.text=objContact.name;
    }
    else if ([type isEqualToString:NSLocalizedString(@"Chat History",@"")])
    {
        Whatsapp *objWhatssApp = (Whatsapp *)dict;
        
        cell.titleLabel.text=objWhatssApp.name;
        NSString* dateString=[(Messages *)[objWhatssApp.messages firstObject] date];
        cell.timeLabel.text=[self convertDateToRelativeString:dateString];
        cell.detailTextLabel.text=[(Messages *)[objWhatssApp.messages firstObject] message];
    }
    else if ([type isEqualToString:NSLocalizedString(@"Call History",@"")])
    {
        CallHistory *objCallHistory = (CallHistory *)dict;
        
        if ([objCallHistory.name isEqualToString:@""])
        {
            cell.titleLabel.text=objCallHistory.phone;
            cell.timeLabel.text=@"";
        }
        else
        {
            cell.titleLabel.text=objCallHistory.name;
            cell.timeLabel.text=objCallHistory.phone;
        }
        cell.timeLabel.text=[self convertDateToRelativeString:objCallHistory.date];
        NSArray *dateArray =[objCallHistory.date componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString *time=[dateArray objectAtIndex:1];
        cell.detailTextLabel.text=[NSString stringWithFormat:@"%@ %@ %@",time,objCallHistory.type,objCallHistory.duration];
        if ([objCallHistory.type isEqualToString:NSLocalizedString(@"Missed",@"")])
        {
            cell.titleLabel.textColor=[UIColor redColor];
        }
    }
    else if ([type isEqualToString:NSLocalizedString(@"Calendar",@"")])
    {
        Calender *objCalender = (Calender *)dict;
        
        cell.titleLabel.text=objCalender.title;
        cell.detailTextLabel.text=objCalender.location;
        cell.startTimeLabel.text=objCalender.startsDate;
        cell.endTimeLabel.text=objCalender.endsDate;
    }
    else if ([type isEqualToString:NSLocalizedString(@"Notes",@"")])
    {
        Notes *objNote = (Notes *)dict;
        cell.titleLabel.text=objNote.note;
        cell.timeLabel.text=[self convertDateToRelativeString:objNote.date];
    }
    
    return cell;
}

-(NSString*)convertDateToRelativeString:(NSString*)datestring{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM-dd-yyyy HH:mm"];
    
    NSDate *date = [dateFormat dateFromString:datestring];
    NSDateFormatter *newDateFormatter = [[NSDateFormatter alloc]init];
    [newDateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString *timeAgo=[date formattedAsTimeAgo];
    return timeAgo;
}
-(void) tableView:(UITableView *)tableViewLocal willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    /* if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row)
     {
     //Finished loading visible part of your table
     if (APPDELEGATE.didUserRequestForRemindLaterOption == NO)
     {
     //            [self showiRateMessage];
     }
     
     }*/
}

- (void)tableView:(UITableView *)tableview didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [searchBar resignFirstResponder];
    NSDictionary *dict1 = [searchResults objectAtIndex:indexPath.row];
    NSUInteger index=[types indexOfObject:[self getContentType:dict1]];
    [tableview deselectRowAtIndexPath:indexPath animated:YES];
    switch (index) {
        case 0:
        {
            MessagesViewController *mvc=[[MessagesViewController alloc]init];
            [mvc setMessagesDictionary:dict1];
            mvc.title=[dict1 valueForKey:@"name"];
            [mvc setWhatsAppKey:NO];
            [self.navigationController pushViewController:mvc animated:YES];
        }
            break;
        case 1:
        {
            [self saveContact:dict1];
        }
            break;
        case 2:
        {
            //[self displayContact:dict1];
            UIAlertView *calert=nil;
            NSString *phNo = [dict1 valueForKey:@"phone"];
            NSString *cleanedString = [[phNo componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
            NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",cleanedString]];
            
            if ([[UIApplication sharedApplication] canOpenURL:phoneUrl])
            {
                [[UIApplication sharedApplication] openURL:phoneUrl];
            }
            else
            {
                calert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Alert",@"") message: NSLocalizedString(@"Call facility is not available.",@"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"") otherButtonTitles:nil, nil];
                [calert show];
            }
        }
            break;
        case 3:
        {
            [self displayEvent:dict1];
        }
            break;
        case 4:
        {
            MessagesViewController *mvc=[[MessagesViewController alloc]init];
            [mvc setMessagesDictionary:dict1];
            mvc.title=[dict1 valueForKey:@"name"];
            [mvc setWhatsAppKey:YES];
            [self.navigationController pushViewController:mvc animated:YES];
        }
            break;
        case 5:
        {
            NotesViewController *nvc=[[NotesViewController alloc]init];
            [nvc setNotesDictionary:dict1];
            [self.navigationController pushViewController:nvc animated:YES];
        }
            break;
        default:
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableViewLocal
{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
   // tableView.layoutMargins = UIEdgeInsetsZero;
    
    
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [searchResults count];
}
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dict1 = [searchResults objectAtIndex:indexPath.row];
    [self displayContact:dict1];
}
#pragma mark - Display Event
-(void)displayEvent:(NSDictionary*)dictionary{
    eventStore = [[EKEventStore alloc] init];
    if([eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)]) {
        [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            //[self presentViewController:controller animated:YES completion:^{ NSLog(@"Event Displayed");}];
            [self performSelectorOnMainThread:@selector(displayCalenderView:) withObject:dictionary waitUntilDone:YES];
        }];
    }
    
}
-(void)displayCalenderView:(NSDictionary*)dictionary{
    EKEvent * event = [EKEvent eventWithEventStore:eventStore];
    event.title     = [dictionary valueForKey:@"title"];
    event.location  = [dictionary valueForKey:@"location"];
    event.startDate = [self convertStringToDate:[dictionary valueForKey:@"startsDate"]];
    event.endDate   = [self convertStringToDate:[dictionary valueForKey:@"endsDate"]];
    event.notes     = [dictionary valueForKey:@"note"];
    
    EKEventViewController *evc=[[EKEventViewController alloc]init];
    evc.event=event;
    UILabel *titleLabel=[UIViewController getTitleLabelWithFrame:self.view.frame];
    titleLabel.text=NSLocalizedString(@"Event Details",@"");
    [titleLabel sizeToFit];
    evc.navigationItem.titleView=titleLabel;
    
    [self.navigationController pushViewController:evc animated:YES];
}

-(NSDate*)convertStringToDate:(NSString*)datestring{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM-dd-yyyy HH:mm"];
    
    NSDate *date = [dateFormat dateFromString:datestring];
    NSDateFormatter *newDateFormatter = [[NSDateFormatter alloc]init];
    [newDateFormatter setDateFormat:@"MM/dd/yyyy"];
    return date;
}

#pragma mark - Display contacts
-(void)saveContact:(NSDictionary*)dictionary{
    
    ABRecordRef person = ABPersonCreate();
    
    NSString* venueName=[dictionary valueForKey:@"name"];
    NSString* venueUrl=@" ";
    NSString* venueUrl2=@" ";
    NSString* venueEmail=@" ";
    NSString* venuePhone=[dictionary valueForKey:@"mobile"];
    
    ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFStringRef) venueName, NULL);
    
    if (venueUrl)
    {
        ABMutableMultiValueRef urlMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueAddValueAndLabel(urlMultiValue, (__bridge CFStringRef) venueUrl, kABPersonHomePageLabel, NULL);
        ABMultiValueAddValueAndLabel(urlMultiValue, (__bridge CFStringRef) venueUrl2, kABPersonHomePageLabel, NULL);
        ABRecordSetValue(person, kABPersonURLProperty, urlMultiValue, nil);
        CFRelease(urlMultiValue);
    }
    
    if (venueEmail)
    {
        ABMutableMultiValueRef emailMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueAddValueAndLabel(emailMultiValue, (__bridge CFStringRef) venueEmail, kABWorkLabel, NULL);
        ABRecordSetValue(person, kABPersonEmailProperty, emailMultiValue, nil);
        CFRelease(emailMultiValue);
    }
    
    if (venuePhone)
    {
        ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        NSArray *venuePhoneNumbers = [venuePhone componentsSeparatedByString:@" or "];
        for (NSString *venuePhoneNumberString in venuePhoneNumbers)
            ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFStringRef) venuePhoneNumberString, kABPersonPhoneMainLabel, NULL);
        ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil);
        CFRelease(phoneNumberMultiValue);
    }
    

    
    ABMutableMultiValueRef multiAddress = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
    NSMutableDictionary *addressDictionary = [[NSMutableDictionary alloc] init];
    
    ABMultiValueAddValueAndLabel(multiAddress, (__bridge CFDictionaryRef) addressDictionary, kABWorkLabel, NULL);
    ABRecordSetValue(person, kABPersonAddressProperty, multiAddress, NULL);
    CFRelease(multiAddress);
    
    ABUnknownPersonViewController *controller = [[ABUnknownPersonViewController alloc] init];
    
    controller.displayedPerson = person;
    controller.allowsAddingToAddressBook = YES;
    UILabel *titleLabel=[UIViewController getTitleLabelWithFrame:self.view.frame];
    titleLabel.text=venueName;
    [titleLabel sizeToFit];
    controller.navigationItem.titleView=titleLabel;
    
    [self.navigationController pushViewController:controller animated:YES];
    
    CFRelease(person);
}

#pragma mark - Display call history
-(void)displayContact:(NSDictionary*)dictionary
{
    
    ABRecordRef person = ABPersonCreate();
    
    NSString* venueName=[dictionary valueForKey:@"name"];
    NSString* venueUrl=nil;
    NSString* venueUrl2=nil;
    NSString* venueEmail=nil;
    NSString* venuePhone=[dictionary valueForKey:@"phone"];
    
    ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFStringRef) venueName, NULL);
    
    if (venueUrl)
    {
        ABMutableMultiValueRef urlMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueAddValueAndLabel(urlMultiValue, (__bridge CFStringRef) venueUrl, kABPersonHomePageLabel, NULL);
        ABMultiValueAddValueAndLabel(urlMultiValue, (__bridge CFStringRef) venueUrl2, kABPersonHomePageLabel, NULL);
        ABRecordSetValue(person, kABPersonURLProperty, urlMultiValue, nil);
        CFRelease(urlMultiValue);
    }
    
    if (venueEmail)
    {
        ABMutableMultiValueRef emailMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueAddValueAndLabel(emailMultiValue, (__bridge CFStringRef) venueEmail, kABWorkLabel, NULL);
        ABRecordSetValue(person, kABPersonEmailProperty, emailMultiValue, nil);
        CFRelease(emailMultiValue);
    }
    
    if (venuePhone)
    {
        ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        NSArray *venuePhoneNumbers = [venuePhone componentsSeparatedByString:@" or "];
        for (NSString *venuePhoneNumberString in venuePhoneNumbers)
            ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFStringRef) venuePhoneNumberString, kABPersonPhoneMainLabel, NULL);
        ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil);
        CFRelease(phoneNumberMultiValue);
    }
    
    ABMutableMultiValueRef multiAddress = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
    NSMutableDictionary *addressDictionary = [[NSMutableDictionary alloc] init];
    
    ABMultiValueAddValueAndLabel(multiAddress, (__bridge CFDictionaryRef) addressDictionary, kABWorkLabel, NULL);
    ABRecordSetValue(person, kABPersonAddressProperty, multiAddress, NULL);
    CFRelease(multiAddress);
    
    ABUnknownPersonViewController *controller = [[ABUnknownPersonViewController alloc] init];
    
    controller.displayedPerson = person;
    controller.allowsAddingToAddressBook = YES;
    
    UILabel *titleLabel=[UIViewController getTitleLabelWithFrame:self.view.frame];
    titleLabel.text=venueName;
    [titleLabel sizeToFit];
    controller.navigationItem.titleView=titleLabel;
    
    [self.navigationController pushViewController:controller animated:YES];
    
    CFRelease(person);
}

@end
