//
//  HomeDetailViewController.h
//  Restor
//
//  Created by Deva Palanisamy on 23/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessagesViewController.h"
#import "NotesViewController.h"
#import "ContactsViewController.h"
#import "WhatsAppChatViewController.h"
#import "HomeDetailTableViewCell.h"
#import "UIViewController+Helper.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>
#import "NSDate+NVTimeAgo.h"
#import "NSDate+NSharpDate.h"

//mv
@interface HomeDetailViewController : GAITrackedViewController<UITableViewDataSource,UITableViewDelegate,EKEventEditViewDelegate,UISearchBarDelegate,UIAlertViewDelegate,UIActionSheetDelegate>{
    UITableView *tableView;
    UISearchBar *searchbar;
    BOOL isSearching;
    NSArray *titles;
    EKEventStore * eventStore;
    UISegmentedControl *segmentedControl;
    NSPredicate *namePredicate;
    NSMutableArray *deleteIndexPaths;
    NSMutableIndexSet *deleteIndexes;
    BOOL isSetEditMode;
    
    EKEventViewController *evc;
}
@property (nonatomic, retain) NSArray *nonFilterContent;
@property (nonatomic, retain) NSArray *filterContent;
@property (nonatomic, retain) NSMutableArray *array;

//mv
@property (nonatomic, copy) NSString *screenName;

@end
