//
//  AllMacros.h
//  Restor
//
//  Created by Deva Palanisamy on 25/05/2014.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#ifndef Restor_AllMacros_h
#define Restor_AllMacros_h
#define UI_FONT_SETTINGS_TABLE [UIFont boldSystemFontOfSize:15.0f]

#define UI_FONT_FOR_NOTES [UIFont boldSystemFontOfSize:15.0f]
#define UI_FONT_FOR_NOTES_TABLE [UIFont boldSystemFontOfSize:15.0f]
#define UI_FONT_FOR_NOTES_TIME_LABEL [UIFont boldSystemFontOfSize:12.0f]

#define UI_FONT_FOR_CALENDAR_TITLE [UIFont boldSystemFontOfSize:15.0f]
#define UI_FONT_FOR_CALENDAR_LOCATION [UIFont boldSystemFontOfSize:12.0f]
#define UI_FONT_FOR_CALENDAR_DATE [UIFont boldSystemFontOfSize:13.0f]

#define UI_FONT_FOR_TIME_LABEL [UIFont boldSystemFontOfSize:12.0f]

#define UI_FONT_FOR_CONTACT_TITLE [UIFont boldSystemFontOfSize:15.0f]

#define UI_FONT_FOR_MESSAGE_TITLE [UIFont boldSystemFontOfSize:15.0f]
#define UI_FONT_FOR_MESSAGE_TIME_LABEL [UIFont boldSystemFontOfSize:13.0f]
#define UI_FONT_FOR_MESSAGE_TEXT_LABEL [UIFont boldSystemFontOfSize:13.0f]

#define UI_FONT_FOR_CALLHISTORY_NAME_LABEL [UIFont boldSystemFontOfSize:17.0f]
#define UI_FONT_FOR_CALLHISTORY_NUMBER_LABEL [UIFont boldSystemFontOfSize:12.0f]
#define UI_FONT_FOR_CALLHISTORY_DETAILTEXT_LABEL [UIFont boldSystemFontOfSize:11.0f]

#define UI_FONT_FOR_ALERT [UIFont boldSystemFontOfSize:14.0f]

#define URL_TO_GET_TOKEN  '@"https://107.170.28.5:8443/smartphonedatarecovery/v1.0/connect/connections"'




#endif
