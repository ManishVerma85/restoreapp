//
//  Messages.m
//
//  Created by UMANGSHU  on 05/12/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "Messages.h"


NSString *const kMessagesDate = @"date";
NSString *const kMessagesType = @"type";
NSString *const kMessagesMessage = @"message";


@interface Messages ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Messages

@synthesize date = _date;
@synthesize type = _type;
@synthesize message = _message;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.date = [self objectOrNilForKey:kMessagesDate fromDictionary:dict];
            self.type = [self objectOrNilForKey:kMessagesType fromDictionary:dict];
            self.message = [self objectOrNilForKey:kMessagesMessage fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.date forKey:kMessagesDate];
    [mutableDict setValue:self.type forKey:kMessagesType];
    [mutableDict setValue:self.message forKey:kMessagesMessage];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.date = [aDecoder decodeObjectForKey:kMessagesDate];
    self.type = [aDecoder decodeObjectForKey:kMessagesType];
    self.message = [aDecoder decodeObjectForKey:kMessagesMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_date forKey:kMessagesDate];
    [aCoder encodeObject:_type forKey:kMessagesType];
    [aCoder encodeObject:_message forKey:kMessagesMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    Messages *copy = [[Messages alloc] init];
    
    if (copy) {

        copy.date = [self.date copyWithZone:zone];
        copy.type = [self.type copyWithZone:zone];
        copy.message = [self.message copyWithZone:zone];
    }
    
    return copy;
}


@end
