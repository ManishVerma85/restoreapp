//
//  Whatsapp.m
//
//  Created by UMANGSHU  on 05/12/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "Whatsapp.h"
#import "Messages.h"


NSString *const kWhatsappName = @"name";
NSString *const kWhatsappPhone = @"phone";
NSString *const kWhatsappMessages = @"messages";


@interface Whatsapp ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Whatsapp

@synthesize name = _name;
@synthesize phone = _phone;
@synthesize messages = _messages;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.name = [self objectOrNilForKey:kWhatsappName fromDictionary:dict];
            self.phone = [self objectOrNilForKey:kWhatsappPhone fromDictionary:dict];
        self.messages = [[NSMutableArray alloc] init];
        for (NSDictionary *dic in [dict objectForKey:kWhatsappMessages])
        {
            [self.messages addObject:[Messages modelObjectWithDictionary:dic]];
        }
        
        /*
    NSObject *receivedMessages = [dict objectForKey:kWhatsappMessages];
    NSMutableArray *parsedMessages = [NSMutableArray array];
    if ([receivedMessages isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedMessages) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedMessages addObject:[Messages modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedMessages isKindOfClass:[NSDictionary class]]) {
       [parsedMessages addObject:[Messages modelObjectWithDictionary:(NSDictionary *)receivedMessages]];
    }

    self.messages = [NSArray arrayWithArray:parsedMessages];
         */
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.name forKey:kWhatsappName];
    [mutableDict setValue:self.phone forKey:kWhatsappPhone];
    NSMutableArray *tempArrayForMessages = [NSMutableArray array];
    for (NSObject *subArrayObject in self.messages) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForMessages addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForMessages addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForMessages] forKey:kWhatsappMessages];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.name = [aDecoder decodeObjectForKey:kWhatsappName];
    self.phone = [aDecoder decodeObjectForKey:kWhatsappPhone];
    self.messages = [aDecoder decodeObjectForKey:kWhatsappMessages];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_name forKey:kWhatsappName];
    [aCoder encodeObject:_phone forKey:kWhatsappPhone];
    [aCoder encodeObject:_messages forKey:kWhatsappMessages];
}

- (id)copyWithZone:(NSZone *)zone
{
    Whatsapp *copy = [[Whatsapp alloc] init];
    
    if (copy) {

        copy.name = [self.name copyWithZone:zone];
        copy.phone = [self.phone copyWithZone:zone];
        copy.messages = [self.messages copyWithZone:zone];
    }
    
    return copy;
}


@end
