//
//  RestoreManager.m
//  restor
//
//  Created by Umang on 12/8/14.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import "RestoreManager.h"

@implementation RestoreManager

@synthesize currentDevice;

static RestoreManager *shared;

////////////////////////////////////////////////////////////////////////
+ (instancetype)sharedManager
{
    static RestoreManager *_sharedReader = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedReader = [[RestoreManager alloc] init];
    });
    
    return _sharedReader;
}

////////////////////////////////////////////////////////////////////////
- (id)init
{
    self = [super init];
    
    if (self)
    {
        
    }
    
    return self;
}

@end
