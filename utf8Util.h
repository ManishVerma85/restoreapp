//
//  utf8Util.h
//  restor
//
//  Created by Andy on 22/07/2015.
//  Copyright (c) 2015 AgamPuram. All rights reserved.
//

#import "fileUtlities.h"
#import <Foundation/Foundation.h>

@interface utf8Util : NSObject
{
    fileUtlities * _fileUtil;
    
    NSMutableArray * _invalidPositions;
   
    BOOL _hasFoundErrors;
    
}

-(BOOL)Validateutf8File:(NSString *)fileName;

+(BOOL)insertUTF8TestDataIntoFile:(NSString *)filePath  atPosition:(int)pos;

@end
