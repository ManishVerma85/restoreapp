//
//  Notes.m
//
//  Created by UMANGSHU  on 05/12/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "Notes.h"


NSString *const kNotesDate = @"date";
NSString *const kNotesNote = @"note";


@interface Notes ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Notes

@synthesize date = _date;
@synthesize note = _note;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.date = [self objectOrNilForKey:kNotesDate fromDictionary:dict];
            self.note = [self objectOrNilForKey:kNotesNote fromDictionary:dict];

    }
    
    return self;
    
}

-(BOOL)isEqualToNotes:(Notes *)object
{
    BOOL b_date = [self.date isEqualToString:object.date];
    BOOL b_note = [self.note isEqualToString:object.note];
    
    if (b_date && b_note)
    {
        return YES;
    }
    return NO;
}


- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.date forKey:kNotesDate];
    [mutableDict setValue:self.note forKey:kNotesNote];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.date = [aDecoder decodeObjectForKey:kNotesDate];
    self.note = [aDecoder decodeObjectForKey:kNotesNote];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_date forKey:kNotesDate];
    [aCoder encodeObject:_note forKey:kNotesNote];
}

- (id)copyWithZone:(NSZone *)zone
{
    Notes *copy = [[Notes alloc] init];
    
    if (copy) {

        copy.date = [self.date copyWithZone:zone];
        copy.note = [self.note copyWithZone:zone];
    }
    
    return copy;
}


@end
