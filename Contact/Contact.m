//
//  Contact.m
//
//  Created by UMANGSHU  on 05/12/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "Contact.h"


NSString *const kContactOther = @"other";
NSString *const kContactJobTitle = @"jobTitle";
NSString *const kContactHomeEmail = @"homeEmail";
NSString *const kContactNote = @"note";
NSString *const kContactMobile = @"mobile";
NSString *const kContactHomePage = @"homePage";
NSString *const kContactCompany = @"company";
NSString *const kContactWork = @"work";
NSString *const kContactOtherEmail = @"otherEmail";
NSString *const kContactMain = @"main";
NSString *const kContactAddress = @"address";
NSString *const kContactHome = @"home";
NSString *const kContactDepartment = @"department";
NSString *const kContactIPhone = @"iPhone";
NSString *const kContactName = @"name";
NSString *const kContactWorkEmail = @"workEmail";


@interface Contact ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Contact

@synthesize other = _other;
@synthesize jobTitle = _jobTitle;
@synthesize homeEmail = _homeEmail;
@synthesize note = _note;
@synthesize mobile = _mobile;
@synthesize homePage = _homePage;
@synthesize company = _company;
@synthesize work = _work;
@synthesize otherEmail = _otherEmail;
@synthesize main = _main;
@synthesize address = _address;
@synthesize home = _home;
@synthesize department = _department;
@synthesize iPhone = _iPhone;
@synthesize name = _name;
@synthesize workEmail = _workEmail;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.other = [self objectOrNilForKey:kContactOther fromDictionary:dict];
            self.jobTitle = [self objectOrNilForKey:kContactJobTitle fromDictionary:dict];
            self.homeEmail = [self objectOrNilForKey:kContactHomeEmail fromDictionary:dict];
            self.note = [self objectOrNilForKey:kContactNote fromDictionary:dict];
            self.mobile = [self objectOrNilForKey:kContactMobile fromDictionary:dict];
            self.homePage = [self objectOrNilForKey:kContactHomePage fromDictionary:dict];
            self.company = [self objectOrNilForKey:kContactCompany fromDictionary:dict];
            self.work = [self objectOrNilForKey:kContactWork fromDictionary:dict];
            self.otherEmail = [self objectOrNilForKey:kContactOtherEmail fromDictionary:dict];
            self.main = [self objectOrNilForKey:kContactMain fromDictionary:dict];
            self.address = [self objectOrNilForKey:kContactAddress fromDictionary:dict];
            self.home = [self objectOrNilForKey:kContactHome fromDictionary:dict];
            self.department = [self objectOrNilForKey:kContactDepartment fromDictionary:dict];
            self.iPhone = [self objectOrNilForKey:kContactIPhone fromDictionary:dict];
            self.name = [self objectOrNilForKey:kContactName fromDictionary:dict];
            self.workEmail = [self objectOrNilForKey:kContactWorkEmail fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.other forKey:kContactOther];
    [mutableDict setValue:self.jobTitle forKey:kContactJobTitle];
    [mutableDict setValue:self.homeEmail forKey:kContactHomeEmail];
    [mutableDict setValue:self.note forKey:kContactNote];
    [mutableDict setValue:self.mobile forKey:kContactMobile];
    [mutableDict setValue:self.homePage forKey:kContactHomePage];
    [mutableDict setValue:self.company forKey:kContactCompany];
    [mutableDict setValue:self.work forKey:kContactWork];
    [mutableDict setValue:self.otherEmail forKey:kContactOtherEmail];
    [mutableDict setValue:self.main forKey:kContactMain];
    [mutableDict setValue:self.address forKey:kContactAddress];
    [mutableDict setValue:self.home forKey:kContactHome];
    [mutableDict setValue:self.department forKey:kContactDepartment];
    [mutableDict setValue:self.iPhone forKey:kContactIPhone];
    [mutableDict setValue:self.name forKey:kContactName];
    [mutableDict setValue:self.workEmail forKey:kContactWorkEmail];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

-(BOOL)isEqualToContact:(Contact *)object
{
    BOOL b_other = [self.other isEqualToString:object.other];
    BOOL b_jobTitle = [self.jobTitle isEqualToString:object.jobTitle];
    BOOL b_homeEmail = [self.homeEmail isEqualToString:object.homeEmail];
    BOOL b_note = [self.note isEqualToString:object.note];
    BOOL b_mobile = [self.mobile isEqualToString:object.mobile];
    BOOL b_homepage = [self.homePage isEqualToString:object.homePage];
    BOOL b_company = [self.company isEqualToString:object.company];
    BOOL b_work = [self.work isEqualToString:object.work];
    BOOL b_otherEmail = [self.otherEmail isEqualToString:object.otherEmail];
    BOOL b_main = [self.main isEqualToString:object.main];
    BOOL b_address = [self.address isEqualToString:object.address];
    BOOL b_home = [self.home isEqualToString:object.home];
    BOOL b_department = [self.home isEqualToString:object.department];
    BOOL b_iPhone = [self.home isEqualToString:object.iPhone];
    BOOL b_name = [self.name isEqualToString:object.name];
    BOOL b_workEmail = [self.workEmail isEqualToString:object.workEmail];
    
    if (b_other && b_jobTitle && b_homeEmail && b_note && b_mobile && b_homepage && b_company && b_work && b_otherEmail  && b_main && b_address && b_home && b_department && b_iPhone && b_name && b_workEmail)
    {
        return YES;
    }
    return NO;
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.other = [aDecoder decodeObjectForKey:kContactOther];
    self.jobTitle = [aDecoder decodeObjectForKey:kContactJobTitle];
    self.homeEmail = [aDecoder decodeObjectForKey:kContactHomeEmail];
    self.note = [aDecoder decodeObjectForKey:kContactNote];
    self.mobile = [aDecoder decodeObjectForKey:kContactMobile];
    self.homePage = [aDecoder decodeObjectForKey:kContactHomePage];
    self.company = [aDecoder decodeObjectForKey:kContactCompany];
    self.work = [aDecoder decodeObjectForKey:kContactWork];
    self.otherEmail = [aDecoder decodeObjectForKey:kContactOtherEmail];
    self.main = [aDecoder decodeObjectForKey:kContactMain];
    self.address = [aDecoder decodeObjectForKey:kContactAddress];
    self.home = [aDecoder decodeObjectForKey:kContactHome];
    self.department = [aDecoder decodeObjectForKey:kContactDepartment];
    self.iPhone = [aDecoder decodeObjectForKey:kContactIPhone];
    self.name = [aDecoder decodeObjectForKey:kContactName];
    self.workEmail = [aDecoder decodeObjectForKey:kContactWorkEmail];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_other forKey:kContactOther];
    [aCoder encodeObject:_jobTitle forKey:kContactJobTitle];
    [aCoder encodeObject:_homeEmail forKey:kContactHomeEmail];
    [aCoder encodeObject:_note forKey:kContactNote];
    [aCoder encodeObject:_mobile forKey:kContactMobile];
    [aCoder encodeObject:_homePage forKey:kContactHomePage];
    [aCoder encodeObject:_company forKey:kContactCompany];
    [aCoder encodeObject:_work forKey:kContactWork];
    [aCoder encodeObject:_otherEmail forKey:kContactOtherEmail];
    [aCoder encodeObject:_main forKey:kContactMain];
    [aCoder encodeObject:_address forKey:kContactAddress];
    [aCoder encodeObject:_home forKey:kContactHome];
    [aCoder encodeObject:_department forKey:kContactDepartment];
    [aCoder encodeObject:_iPhone forKey:kContactIPhone];
    [aCoder encodeObject:_name forKey:kContactName];
    [aCoder encodeObject:_workEmail forKey:kContactWorkEmail];
}

- (id)copyWithZone:(NSZone *)zone
{
    Contact *copy = [[Contact alloc] init];
    
    if (copy) {

        copy.other = [self.other copyWithZone:zone];
        copy.jobTitle = [self.jobTitle copyWithZone:zone];
        copy.homeEmail = [self.homeEmail copyWithZone:zone];
        copy.note = [self.note copyWithZone:zone];
        copy.mobile = [self.mobile copyWithZone:zone];
        copy.homePage = [self.homePage copyWithZone:zone];
        copy.company = [self.company copyWithZone:zone];
        copy.work = [self.work copyWithZone:zone];
        copy.otherEmail = [self.otherEmail copyWithZone:zone];
        copy.main = [self.main copyWithZone:zone];
        copy.address = [self.address copyWithZone:zone];
        copy.home = [self.home copyWithZone:zone];
        copy.department = [self.department copyWithZone:zone];
        copy.iPhone = [self.iPhone copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.workEmail = [self.workEmail copyWithZone:zone];
    }
    
    return copy;
}

-(NSString *)getUnknownContactName
{
    
    NSString * unknownNumber;

    NSString * unkownContact;
    
    
    
 //   NSString* venuePhone=[dictionary valueForKey:@"main"];
   // NSString* venueMobile=[dictionary valueForKey:@"mobile"];
    
    
    if (self.main.length==0)
    {
        unknownNumber=_work;
    }
    else
    {
        unknownNumber=_main;
    }
    
    if (unknownNumber.length==0)
    {
        unknownNumber=_home;
    }
    
    if (_mobile.length==0  && unknownNumber.length==0)
    {
        unknownNumber=_other;
    }
    else if(unknownNumber.length==0)
    {
        unknownNumber = _mobile;
    }
    
    unkownContact= [[NSString alloc] initWithFormat:@"Unknown - %@",unknownNumber];

    return unkownContact;
}




@end
