//
//  Contact.h
//
//  Created by UMANGSHU  on 05/12/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Contact : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *other;
@property (nonatomic, strong) NSString *jobTitle;
@property (nonatomic, strong) NSString *homeEmail;
@property (nonatomic, strong) NSString *note;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *homePage;
@property (nonatomic, strong) NSString *company;
@property (nonatomic, strong) NSString *work;
@property (nonatomic, strong) NSString *otherEmail;
@property (nonatomic, strong) NSString *main;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *home;
@property (nonatomic, strong) NSString *department;
@property (nonatomic, strong) NSString *iPhone;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *workEmail;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;
-(BOOL)isEqualToContact:(Contact *)object;
-(NSString *)getUnknownContactName;
@end
