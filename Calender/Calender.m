//
//  Calender.m
//
//  Created by UMANGSHU  on 05/12/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "Calender.h"


NSString *const kCalenderTitle = @"title";
NSString *const kCalenderEndsDate = @"endsDate";
NSString *const kCalenderStartsDate = @"startsDate";
NSString *const kCalenderLocation = @"location";
NSString *const kCalenderNote = @"note";


@interface Calender ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Calender

@synthesize title = _title;
@synthesize endsDate = _endsDate;
@synthesize startsDate = _startsDate;
@synthesize location = _location;
@synthesize note = _note;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.title = [self objectOrNilForKey:kCalenderTitle fromDictionary:dict];
            self.endsDate = [self objectOrNilForKey:kCalenderEndsDate fromDictionary:dict];
            self.startsDate = [self objectOrNilForKey:kCalenderStartsDate fromDictionary:dict];
            self.location = [self objectOrNilForKey:kCalenderLocation fromDictionary:dict];
            self.note = [self objectOrNilForKey:kCalenderNote fromDictionary:dict];

    }
    
    return self;
    
}

-(BOOL)isEqualToCalender:(Calender *)object
{
    if ([self.title isEqualToString:object.title] && [self.startsDate isEqualToString:object.startsDate] && [self.endsDate isEqualToString:object.endsDate] && [self.location isEqualToString:object.location] && [self.note isEqualToString:object.note])
    {
        return YES;
    }
    return NO;
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.title forKey:kCalenderTitle];
    [mutableDict setValue:self.endsDate forKey:kCalenderEndsDate];
    [mutableDict setValue:self.startsDate forKey:kCalenderStartsDate];
    [mutableDict setValue:self.location forKey:kCalenderLocation];
    [mutableDict setValue:self.note forKey:kCalenderNote];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.title = [aDecoder decodeObjectForKey:kCalenderTitle];
    self.endsDate = [aDecoder decodeObjectForKey:kCalenderEndsDate];
    self.startsDate = [aDecoder decodeObjectForKey:kCalenderStartsDate];
    self.location = [aDecoder decodeObjectForKey:kCalenderLocation];
    self.note = [aDecoder decodeObjectForKey:kCalenderNote];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_title forKey:kCalenderTitle];
    [aCoder encodeObject:_endsDate forKey:kCalenderEndsDate];
    [aCoder encodeObject:_startsDate forKey:kCalenderStartsDate];
    [aCoder encodeObject:_location forKey:kCalenderLocation];
    [aCoder encodeObject:_note forKey:kCalenderNote];
}

- (id)copyWithZone:(NSZone *)zone
{
    Calender *copy = [[Calender alloc] init];
    
    if (copy) {

        copy.title = [self.title copyWithZone:zone];
        copy.endsDate = [self.endsDate copyWithZone:zone];
        copy.startsDate = [self.startsDate copyWithZone:zone];
        copy.location = [self.location copyWithZone:zone];
        copy.note = [self.note copyWithZone:zone];
    }
    
    return copy;
}


@end
