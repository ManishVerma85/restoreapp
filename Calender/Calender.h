//
//  Calender.h
//
//  Created by UMANGSHU  on 05/12/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Calender : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *endsDate;
@property (nonatomic, strong) NSString *startsDate;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *note;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;
- (BOOL)isEqualToCalender:(Calender *)object;
@end
