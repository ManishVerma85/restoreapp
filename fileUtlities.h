//
//  fileUtlities.h
//  restor
//
//  Created by Andy on 22/07/2015.
//  Copyright (c) 2015 . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface fileUtlities : NSObject
{
    int _fileDescriptor;
    int _filePosition;
    int _chunkSize;

}

+(unsigned long long) fileSizeAtPath:(NSString *)fileName;
+(BOOL)replaceFile:(NSString *)initialFile withThisFile:(NSString *) newFileToReplaceWith;
+(BOOL)deleteFile:(NSString *)initialFile;
+(BOOL)insertIntoFile:(NSString *)filePath  Data:(NSData *)dat atPosition:(unsigned long long)pos;
+(BOOL)appendtoFile:(NSString *) filePath thisData:(NSData *)dataToAppend;
+(int)getNumberDirectories:(NSString *)folderPath;


- (BOOL)setFileToChunkFile:(NSString *)filePath withChunkSize:(int)chunkSize;
- (NSData*)readFileChunk;
- (BOOL)closeFile;

@end
