//
//  CallHistory.m
//
//  Created by UMANGSHU  on 05/12/14
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "CallHistory.h"


NSString *const kCallHistoryDate = @"date";
NSString *const kCallHistoryPhone = @"phone";
NSString *const kCallHistoryName = @"name";
NSString *const kCallHistoryType = @"type";
NSString *const kCallHistoryDuration = @"duration";


@interface CallHistory ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation CallHistory

@synthesize date = _date;
@synthesize phone = _phone;
@synthesize name = _name;
@synthesize type = _type;
@synthesize duration = _duration;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.date = [self objectOrNilForKey:kCallHistoryDate fromDictionary:dict];
            self.phone = [self objectOrNilForKey:kCallHistoryPhone fromDictionary:dict];
            self.name = [self objectOrNilForKey:kCallHistoryName fromDictionary:dict];
            self.type = [self objectOrNilForKey:kCallHistoryType fromDictionary:dict];
            self.duration = [self objectOrNilForKey:kCallHistoryDuration fromDictionary:dict];

    }
    
    return self;
    
}

-(BOOL)isEqualToCallHistory:(CallHistory *)object
{
    BOOL b_date = [self.date isEqualToString:object.date];
    BOOL b_phone = [self.phone isEqualToString:object.phone];
    BOOL b_name = [self.name isEqualToString:object.name];
    BOOL b_type = [self.type isEqualToString:object.type];
    BOOL b_duration = [self.duration isEqualToString:object.duration];
    
    if (b_date && b_phone && b_name && b_type && b_duration)
    {
        return YES;
    }
    return NO;
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.date forKey:kCallHistoryDate];
    [mutableDict setValue:self.phone forKey:kCallHistoryPhone];
    [mutableDict setValue:self.name forKey:kCallHistoryName];
    [mutableDict setValue:self.type forKey:kCallHistoryType];
    [mutableDict setValue:self.duration forKey:kCallHistoryDuration];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.date = [aDecoder decodeObjectForKey:kCallHistoryDate];
    self.phone = [aDecoder decodeObjectForKey:kCallHistoryPhone];
    self.name = [aDecoder decodeObjectForKey:kCallHistoryName];
    self.type = [aDecoder decodeObjectForKey:kCallHistoryType];
    self.duration = [aDecoder decodeObjectForKey:kCallHistoryDuration];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_date forKey:kCallHistoryDate];
    [aCoder encodeObject:_phone forKey:kCallHistoryPhone];
    [aCoder encodeObject:_name forKey:kCallHistoryName];
    [aCoder encodeObject:_type forKey:kCallHistoryType];
    [aCoder encodeObject:_duration forKey:kCallHistoryDuration];
}

- (id)copyWithZone:(NSZone *)zone
{
    CallHistory *copy = [[CallHistory alloc] init];
    
    if (copy) {

        copy.date = [self.date copyWithZone:zone];
        copy.phone = [self.phone copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.type = [self.type copyWithZone:zone];
        copy.duration = [self.duration copyWithZone:zone];
    }
    
    return copy;
}


@end
