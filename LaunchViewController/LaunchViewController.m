//
//  LaunchViewController.m
//  restor
//
//  Created by Umang on 12/10/14.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import "LaunchViewController.h"
#import "AppDelegate.h"


@interface LaunchViewController ()

@end

@implementation LaunchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    

    [btnLaunchNewUser.layer setCornerRadius:7.0f];
    [btnViewData.layer setCornerRadius:7.0f];
    
 //   [btnLaunchNewUser
    
 //   [btnViewData.layer setBackgroundColor:CFBridgingRetain([UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:1])];
   // [UIColor colorWithRed:0.169 green:0.627 blue:0.878 alpha:1] /*#2ba0e0*/
//    [btnViewData.layer setBackgroundColor:CFBridgingRetain([UIColor colorWithRed:0.169 green:0.627 blue:0.878 alpha:1])];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLaunch"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    /*if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }*/
    
    _checkDataTimer = [NSTimer scheduledTimerWithTimeInterval:2.6 target:self selector:@selector(checkForData) userInfo:nil repeats:YES];
    
    _dataFoundCount = 0;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    //mv
    // Set screen name.
    self.screenName = @"Launch Screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btnLaunchNewUserClicked:(id)sender
{
   //TutorialViewController * tutorialViewController =[[TutorialViewController alloc] init];
    //[self.navigationController pushViewController:tutorialViewController animated:YES];
    WebViewController *controller = [[WebViewController alloc] init];
    [controller setUrlForWebview:[NSURL URLWithString:NSLocalizedString(@"NoDataLink", nil)]];
    UIColor *backGroundColor = [UIColor colorWithRed:54.0f/255.0f
                                               green:62.0f/255.0f
                                                blue:73.0f/255.0f
                                               alpha:1.0f];
    
    
   // self.navigationController.navigationBar.topItem.title=@"How to";
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor] ];
    
    self.navigationController.navigationBar.barTintColor = backGroundColor;
    
    [self.navigationController pushViewController:controller animated:YES];

    
}

-(IBAction)btnViewDataClicked:(id)sender
{
    if ([[JSONReader sharedReader] getDevicesArray].count>0)
    {
        AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [delegate showTabbarController];
    }
  //  else
  //  {
  //      NODataViewController *controller = [[NODataViewController alloc] initWithNibName:@"NODataViewController" bundle:nil];
  //      [self.navigationController pushViewController:controller animated:YES];
  //  }
}


/**
 Determine if data has been found*/
-(void)checkForData
{
    
    
      if ([[JSONReader sharedReader] getDevicesArray].count>0)
     {
         if (_dataFoundCount==0 || _dataFoundCount==8)
         {
             

             NSLog(@"Data Found - a restore has been done while in LaunchViewController screen");
     
             [[NSNotificationCenter defaultCenter]
              postNotificationName:@"dataFound" object:self];
     
         }
     
         //let it run one more time to make sure
         // all data has been loaded
         if (_dataFoundCount == 8)
         {
             [_checkDataTimer invalidate];
         }
     
         _dataFoundCount++;
     }
    
}


@end
