//
//  LaunchViewController.h
//  restor
//
//  Created by Umang on 12/10/14.
//  Copyright (c) 2014 AgamPuram. All rights reserved.
//

#import <UIKit/UIKit.h>
//mv
@interface LaunchViewController : GAITrackedViewController
{
    __weak IBOutlet UIButton *btnLaunchNewUser;
    __weak IBOutlet UIButton *btnViewData;
    
    
    NSTimer * _checkDataTimer;
    
    int _dataFoundCount;
    
}

//mv
@property (nonatomic, copy) NSString *screenName;


-(IBAction)btnLaunchNewUserClicked:(id)sender;
-(IBAction)btnViewDataClicked:(id)sender;
@end
