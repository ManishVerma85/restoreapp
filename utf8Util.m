//
//  utf8Util.m
//  restor
//
//  Created by Andy on 22/07/2015.
//  Copyright (c) 2015 AgamPuram. All rights reserved.
//

#import "fileUtlities.h"
#import "utf8Util.h"

const long long kChunkSize = 1024;


@implementation utf8Util



- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        _fileUtil = [[fileUtlities alloc] init];
        _invalidPositions = [[NSMutableArray alloc] init];
    }
    
    return self;
}


/*
 parse a file and check each character is valid utf-8 
 */
-(BOOL)Validateutf8File:(NSString *)fileName
{
    BOOL fileReplaced=NO;
    BOOL invalidChars=NO;
    BOOL  tempWrote=NO;
    
    
    NSMutableData * chunk;
    
    //openfile
    [_fileUtil setFileToChunkFile:fileName withChunkSize:kChunkSize];
    
    
    //get file size
    long long fileSize = [fileUtlities fileSizeAtPath:fileName];
    
    if(fileSize>0)
    {
    
        //test
        long long numberOfChunks = fileSize/kChunkSize;
    
        if (fileSize % kChunkSize>0)
        {
        
            numberOfChunks ++;
        }
    
    
        int iloopCounter =0;
    
    
        NSString *tempFilename = [[NSString alloc]  initWithFormat:@"%@.tempjs",[fileName stringByDeletingPathExtension] ];
    
    
        //delete a temp file just in case it exists
        [fileUtlities deleteFile:tempFilename];
        
    
        for (iloopCounter=0; iloopCounter<numberOfChunks; iloopCounter++)
        {
        
            //load file chunk
            chunk = [[NSMutableData alloc] initWithData: [_fileUtil readFileChunk]];// :fileName withChunkSize:kChunkSize];
        
     
            //validate file chunk
            if (![self isValidUTF8:chunk])
            {
                invalidChars=YES;
                
                //replace character with space
                unsigned char replaceByte = 32;
            
                for( NSNumber * pos in _invalidPositions)
                {
                    NSLog(@"pos to replace %d",[pos intValue]);
                    
                    [chunk replaceBytesInRange:NSMakeRange([pos intValue],1) withBytes:&replaceByte];
                }
            
            }
            
            //always write the file out
            NSError * writeError;
            
            
            if (iloopCounter==0)
            {
                          //hopefully this will append
                tempWrote = [chunk writeToFile:tempFilename options:NSDataWritingWithoutOverwriting error:&writeError];
 
            }
            else
            {
                //not the first time throught the loop so append to file
                tempWrote = [fileUtlities appendtoFile:tempFilename thisData:chunk];
            }
            
             
            //write file chunk need to append not overwrite like this will
            //[chunk writeToFile:tempFilename atomically:YES];
            
    
            //and repeat to end of file
        }
    
    
        //only replace if invalid chars
        if (invalidChars && tempWrote)
        {
            //replace file
            fileReplaced = [fileUtlities replaceFile:fileName withThisFile:tempFilename];
        }
             
        [fileUtlities deleteFile:tempFilename];
    
    }
    else
    {
        fileReplaced = NO;
    }
    
    return fileReplaced;
}


-(BOOL)isValidUTF8:(NSData *)utf8DataProbably
{
    BOOL isValid=YES;
    
    //loop through the data
    const Byte * buffer = [utf8DataProbably bytes];
    
    int size = (int)utf8DataProbably.length;
    
  //  BOOL firstByte=YES;
    int expectedLength = 1;
    
    for (int iloopCounter=0; iloopCounter<size; iloopCounter++)
    {
        
        //if (firstByte)
        {

            if (buffer[iloopCounter]>244 || buffer[iloopCounter]==192 || buffer[iloopCounter]==193)
            {
                isValid=NO;
                
                /*unsigned char tempChar =  buffer[iloopCounter];
                
                NSLog(@"Invalid UTF-8 char is %d at position %d",tempChar,iloopCounter);
                */
                
                NSNumber * invalidPos = [[NSNumber alloc] initWithInt:iloopCounter];
                
                
                [_invalidPositions addObject:invalidPos];
                
            }
            else if((buffer[iloopCounter] & 0b10000000) == 0b00000000)
            {
                expectedLength = 1;
            }
            else if ((buffer[iloopCounter] & 0b11100000) == 0b11000000)
            {
                expectedLength = 2;
            }
            else if ((buffer[iloopCounter] & 0b11110000) == 0b11100000)
            {
                expectedLength = 3;
            }
            else if ((buffer[iloopCounter] & 0b11111000) == 0b11110000)
            {
                expectedLength = 4;
            }
            else if ((buffer[iloopCounter] & 0b11000000) == 0b10000000)
            {
                //continuation byte
                //expectedLength = 6;// should not occur as current utf8 spec says its now only 4 characters
                
                
            }
          /*  else if ((buffer[iloopCounter] & 0b11111100) == 0b11111000)
            {
                expectedLength = 5;// should not occur as current utf8 spec says its now only 4 characters
            }
            else if ((buffer[iloopCounter] & 0b11111110) == 0b11111100)
            {
                expectedLength = 6;// should not occur as current utf8 spec says its now only 4 characters
            }*/
            else
            {
                //is invalid
                
                isValid=NO;
                
 //               unsigned char tempChar =  buffer[iloopCounter];
                
 //               NSLog(@"Invalid UTF-8 char is %d at position %d",tempChar,iloopCounter);
        
                NSNumber * invalidPos = [[NSNumber alloc] initWithInt:iloopCounter];
                
                
                [_invalidPositions addObject:invalidPos];
                
                //recorrecting data
        
                //assume it is a first byte?
                //check if it is a continuation byte?
                //continuation byte then reconstruct it
                
            }
            
        }
        
        
        
    }
    
    
    return isValid;
}


/*
 determines if it is a first byte and sends back the number of characters*/
-(int)firstByte:(NSData *) data withOffset:(int)offset
{
    int expectedLength=-1;
    int size = (int)data.length;
    
    //loop through the data
    const Byte * buffer = [data bytes];
    

    for (int iloopCounter=offset; iloopCounter<size; iloopCounter++)
    {
        
        
        if((buffer[iloopCounter] & 0b10000000) == 0b00000000)
        {
            expectedLength = 1;
        
        }
        else if ((buffer[iloopCounter] & 0b11100000) == 0b11000000)
        {
            expectedLength = 2;
        
        }
        else if ((buffer[iloopCounter] & 0b11110000) == 0b11100000)
        {
            expectedLength = 3;
        
        }
        else if ((buffer[iloopCounter] & 0b11111000) == 0b11110000)
        {
            expectedLength = 4;
        
        }
        else if ((buffer[iloopCounter] & 0b11111100) == 0b11111000)
        {
            expectedLength = 5;// should not occur as current utf8 spec says its now only 4 characters
        
        }
        else if ((buffer[iloopCounter] & 0b11111110) == 0b11111100)
        {
            expectedLength = 6;// should not occur as current utf8 spec says its now only 4 characters
        
        }
        else if ((buffer[iloopCounter] & 0b11000000) == 0b10000000)
        {
            
            //this is a continuation character
            
            
            
            
                //is invalid
                
                //recorrecting data
                
                //assume it is a first byte?
                //check if it is a continuation byte?
                //continuation byte then reconstruct it
                
        }
        else
        {
            //invalid utf-8
            //store the poisition that is invalid
            
            
            
            
        }

        
        
        
    }

    
    return  expectedLength;
    
}




+ (NSString *) data2UTF8String:(NSData *) data
{
    
    // First try to do the 'standard' UTF-8 conversion
    NSString * bufferStr = [[NSString alloc] initWithData:data
                                                 encoding:NSUTF8StringEncoding];
    
    // if it fails, do the 'lossy' UTF8 conversion
    if (!bufferStr)
    {
        const Byte * buffer = [data bytes];
        
        NSMutableString * filteredString = [[NSMutableString alloc] init];
        
        int i = 0;
        while (i < [data length])
        {
            
            int expectedLength = 1;
            
            if      ((buffer[i] & 0b10000000) == 0b00000000) expectedLength = 1;
            else if ((buffer[i] & 0b11100000) == 0b11000000) expectedLength = 2;
            else if ((buffer[i] & 0b11110000) == 0b11100000) expectedLength = 3;
            else if ((buffer[i] & 0b11111000) == 0b11110000) expectedLength = 4;
            else if ((buffer[i] & 0b11111100) == 0b11111000) expectedLength = 5;
            else if ((buffer[i] & 0b11111110) == 0b11111100) expectedLength = 6;
            
            int length = (int)MIN(expectedLength, [data length] - i);
            NSData * character = [NSData dataWithBytes:&buffer[i] length:(sizeof(Byte) * length)];
            
            NSString * possibleString = [NSString stringWithUTF8String:[character bytes]];
            if (possibleString)
            {
                [filteredString appendString:possibleString];
            }
            i = i + expectedLength;
        
        }
        
        bufferStr = filteredString;
    }
    
    return bufferStr;
}



+(BOOL)insertUTF8TestDataIntoFile:(NSString *)filePath  atPosition:(int)pos
{
    BOOL testDataInserted=NO;

    const unsigned char bytes[] = {255,192,193,245,246,247,248,249,250,251,252,253,254};//all invalid utf-8 data
    NSData *data = [NSData dataWithBytes:bytes length:sizeof(bytes)];
    NSLog(@"%@", data);

    
    testDataInserted =  [fileUtlities insertIntoFile:filePath Data:data atPosition:pos];
    
    
    return testDataInserted;
}




@end
